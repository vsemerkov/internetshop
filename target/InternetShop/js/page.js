﻿$(document).ready(function() {
	
	$(".dropdown-toggle").dropdown();
	
	$(function () {
        $("input[name=email]").keyup(function () { validateEmail(); });
        $("input[name=email]").blur(function () { validateEmail(); });
    });
	
	function validateEmail(){
		$.ajax({
            url: 'emailValidation',
            type: 'POST',
            data: {
                'email': $("input[name=email]").val()
            },
            success: function (data) {
                if (data["result"]) {
                	$("span[id=email]").text(data["message"]);
                   }
                else {
                	$("span[id=email]").text("");
                }
            },
            error: function (data) {
                alert("Email validation error!");
            }
        });
	}
	
	$(function () {
        $("input[name=number]").keyup(function () { validateProductNumber($(this)); });
        $("input[name=number]").blur(function () { validateProductNumber($(this)); });
    });
	
	function isPositiveNumber(x) { 
		   var y = parseInt(x); 
		   if (isNaN(y)) return false; 
		   return x == y && x.toString() == y.toString() && y > 0; 
		}
	
	function validateProductNumber(param){
		var id = param.attr("id");
		var number;
		if(!isPositiveNumber(param.val())){
			param.val("");
			number = 0;
		}else{
			number = param.val();
		}
		$.ajax({
            url: 'changeNumberOfProduct',
            type: 'POST',
            data: {
                'id': id,
                'number': number
            },
            success: function (data) {
            	if(data["result"]){
            		$("span[id=numberOfProducts]").text(data["numberOfProducts"]);
            		var totalSum = Number.prototype.toFixed.call(parseFloat(data["totalSum"]) || 0, 2);
            		$("span[id=totalSum]").text(totalSum);
            		var sum = Number.prototype.toFixed.call(parseFloat(data["sum"]) || 0, 2);
            		$("span[id=sum" + id + "]").text(sum);
            	}
            },
            error: function (data) {
                alert("Number changing error!");
            }
        });
	}
	
	$(function () {
        $("a.image_template_button").click(function () { calculateProductNumber($(this)); });
    });
	
	function calculateProductNumber(param){
		var id = param.attr("id");
		var fl = 0;
		if(id.startsWith("plus")){
			id = id.substring(4);
			fl = 1;
		}
		if(id.startsWith("minus")){
			id = id.substring(5);
			fl = -1;
		}
		var number = $("input[id="+id+"]").val();
		if(isNaN(number)){
			number = 0;
		}
		if(fl > 0){
			number++;
		}
		if(fl < 0){
			number--;
		}
		if(number > 0){
			$("input[id="+id+"]").val(number);
		}else{
			$("input[id="+id+"]").val("");
		}
		validateProductNumber($("input[id="+id+"]"));
	}
	
	String.prototype.startsWith = function(str){
		return (this.match("^"+str)==str);
	};
	
	function calculateTotalSum(){
		var totalSum = 0;
		$("span.sum").each(function(index, sum){
			var $sum = $(sum);
            totalSum += parseFloat($sum.text());			
		});
		$("span[id=totalSum]").text(totalSum);
	}
	
	$(function () {
        $("button.add_to_basket").click(function () { addToBasket($(this)); });
    });
	
	function addToBasket(param){
		var id = param.attr("id");
		$.ajax({
            url: 'addToBasket',
            type: 'POST',
            data: {
                'id': id
            },
            success: function (data) {
                if (data["result"]) {
                	$("span[id=numberOfProducts]").text(data["numberOfProducts"]);
                	$("span[id=totalSum]").text(data["totalSum"]);
                }
            },
            error: function (data) {
                alert("Product addition in basket error!");
            }
        });
	}
	
	basketParameters();
	
	function basketParameters(){
		$.ajax({
            url: 'basketParameters',
            type: 'GET',
            success: function (data) {
            	$("span[id=numberOfProducts]").text(data["numberOfProducts"]);
            	var totalSum = Number.prototype.toFixed.call(parseFloat(data["totalSum"]) || 0, 2);
            	$("span[id=totalSum]").text(totalSum);
            },
            error: function (data) {
                alert("Basket error!");
            }
        });
	}
	
	$(function () {
        $("select[name=paymentType]").change(function () { changeCardField(); });
    });
	
	changeCardField();
	
	function changeCardField(){
		if($("select[name=paymentType]").val() == "cash"){
			$("div[id=card_number]").removeClass("visible");
			$("div[id=card_number]").addClass("invisible");
		}else{
			$("div[id=card_number]").removeClass("invisible");
			$("div[id=card_number]").addClass("visible");
		}		
	}
	
	changeCardText();
	
	function changeCardText(){
		if($("input[name=paymentType]").val() == "cash"){
			$("div[id=card_number_text]").removeClass("visible");
			$("div[id=card_number_text]").addClass("invisible");
		}else{
			$("div[id=card_number_text]").removeClass("invisible");
			$("div[id=card_number_text]").addClass("visible");
		}		
	}
	
});