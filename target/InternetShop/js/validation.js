$(document).ready(function () {
	var namePattern = /^[a-zA-Z]{2,25}$/;
	
	$("input[name=firstName]").focus();
	
	$(function () {
        $("button[name=sendData]").click(function () {return checkData();});
		$("input[name=firstName]").keyup(function () { checkFirstName(); });
		$("input[name=firstName]").blur(function () { checkFirstName(); });
		$("input[name=lastName]").keyup(function () { checkLastName(); });
		$("input[name=lastName]").blur(function () { checkLastName(); });
		$("input[name=email]").keyup(function () { checkEmail(); });
		$("input[name=email]").blur(function () { checkEmail(); });
		$("input[name=password]").keyup(function () { checkPassword(); });
		$("input[name=password]").blur(function () { checkPassword(); });
		$("input[name=passwordRepeat]").keyup(function () { checkPasswordRepeat(); });
		$("input[name=passwordRepeat]").blur(function () { checkPasswordRepeat(); });
		$("input[name=captcha]").keyup(function () { checkCaptchaLength(); });
		$("input[name=captcha]").blur(function () { checkCaptchaLength(); });
		$("button[name=sendLoginData]").click(function () {return checkLoginData();});
		$("button[name=formSendLoginData]").click(function () {checkFormLoginData();});
    });
	
	function checkFirstName(){
		var firstName = $("input[name=firstName]").val();
		if(!namePattern.test(firstName)){
			$("span[id=firstName]").text("First name is incorrect!");
			return false;
		}
		else{
			$("span[id=firstName]").text("");
			return true;
		}
	}
	
	function checkLastName(){
		var lastName = $("input[name=lastName]").val();
		if(!namePattern.test(lastName)){
			$("span[id=lastName]").text("Last name is incorrect!");
			return false;
		}
		else{
			$("span[id=lastName]").text("");
			return true;
		}
	}
	
	function checkEmail(name, id){
		var email = $("input[name="+name+"]").val();
		var emailPattern = /^([a-z0-9_\.-]+)@([a-z0-9_\.-]+)\.([a-z\.]{2,6})$/i;
		if(!emailPattern.test(email)){
			$("span[id="+id+"]").text("Email is incorrect!");
			return false;
		}
		else{
			$("span[id="+name+"]").text("");
			return true;
		}
	}
	
	function checkPassword(name, id){
		var password = $("input[name="+name+"]").val();
		var passwordPattern = /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/;
		if(!passwordPattern.test(password)){
			$("span[id="+id+"]").text("Password must contains more than 8 characters, lower-case and upper-case characters, digits, wildcard characters!");
			return false;
		}
		else{
			$("span[id="+name+"]").text("");
			return true;
		}
	}
	
	function checkPasswordRepeat(){
		var password = $("input[name=password]").val();
		var passwordRepeat = $("input[name=passwordRepeat]").val();
		if(!(password.toString() == passwordRepeat.toString())){
			$("span[id=passwordRepeat]").text("Passwords don't agree!");
			return false;
		}
		else{
			$("span[id=passwordRepeat]").text("");
			return true;
		}
	}
	
	function checkCaptchaLength(){
		var captcha = $("input[name=captcha]").val();
		if(captcha == undefined || captcha.length != 5){
			$("span[id=captcha]").text("Captcha length is not equals to 5!");
			return false;
		}
		else{
			$("span[id=captcha]").text("");
			return true;
		}
	}
	
	function checkData(){
		var result = checkFirstName();
		result = checkLastName() && result;
		result = checkEmail("email", "email") && result;
		result = checkPassword("password", "password") && result;
		result = checkPasswordRepeat() && result;
		result = checkCaptchaLength() && result;
		return result;
	}
	
	function checkLoginData(){
		var result = checkEmail("loginEmail", "loginError");
		result = checkPassword("loginPassword", "loginError") && result;
		return result;
	}
	
	function checkFormLoginData(){
		var result = checkEmail("formLoginEmail", "formLoginError");
		result = checkPassword("formLoginPassword", "formLoginError") && result;
		if(result){
			$.ajax({
	            url: 'login',
	            type: 'POST',
	            data: {
	                'loginEmail': $("input[name=formLoginEmail]").val(),
	                'loginPassword': $("input[name=formLoginPassword]").val(),
	            }
			
	        });
		}
		return result;
	}
});