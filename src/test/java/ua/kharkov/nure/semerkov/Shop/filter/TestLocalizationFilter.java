package ua.kharkov.nure.semerkov.Shop.filter;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.mockito.ArgumentCaptor;

import ua.kharkov.nure.semerkov.Shop.parameter.ServiceParameters;

/**
 * Test Case for check <code>LocalizationFilter</code> class.
 * 
 * @author Volodymyr_Semerkov
 * 
 */
@RunWith(value = Parameterized.class)
public class TestLocalizationFilter {
	private FilterChain chain;
	private LocalizationFilter localizationFilter;
	private HttpServletRequest request;
	private HttpServletResponse response;
	private ArgumentCaptor<HttpServletRequest> requestCaptor = ArgumentCaptor
			.forClass(HttpServletRequest.class);
	private List<Locale> supportedLocales;
	private Locale defaultLocale;
	private String localeManager;
	private String managerLocale;
	private String langParameter;
	private Enumeration<Locale> browserLocales;
	private String langControlValue;

	public TestLocalizationFilter(Locale[] supportedLocales,
			Locale defaultLocale, String localeManager, String managerLocale,
			String langParameter, Locale[] browserLocales,
			String langControlValue) {
		this.supportedLocales = Arrays.asList(supportedLocales);
		this.defaultLocale = defaultLocale;
		this.localeManager = localeManager;
		this.managerLocale = managerLocale;
		this.langParameter = langParameter;
		this.browserLocales = Collections.enumeration(Arrays
				.asList(browserLocales));
		this.langControlValue = langControlValue;
	}

	@Parameters
	public static Collection<Object[]> formParameters() {
		return Arrays
				.asList(new Object[][] {
						// TESTS FOR SESSION LOCALE MANAGER
						// test for browser locales
						{
								new Locale[] { new Locale("en"),
										new Locale("ru") },
								new Locale("en"),
								ServiceParameters.SESSION_MANAGER,
								null,
								null,
								new Locale[] { new Locale("ru"),
										new Locale("en", "US") }, "ru" },
						// test for manager locale
						{
								new Locale[] { new Locale("en"),
										new Locale("ru") },
								new Locale("en"),
								ServiceParameters.SESSION_MANAGER,
								"ru",
								null,
								new Locale[] { new Locale("en"),
										new Locale("ru") }, "ru" },
						// test for language parameter
						{
								new Locale[] { new Locale("en"),
										new Locale("ru") },
								new Locale("en"),
								ServiceParameters.SESSION_MANAGER,
								"ru",
								"en",
								new Locale[] { new Locale("en"),
										new Locale("ru") }, "en" },
						// test for default locale
						{
								new Locale[] { new Locale("en"),
										new Locale("ru") },
								new Locale("ru"),
								ServiceParameters.SESSION_MANAGER,
								null,
								null,
								new Locale[] { new Locale("fr"),
										new Locale("ro") }, "ru" },
						// test for manager locale if language parameter is
						// incorrect
						{
								new Locale[] { new Locale("ru") },
								new Locale("ru"),
								ServiceParameters.SESSION_MANAGER,
								"ru",
								"language",
								new Locale[] { new Locale("en"),
										new Locale("ru") }, "ru" },
						// test for default locale if language parameter is
						// incorrect
						{
								new Locale[] { new Locale("en") },
								new Locale("en"),
								ServiceParameters.SESSION_MANAGER,
								null,
								"language",
								new Locale[] { new Locale("en"),
										new Locale("ru") }, "en" },
						// TESTS FOR COOKIES LOCALE MANAGER
						// test for browser locales
						{
								new Locale[] { new Locale("en"),
										new Locale("ru") },
								new Locale("en"),
								ServiceParameters.COOKIES_MANAGER,
								null,
								null,
								new Locale[] { new Locale("ru"),
										new Locale("en", "US") }, "ru" },
						// test for manager locale
						{
								new Locale[] { new Locale("en"),
										new Locale("ru") },
								new Locale("en"),
								ServiceParameters.COOKIES_MANAGER,
								"ru",
								null,
								new Locale[] { new Locale("en"),
										new Locale("ru") }, "ru" },
						// test for language parameter
						{
								new Locale[] { new Locale("en"),
										new Locale("ru") },
								new Locale("en"),
								ServiceParameters.COOKIES_MANAGER,
								"ru",
								"en",
								new Locale[] { new Locale("en"),
										new Locale("ru") }, "en" },
						// test for manager locale if language parameter is
						// incorrect
						{
								new Locale[] { new Locale("ru") },
								new Locale("ru"),
								ServiceParameters.COOKIES_MANAGER,
								"ru",
								"language",
								new Locale[] { new Locale("en"),
										new Locale("ru") }, "ru" } });
	}

	@Before
	public void create() throws ServletException {
		chain = mock(FilterChain.class);
		ServletContext servletContext = mock(ServletContext.class);
		when(servletContext.getAttribute(ServiceParameters.SUPPORTED_LOCALES))
				.thenReturn(supportedLocales);
		when(servletContext.getAttribute(ServiceParameters.DEFAULT_LOCALE))
				.thenReturn(defaultLocale);
		when(servletContext.getInitParameter(ServiceParameters.LOCALE_MANAGER))
				.thenReturn(localeManager);
		when(
				servletContext
						.getInitParameter(ServiceParameters.COOKIES_TIME_OUT))
				.thenReturn("180");
		FilterConfig filterConfig = mock(FilterConfig.class);
		when(filterConfig.getServletContext()).thenReturn(servletContext);
		request = mock(HttpServletRequest.class);
		response = mock(HttpServletResponse.class);
		HttpSession session = mock(HttpSession.class);
		when(session.getAttribute(ServiceParameters.USER_LOCALE)).thenReturn(
				managerLocale == null ? null : new Locale(managerLocale));
		when(request.getSession()).thenReturn(session);
		Cookie[] cookies = new Cookie[] { new Cookie(
				ServiceParameters.USER_LOCALE, managerLocale) };
		when(request.getCookies()).thenReturn(
				managerLocale == null ? null : cookies);
		when(request.getParameter(ServiceParameters.LANG_PARAMETER))
				.thenReturn(langParameter);
		when(request.getLocales()).thenReturn(browserLocales);
		localizationFilter = new LocalizationFilter();
		localizationFilter.init(filterConfig);
	}

	@Test
	public void test() throws IOException, ServletException {
		localizationFilter.doFilter(request, response, chain);
		verify(chain).doFilter(requestCaptor.capture(),
				any(HttpServletResponse.class));
		assertEquals(langControlValue, requestCaptor.getValue().getLocales()
				.nextElement().getLanguage());
	}
}
