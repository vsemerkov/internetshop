package ua.kharkov.nure.semerkov.Shop.security.captcha.manager;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;

import ua.kharkov.nure.semerkov.Shop.parameter.ServiceParameters;
import ua.kharkov.nure.semerkov.Shop.parameter.UserParameters;

/**
 * Test Case for check <code>SessionCaptchaManager</code> class.
 * 
 * @author Volodymyr_Semerkov
 * 
 */
public class TestSessionCaptchaManager {
	private static HttpServletRequest request;
	private static CaptchaManager captchaManager;
	private static HttpSession session;

	@Before
	public void create() {
		request = mock(HttpServletRequest.class);
		captchaManager = new SessionCaptchaManager(1800);
		session = mock(HttpSession.class);
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute(ServiceParameters.CAPTCHA__CODE)).thenReturn(
				"1111");
	}

	@Test
	public void testCaptchaManagerByCorrectData() {
		when(request.getParameter(UserParameters.USER__CAPTCHA)).thenReturn(
				"1111");
		assertTrue(captchaManager.validateCaptcha(request));
	}

	@Test
	public void testCaptchaManagerByIncorrectData() {
		when(request.getParameter(UserParameters.USER__CAPTCHA)).thenReturn(
				"0000");
		assertFalse(captchaManager.validateCaptcha(request));
	}
}
