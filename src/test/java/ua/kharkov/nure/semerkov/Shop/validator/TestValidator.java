package ua.kharkov.nure.semerkov.Shop.validator;

import static org.junit.Assert.assertTrue;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import ua.kharkov.nure.semerkov.Shop.parameter.UserParameters;
import ua.kharkov.nure.semerkov.Shop.entity.FormBean;

/**
 * Test Case for check <code>Validator</code> class.
 * 
 * @author Volodymyr_Semerkov
 * 
 */
public class TestValidator {
	private static Validator validator;
	private FormBean formBean;

	@Before
	public void initFormBean() {
		validator = new Validator();
		formBean = new FormBean("Ivan", "Ivanov", "ivanov@gmail.com",
				"Qwerty_123", "Qwerty_123", true);
	}

	@Test
	public void testRightData() {
		Map<String, String> paramErrors = validator.validateFormBean(formBean);
		assertTrue(paramErrors.size() == 0);
	}

	@Test
	public void testNameValidation() {
		formBean.setFirstName("First_name1");
		formBean.setLastName("Petrov*");
		Map<String, String> paramErrors = validator.validateFormBean(formBean);
		assertTrue(paramErrors.containsKey(UserParameters.USER__FIRST_NAME));
		assertTrue(paramErrors.containsKey(UserParameters.USER__LAST_NAME));
	}

	@Test
	public void testEmailValidation() {
		formBean.setEmail("stepan^@bk.ru");
		Map<String, String> paramErrors = validator.validateFormBean(formBean);
		assertTrue(paramErrors.containsKey(UserParameters.USER__EMAIL));
	}

	@Test
	public void testPasswordValidation() {
		formBean.setPassword("efedde_azzaq");
		Map<String, String> paramErrors = validator.validateFormBean(formBean);
		assertTrue(paramErrors.containsKey(UserParameters.USER__PASSWORD));
	}
}
