package ua.kharkov.nure.semerkov.Shop.servlets;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import ua.kharkov.nure.semerkov.Shop.parameter.DatabaseParameters;
import ua.kharkov.nure.semerkov.Shop.Path;
import ua.kharkov.nure.semerkov.Shop.parameter.ServiceParameters;
import ua.kharkov.nure.semerkov.Shop.parameter.UserParameters;
import ua.kharkov.nure.semerkov.Shop.security.captcha.manager.CaptchaManager;
import ua.kharkov.nure.semerkov.Shop.dao.UserDAO;
import ua.kharkov.nure.semerkov.Shop.dao.db.jdbc.JdbcHelper;
import ua.kharkov.nure.semerkov.Shop.dao.mysql.MysqlUserDAO;
import ua.kharkov.nure.semerkov.Shop.dao.exception.DAOException;
import ua.kharkov.nure.semerkov.Shop.db.DatabaseManager;
import ua.kharkov.nure.semerkov.Shop.service.UserImageService;
import ua.kharkov.nure.semerkov.Shop.service.UserService;

/**
 * Test Case for check <code>RegistrationServlet</code> class.
 * 
 * @author Volodymyr_Semerkov
 * 
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(JdbcHelper.class)
public class TestRegistrationServlet {
	public static final String USER_EMAIL = "ivan.ivanov@gmail.com";
	private RegistrationServlet registrationServlet;
	private HttpServletRequest request;
	private HttpServletResponse response;
	private RequestDispatcher requestDispatcher;
	private ServletContext servletContext;
	private UserService userService;
	private UserImageService imageService;
	private CaptchaManager captchaManager;

	@BeforeClass
	public static void createDatabase() throws DAOException {
		DatabaseManager.create(DatabaseParameters.TEST_DB_FILE_PATH);
	}

	@Before
	public void createParameters() throws ServletException,
            ClassNotFoundException, SQLException {
		registrationServlet = new RegistrationServlet();
		request = mock(HttpServletRequest.class);
		response = mock(HttpServletResponse.class);
		requestDispatcher = mock(RequestDispatcher.class);
		when(request.getRequestDispatcher(Path.PAGE__SIGN_UP)).thenReturn(
				requestDispatcher);
		servletContext = mock(ServletContext.class);
		PowerMockito.mockStatic(JdbcHelper.class);
		PowerMockito.when(JdbcHelper.getConnection()).thenAnswer(
				new Answer<Connection>() {

					@Override
					public Connection answer(InvocationOnMock invocation)
							throws Throwable {
						return getConnection();
					}
				});
		UserDAO userDAO = new MysqlUserDAO();
		userService = new UserService(userDAO);
		when(servletContext.getAttribute(ServiceParameters.USER__SERVICE))
				.thenReturn(userService);
		imageService = mock(UserImageService.class);
		when(servletContext.getAttribute(ServiceParameters.USER_IMAGE__SERVICE))
				.thenReturn(imageService);
		captchaManager = mock(CaptchaManager.class);
		when(servletContext.getAttribute(ServiceParameters.CAPTCHA__MANAGER))
				.thenReturn(captchaManager);
		ServletConfig servletConfig = mock(ServletConfig.class);
		when(servletConfig.getServletContext()).thenReturn(servletContext);
		registrationServlet.init(servletConfig);
		when(registrationServlet.getServletConfig()).thenReturn(servletConfig);
		when(servletConfig.getServletContext()).thenReturn(servletContext);
		when(registrationServlet.getServletContext())
				.thenReturn(servletContext);
		when(request.getServletContext()).thenReturn(servletContext);
		when(captchaManager.validateCaptcha(request)).thenReturn(true);
		when(request.getParameter(UserParameters.USER__FIRST_NAME)).thenReturn(
				"Ivan");
		when(request.getParameter(UserParameters.USER__LAST_NAME)).thenReturn(
				"Ivanov");
		when(request.getParameter(UserParameters.USER__EMAIL)).thenReturn(
				USER_EMAIL);
		when(request.getParameter(UserParameters.USER__PASSWORD)).thenReturn(
				"Simple_password_007");
		when(request.getParameter(UserParameters.USER__PASSWORD_REPEAT))
				.thenReturn("Simple_password_007");
		when(request.getParameter(UserParameters.USER__SUBSCRIBE)).thenReturn(
				"on");
	}

	@Test
	public void testServletByCorrectData() throws IOException,
			ServletException, DAOException {
		registrationServlet.doPost(request, response);
		verify(response).sendRedirect(Path.COMMAND__INDEX);
	}

	@Test
	public void testServletByIncorrectPasswordData() throws IOException,
			ServletException {
		when(request.getParameter(UserParameters.USER__EMAIL)).thenReturn(
				"ivan_ivanov@gmail.com");
		when(request.getParameter(UserParameters.USER__PASSWORD)).thenReturn(
				"password");
		when(request.getParameter(UserParameters.USER__PASSWORD_REPEAT))
				.thenReturn("Simple_password_007");
		registrationServlet.doPost(request, response);
		verify(request).getRequestDispatcher(Path.PAGE__SIGN_UP);
		verify(requestDispatcher).forward(request, response);
	}

	@Test
	public void testUserExists() throws ServletException, IOException {
		registrationServlet.doPost(request, response);
		verify(response).sendRedirect(Path.COMMAND__INDEX);
		registrationServlet.doPost(request, response);
		verify(request).getRequestDispatcher(Path.PAGE__SIGN_UP);
		verify(requestDispatcher).forward(request, response);
	}

	@After
	public void remove() throws DAOException {
		userService.remove(USER_EMAIL);
	}

	@AfterClass
	public static void dropDatabase() throws DAOException {
		DatabaseManager.drop(DatabaseParameters.TEST_DB_NAME);
	}

	private Connection getConnection() throws ClassNotFoundException,
			SQLException {
		Class.forName(DatabaseParameters.DB_DRIVER_NAME);
		Connection connection = DriverManager.getConnection(
				DatabaseParameters.TEST_DB_URL, DatabaseParameters.USER,
				DatabaseParameters.PASSWORD);
		connection.setAutoCommit(false);
		return connection;
	}
}
