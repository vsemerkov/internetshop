package ua.kharkov.nure.semerkov.Shop.security.captcha.manager;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;

import ua.kharkov.nure.semerkov.Shop.parameter.ServiceParameters;
import ua.kharkov.nure.semerkov.Shop.parameter.UserParameters;

/**
 * Test Case for check <code>HiddenCaptchaManager</code> class.
 * 
 * @author Volodymyr_Semerkov
 * 
 */
public class TestHiddenCaptchaManager {
	private static HttpServletRequest request;
	private static HttpServletResponse response;
	private static CaptchaManager captchaManager;

	@Before
	public void create() {
		request = mock(HttpServletRequest.class);
		response = mock(HttpServletResponse.class);
		captchaManager = new HiddenCaptchaManager(1800, 300);
		when(request.getParameter(ServiceParameters.CAPTCHA__ID)).thenReturn(
				"0");
		when(request.getParameter(UserParameters.USER__CAPTCHA)).thenReturn(
				"1111");
	}

	@Test
	public void testCaptchaManagerByCorrectData() {
		captchaManager.setCaptchaCode(request, response, "1111");
		assertTrue(captchaManager.validateCaptcha(request));
	}

	@Test
	public void testCaptchaManagerByIncorrectData() {
		captchaManager.setCaptchaCode(request, response, "0000");
		assertFalse(captchaManager.validateCaptcha(request));
	}
}
