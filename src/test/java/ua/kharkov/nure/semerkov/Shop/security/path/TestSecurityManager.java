package ua.kharkov.nure.semerkov.Shop.security.path;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import ua.kharkov.nure.semerkov.Shop.Role;
import ua.kharkov.nure.semerkov.Shop.security.path.*;
import ua.kharkov.nure.semerkov.Shop.security.path.SecurityManager;

/**
 * Test Case for check <code>SecurityManager</code> class.
 * 
 * @author Volodymyr_Semerkov
 * 
 */
@RunWith(value = Parameterized.class)
public class TestSecurityManager {
	private SecurityManager securityManager;
	private String urlPattern;
	private List<Role> roles;
	private String path;
	private Role role;
	private boolean res;

	public TestSecurityManager(String urlPattern, Role[] roles, String path,
			Role role, boolean res) {
		this.urlPattern = urlPattern;
		this.roles = Arrays.asList(roles);
		this.path = path;
		this.role = role;
		this.res = res;
	}

	@Parameters
	public static Collection<Object[]> formParameters() {
		return Arrays.asList(new Object[][] {
				// 0 - role has access to page
				{ "/login*", new Role[] { Role.ADMIN, Role.CLIENT}, "/login",
						Role.VISITOR, true },
				// 1 - role hasn't access to page
				{ "/login*", new Role[] { Role.ADMIN }, "/login/?lang=ru", Role.ADMIN,
						false },
				// 2 - url-pattern and path are different
				{ "/registration*", new Role[] { Role.ADMIN }, "/login",
						Role.ADMIN, true }

		});
	}

	@Before
	public void create() {
		List<Constraint> constraints = new ArrayList<Constraint>();
		Constraint constraint = new Constraint();
		constraint.setURLPattern(urlPattern);
		for (Role role : roles) {
			constraint.getRoles().add(role);
		}
		constraints.add(constraint);
		securityManager = new SecurityManager(constraints);
	}

	@Test
	public void test() {
		assertEquals(securityManager.accept(path, role), res);
	}
}
