package ua.kharkov.nure.semerkov.Shop.servlets.order;

import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import ua.kharkov.nure.semerkov.Shop.Path;
import ua.kharkov.nure.semerkov.Shop.Role;
import ua.kharkov.nure.semerkov.Shop.dao.OrderDAO;
import ua.kharkov.nure.semerkov.Shop.dao.OrderedProductDAO;
import ua.kharkov.nure.semerkov.Shop.dao.db.jdbc.JdbcHelper;
import ua.kharkov.nure.semerkov.Shop.dao.exception.DAOException;
import ua.kharkov.nure.semerkov.Shop.dao.mysql.MysqlOrderDAO;
import ua.kharkov.nure.semerkov.Shop.dao.mysql.MysqlOrderedProductDAO;
import ua.kharkov.nure.semerkov.Shop.db.DatabaseManager;
import ua.kharkov.nure.semerkov.Shop.entity.User;
import ua.kharkov.nure.semerkov.Shop.parameter.DatabaseParameters;
import ua.kharkov.nure.semerkov.Shop.parameter.ServiceParameters;
import ua.kharkov.nure.semerkov.Shop.parameter.UserParameters;
import ua.kharkov.nure.semerkov.Shop.security.declaration.SecureServlet;
import ua.kharkov.nure.semerkov.Shop.service.OrderService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static org.mockito.Mockito.*;

/**
 * Test Case for check <code>AdminOrdersServlet</code> class.
 *
 * @author Volodymyr_Semerkov
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(JdbcHelper.class)
public class TestAdminOrdersServlet {
    private SecureServlet adminOrdersServlet;
    private HttpServletRequest request;
    private HttpServletResponse response;
    private HttpSession session;
    private RequestDispatcher requestDispatcher;
    private ServletContext servletContext;

    @BeforeClass
    public static void createDatabase() throws DAOException {
        DatabaseManager.create(DatabaseParameters.TEST_DB_FILE_PATH);
    }

    @Before
    public void createParameters() throws SQLException, ServletException {
        adminOrdersServlet = new AdminOrdersServlet();
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        session = mock(HttpSession.class);
        requestDispatcher = mock(RequestDispatcher.class);
        when(request.getRequestDispatcher(Path.PAGE__SIGN_UP)).thenReturn(
                requestDispatcher);
        servletContext = mock(ServletContext.class);
        PowerMockito.mockStatic(JdbcHelper.class);
        PowerMockito.when(JdbcHelper.getConnection()).thenAnswer(
                new Answer<Connection>() {

                    @Override
                    public Connection answer(InvocationOnMock invocation)
                            throws Throwable {
                        return getConnection();
                    }
                });
        OrderDAO orderDAO = new MysqlOrderDAO();
        OrderedProductDAO orderedProductDAO = new MysqlOrderedProductDAO();
        OrderService orderService = new OrderService(orderDAO, orderedProductDAO);
        when(servletContext.getAttribute(ServiceParameters.ORDER__SERVICE))
                .thenReturn(orderService);
        ServletConfig servletConfig = mock(ServletConfig.class);
        when(servletConfig.getServletContext()).thenReturn(servletContext);
        adminOrdersServlet.init(servletConfig);
        when(adminOrdersServlet.getServletConfig()).thenReturn(servletConfig);
        when(servletConfig.getServletContext()).thenReturn(servletContext);
        when(adminOrdersServlet.getServletContext())
                .thenReturn(servletContext);
        when(request.getServletContext()).thenReturn(servletContext);
    }

    @Test
    public void testAdminRole() throws IOException, ServletException {
        User user = new User();
        user.setRole(Role.ADMIN);
        when(session.getAttribute(UserParameters.USER)).thenReturn(user);
        when(request.getSession()).thenReturn(session);
        when(request.getMethod()).thenReturn("GET");
        adminOrdersServlet.service(request, response);
        verify(request, never()).getRequestDispatcher(Path.PAGE__SIGN_IN);
        verify(response, never()).sendError(403);
    }

    @Test
    public void testClientRole() throws IOException, ServletException {
        User user = new User();
        user.setRole(Role.CLIENT);
        when(session.getAttribute(UserParameters.USER)).thenReturn(user);
        when(request.getSession()).thenReturn(session);
        when(request.getMethod()).thenReturn("GET");
        adminOrdersServlet.service(request, response);
        verify(response).sendError(403);
    }

    @Test
    public void testVisitorRole() throws IOException, ServletException {
        User user = new User();
        user.setRole(Role.VISITOR);
        when(session.getAttribute(UserParameters.USER)).thenReturn(user);
        when(request.getSession()).thenReturn(session);
        when(request.getMethod()).thenReturn("GET");
        adminOrdersServlet.service(request, response);
        verify(request).getRequestDispatcher(Path.PAGE__SIGN_IN);
    }

    @AfterClass
    public static void dropDatabase() throws DAOException {
        DatabaseManager.drop(DatabaseParameters.TEST_DB_NAME);
    }

    private Connection getConnection() throws ClassNotFoundException,
            SQLException {
        Class.forName(DatabaseParameters.DB_DRIVER_NAME);
        Connection connection = DriverManager.getConnection(
                DatabaseParameters.TEST_DB_URL, DatabaseParameters.USER,
                DatabaseParameters.PASSWORD);
        connection.setAutoCommit(false);
        return connection;
    }
}
