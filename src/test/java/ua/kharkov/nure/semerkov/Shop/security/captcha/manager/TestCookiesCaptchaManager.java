package ua.kharkov.nure.semerkov.Shop.security.captcha.manager;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;

import ua.kharkov.nure.semerkov.Shop.parameter.ServiceParameters;
import ua.kharkov.nure.semerkov.Shop.parameter.UserParameters;

/**
 * Test Case for check <code>CookiesCaptchaManager</code> class.
 * 
 * @author Volodymyr_Semerkov
 * 
 */
public class TestCookiesCaptchaManager {
	private static HttpServletRequest request;
	private static HttpServletResponse response;
	private static CaptchaManager captchaManager;
	private static Cookie[] cookies;
	private static Cookie cookie;

	@Before
	public void create() {
		request = mock(HttpServletRequest.class);
		response = mock(HttpServletResponse.class);
		captchaManager = new CookiesCaptchaManager(1800, 300);
		cookies = new Cookie[1];
		cookie = new Cookie(ServiceParameters.CAPTCHA__ID, String.valueOf(1));
		cookies[0] = cookie;
		when(request.getCookies()).thenReturn(cookies);
		captchaManager.setCaptchaCode(request, response, "1111");
	}

	@Test
	public void testCaptchaManagerByCorrectData() {
		when(request.getParameter(UserParameters.USER__CAPTCHA)).thenReturn(
				"1111");
		assertTrue(captchaManager.validateCaptcha(request));
	}

	@Test
	public void testCaptchaManagerByIncorrectData() {
		when(request.getParameter(UserParameters.USER__CAPTCHA)).thenReturn(
				"0000");
		assertFalse(captchaManager.validateCaptcha(request));
	}
}
