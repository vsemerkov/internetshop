CREATE DATABASE  IF NOT EXISTS `shop_semerkov`;

USE `shop_semerkov`;

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `manufacturers`;

CREATE TABLE `manufacturers` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `price` double NOT NULL DEFAULT '0',
  `number` INT NOT NULL DEFAULT '0',
  `photo_file_name` varchar(255) DEFAULT NULL,
  `manufacturer_id` bigint(20) NOT NULL,
  `category_id` bigint(20) NOT NULL,
  `info` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `manufacturer_id_idx` (`manufacturer_id`),
  KEY `category_idx` (`category_id`),
 CONSTRAINT `manufacturer` FOREIGN KEY (`manufacturer_id`) REFERENCES `manufacturers` (`id`) ON UPDATE CASCADE  ON DELETE RESTRICT,
 CONSTRAINT `category` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON UPDATE CASCADE ON DELETE RESTRICT
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `password` varchar(128) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `subscribe` bit(1) NOT NULL DEFAULT b'0',
  `role` varchar(45) NOT NULL,
  `image_name` varchar(255) DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `attempt_number` int(11) NOT NULL DEFAULT '0',
  `unlocking_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8;

CREATE  TABLE `orders` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `order_status` VARCHAR(45) CHARACTER SET 'utf8' NOT NULL,
  `canceled_order_status` VARCHAR(45) CHARACTER SET 'utf8' NULL,
  `order_date` TIMESTAMP NOT NULL,
  `user_id` BIGINT NOT NULL,
  `delivery_type` varchar(45) NOT NULL,
  `payment_type` varchar(45) NOT NULL,
  `address` VARCHAR(256) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `user_id_idx` (`user_id` ASC),
  CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE ON DELETE RESTRICT
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8;

CREATE  TABLE `ordered_products` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `order_id` BIGINT NOT NULL,
  `product_id` BIGINT NOT NULL,
  `price` DOUBLE NOT NULL,
  `number` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `order_id_idx` (`order_id` ASC),
  INDEX `product_id_idx` (`product_id` ASC),
  CONSTRAINT `order_id` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON UPDATE CASCADE ON DELETE RESTRICT,
  CONSTRAINT `product_id` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON UPDATE CASCADE ON DELETE RESTRICT
)
ENGINE=InnoDB
DEFAULT CHARSET = utf8;

INSERT INTO categories(id, name) VALUES(1, 'printer');
INSERT INTO categories(id, name) VALUES(2, 'scanner');
INSERT INTO categories(id, name) VALUES(3, 'MFP');

INSERT INTO manufacturers(id, name) VALUES(1, 'Canon');
INSERT INTO manufacturers(id, name) VALUES(2, 'Epson');
INSERT INTO manufacturers(id, name) VALUES(3, 'HP');
INSERT INTO manufacturers(id, name) VALUES(4, 'Samsung');
INSERT INTO manufacturers(id, name) VALUES(5, 'Brother');
INSERT INTO manufacturers(id, name) VALUES(6, 'Xerox');

INSERT INTO products(name, price, number, photo_file_name, manufacturer_id, category_id, info) VALUES('ML-2165', 776, 10, 'printer1.jpg', 4, 1, 'Samsung ML-2165 – устройство, сочетающее в себе экономическую эффективность и производительность, а также стильный и компактный дизайн в сочетании с легкостью управления.');
INSERT INTO products(name, price, number, photo_file_name, manufacturer_id, category_id, info) VALUES('SCX-4650N', 1757, 5, 'printer2.jpg', 4, 3, 'Универсальный принтер Samsung SCX-4650N является устройством типа "3-в-1", которое обеспечивает высокую эффективность работы любого офиса. Благодаря режимам печати, копирования и сканирования, SCX-4650N может стать незаменимым инструментом любого офиса.');
INSERT INTO products(name, price, number, photo_file_name, manufacturer_id, category_id, info) VALUES('L800', 2046, 3, 'printer5.jpg', 2, 1, 'Epson L800 — уникальное 6-цветное устройство со встроенными большими емкостями для чернил, специально созданное для тех, кому необходима экономичная печать фотографий и СD/DVD дисков – для фотосалонов, фотолабораторий, частных фотографов, которые работают на дому, и многих других бизнес-пользователей.');
INSERT INTO products(name, price, number, photo_file_name, manufacturer_id, category_id, info) VALUES('i-SENSYS MF3010', 1427, 7, 'printer6.jpg', 1, 3, 'Элегантный и стильный, полностью черный корпус MF3010 заключает интеллектуальное многофункциональное лазерное устройство «3 в 1». Высококачественный монохромный принтер, сканер и копир в одном компактном корпусе. Быстрое и экономичное, это эффектное устройство легко помещается на столе. Оно идеально для персонального использования или небольших и домашних офисов, где требуется не только принтер, но также и экономия времени и денег.');
INSERT INTO products(name, price, number, photo_file_name, manufacturer_id, category_id, info) VALUES('i-SENSYS LBP6020', 825, 10, 'printer2.jpg', 1, 1, 'Компактный настольный лазерный принтер с функциями энергосбережения.');
INSERT INTO products(name, price, number, photo_file_name, manufacturer_id, category_id, info) VALUES('LaserJet Pro M1132', 1518, 12, 'printer4.jpg', 3, 3, 'HP LaserJet Pro M1132 MFP сочетает функции печати, копирования и сканирования.');
INSERT INTO products(name, price, number, photo_file_name, manufacturer_id, category_id, info) VALUES('DCP-7057R', 1262, 5, 'printer6.jpg', 5, 3, 'Лазерный принтер, копировальный аппарат и цветной планшетный сканер.');
INSERT INTO products(name, price, number, photo_file_name, manufacturer_id, category_id, info) VALUES('Travel Scanner 100', 1889, 10, 'scanner.jpg', 6, 2, 'Travel Scanner 100 - является дополнением существующего модельного ряда сканеров Xerox.');
INSERT INTO products(name, price, number, photo_file_name, manufacturer_id, category_id, info) VALUES('CanoScan LIDE 210', 875, 5, 'scanner.jpg', 1, 2, 'CanoScan LiDE 210 осуществляет чёткое и качественное сканирование с высокой скоростью (сканирование документа A4 с разрешением 300 точек на дюйм, составляет около 10 секунд).');
INSERT INTO products(name, price, number, photo_file_name, manufacturer_id, category_id, info) VALUES('Perfection V500', 2178, 10, 'scanner.jpg', 2, 2, NULL);
INSERT INTO products(name, price, number, photo_file_name, manufacturer_id, category_id, info) VALUES('Perfection W800', 3000, 5, 'printer5.jpg', 1, 1, NULL);