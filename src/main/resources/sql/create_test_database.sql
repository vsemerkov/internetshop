CREATE DATABASE  IF NOT EXISTS `test_shop_semerkov`;

USE `test_shop_semerkov`;

DROP TABLE IF EXISTS `users`;
DROP TABLE IF EXISTS `products`;
DROP TABLE IF EXISTS `categories`;
DROP TABLE IF EXISTS `manufacturers`;

CREATE TABLE `categories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8;

CREATE TABLE `manufacturers` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8;

CREATE TABLE `products` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `price` double NOT NULL DEFAULT '0',
  `number` INT NOT NULL DEFAULT '0',
  `photo_file_name` varchar(255) DEFAULT NULL,
  `manufacturer_id` bigint(20) NOT NULL,
  `category_id` bigint(20) NOT NULL,
  `info` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `manufacturer_id_idx` (`manufacturer_id`),
  KEY `category_idx` (`category_id`),
  CONSTRAINT `manufacturer` FOREIGN KEY (`manufacturer_id`) REFERENCES `manufacturers` (`id`) ON UPDATE CASCADE  ON DELETE RESTRICT,
  CONSTRAINT `category` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON UPDATE CASCADE ON DELETE RESTRICT
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8;

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `password` varchar(128) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `subscribe` bit(1) NOT NULL DEFAULT b'0',
  `role` varchar(45) NOT NULL,
  `image_name` varchar(255) DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `attempt_number` int(11) NOT NULL DEFAULT '0',
  `unlocking_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8;

CREATE  TABLE `orders` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `order_status` VARCHAR(45) CHARACTER SET 'utf8' NOT NULL,
  `canceled_order_status` VARCHAR(45) CHARACTER SET 'utf8' NULL,
  `order_date` TIMESTAMP NOT NULL,
  `user_id` BIGINT NOT NULL,
  `delivery_type` varchar(45) NOT NULL,
  `payment_type` varchar(45) NOT NULL,
  `address` VARCHAR(256) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `user_id_idx` (`user_id` ASC),
  CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE ON DELETE RESTRICT
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8;

CREATE  TABLE `ordered_products` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `order_id` BIGINT NOT NULL,
  `product_id` BIGINT NOT NULL,
  `price` DOUBLE NOT NULL,
  `number` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `order_id_idx` (`order_id` ASC),
  INDEX `product_id_idx` (`product_id` ASC),
  CONSTRAINT `order_id` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON UPDATE CASCADE ON DELETE RESTRICT,
  CONSTRAINT `product_id` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON UPDATE CASCADE ON DELETE RESTRICT
)
ENGINE=InnoDB
DEFAULT CHARSET = utf8;