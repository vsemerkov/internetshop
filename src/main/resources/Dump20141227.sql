CREATE DATABASE  IF NOT EXISTS `shop_semerkov` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `shop_semerkov`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: localhost    Database: shop_semerkov
-- ------------------------------------------------------
-- Server version	5.5.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (4,'inkjet'),(1,'MFP'),(2,'printer'),(3,'scanner');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `manufacturers`
--

DROP TABLE IF EXISTS `manufacturers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manufacturers` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `manufacturers`
--

LOCK TABLES `manufacturers` WRITE;
/*!40000 ALTER TABLE `manufacturers` DISABLE KEYS */;
INSERT INTO `manufacturers` VALUES (6,'Brother'),(1,'Canon'),(2,'Epson'),(3,'HP'),(4,'Samsung'),(5,'Xerox');
/*!40000 ALTER TABLE `manufacturers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordered_products`
--

DROP TABLE IF EXISTS `ordered_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ordered_products` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `price` double NOT NULL,
  `number` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `order_id_idx` (`order_id`),
  KEY `product_id_idx` (`product_id`),
  CONSTRAINT `order` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `product` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ordered_products`
--

LOCK TABLES `ordered_products` WRITE;
/*!40000 ALTER TABLE `ordered_products` DISABLE KEYS */;
INSERT INTO `ordered_products` VALUES (2,5,2,1757,1),(3,6,2,1757,1),(4,6,4,1427,1),(5,7,1,776,1),(6,7,2,1757,5),(7,8,2,1757,1),(8,8,4,1427,4),(9,9,2,1757,1),(62,59,2,1757,1),(63,60,2,1757,1),(64,61,7,1262,4),(65,61,6,1518,1),(66,61,4,1427,1),(67,61,1,776,1),(70,63,2,1757,2),(71,63,4,1427,1),(72,63,6,1518,1),(73,64,2,1757,3),(74,65,4,1427,2),(75,65,6,1518,5),(76,66,7,1262,1),(77,67,2,1757,8),(78,67,4,1427,7),(79,67,6,1518,7),(80,67,7,1262,10),(81,67,1,776,20),(82,68,2,1757,1),(83,69,2,1757,2),(84,69,4,1427,1),(85,70,6,1518,3);
/*!40000 ALTER TABLE `ordered_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_status` varchar(45) NOT NULL,
  `canceled_order_status` varchar(45) DEFAULT NULL,
  `order_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` bigint(20) NOT NULL,
  `delivery_type` varchar(45) NOT NULL,
  `payment_type` varchar(45) NOT NULL,
  `address` varchar(256) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `user_id_idx` (`user_id`),
  CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (5,'COMPLETED',NULL,'2014-06-19 03:28:39',11,'COURIER','CASH','fdfsfdfs'),(6,'CANCELED','SHOP','2014-06-19 03:28:54',11,'COURIER','NON_CASH','rtrttrtrtt'),(7,'COMPLETED',NULL,'2014-06-18 06:31:26',11,'SELF_DELIVERY','NON_CASH','г. Харьков ул. Космическая, 21'),(8,'EXECUTION',NULL,'2014-06-06 00:22:12',11,'COURIER','CASH','?????????'),(9,'CANCELED','SHOP','2014-06-07 23:29:39',11,'COURIER','CASH','ddadasdsa'),(59,'COMPLETED',NULL,'2014-06-07 23:30:39',11,'COURIER','CASH','fgfdgfg'),(60,'EXECUTION',NULL,'2014-06-08 00:21:39',11,'COURIER','CASH','rrrrrrrrrrrrrrrrrrrrrrr'),(61,'EXECUTION',NULL,'2014-06-08 00:21:39',11,'COURIER','CASH','fddfdfdfdf'),(63,'CANCELED','CLIENT','2014-06-08 00:21:47',11,'COURIER','CASH','ttttttttttttttttt'),(64,'CANCELED','CLIENT','2014-06-07 23:25:31',11,'COURIER','CASH','rrfrfrfrf'),(65,'EXECUTION',NULL,'2014-06-12 02:15:34',11,'COURIER','CASH','vvvvvvvvvvvvvv'),(66,'EXECUTION',NULL,'2014-06-12 02:15:34',11,'COURIER','NON_CASH','4476070035905272'),(67,'COMPLETED',NULL,'2014-06-12 07:38:49',11,'COURIER','CASH','fgdfgdgdfgdfgdgdg'),(68,'EXECUTION',NULL,'2014-06-12 02:15:34',17,'COURIER','CASH','fffffffffffffffffffffff'),(69,'CANCELED','CLIENT','2014-06-18 06:04:25',11,'COURIER','CASH','г. Харьков пр. Ленина, 14'),(70,'CANCELED','CLIENT','2014-06-19 03:26:56',11,'COURIER','CASH','г. Харьков пр. Ленина, 145');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `price` double NOT NULL DEFAULT '0',
  `number` int(11) NOT NULL DEFAULT '0',
  `photo_file_name` varchar(255) DEFAULT NULL,
  `manufacturer_id` bigint(20) NOT NULL,
  `category_id` bigint(20) NOT NULL,
  `info` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `manufacturer_id_idx` (`manufacturer_id`),
  KEY `category_idx` (`category_id`),
  CONSTRAINT `category` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `manufacturer` FOREIGN KEY (`manufacturer_id`) REFERENCES `manufacturers` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,'ML-2165',776,29,'printer1.jpg',4,2,'Samsung ML-2165 – устройство, сочетающее в себе экономическую эффективность и производительность, а также стильный и компактный дизайн в сочетании с легкостью управления.'),(2,'SCX-4650N',1757,17,'printer2.jpg',4,1,'Универсальный принтер Samsung SCX-4650N является устройством типа \"3-в-1\", которое обеспечивает высокую эффективность работы любого офиса. Благодаря режимам печати, копирования и сканирования, SCX-4650N может стать незаменимым инструментом любого офиса.'),(3,'L800',2046,32,'printer5.jpg',2,2,'Epson L800 — уникальное 6-цветное устройство со встроенными большими емкостями для чернил, специально созданное для тех, кому необходима экономичная печать фотографий и СD/DVD дисков – для фотосалонов, фотолабораторий, частных фотографов, которые работают на дому, и многих других бизнес-пользователей.'),(4,'i-SENSYS MF3010',1427,21,'printer6.jpg',1,1,'Элегантный и стильный, полностью черный корпус MF3010 заключает интеллектуальное многофункциональное лазерное устройство «3 в 1». Высококачественный монохромный принтер, сканер и копир в одном компактном корпусе. Быстрое и экономичное, это эффектное устройство легко помещается на столе. Оно идеально для персонального использования или небольших и домашних офисов, где требуется не только принтер, но также и экономия времени и денег.'),(5,'i-SENSYS LBP6020',825,20,'printer2.jpg',1,2,'Компактный настольный лазерный принтер с функциями энергосбережения.'),(6,'LaserJet Pro M1132',1518,19,'printer4.jpg',3,1,'HP LaserJet Pro M1132 MFP сочетает функции печати, копирования и сканирования.'),(7,'DCP-7057R',1262,5,'printer6.jpg',6,1,'Лазерный принтер, копировальный аппарат и цветной планшетный сканер.'),(8,'Travel Scanner 100',1889,10,'scanner.jpg',5,3,'Travel Scanner 100 - является дополнением существующего модельного ряда сканеров Xerox.'),(9,'CanoScan LIDE 210',875,20,'scanner.jpg',1,3,'CanoScan LiDE 210 осуществляет чёткое и качественное сканирование с высокой скоростью (сканирование документа A4 с разрешением 300 точек на дюйм, составляет около 10 секунд).'),(10,'Perfection V500',2178,10,'scanner.jpg',2,3,NULL),(11,'Perfection W800',3000,17,'printer5.jpg',1,2,NULL);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `password` varchar(128) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `subscribe` bit(1) NOT NULL DEFAULT b'0',
  `role` varchar(45) NOT NULL,
  `image_name` varchar(255) DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `attempt_number` int(11) NOT NULL DEFAULT '0',
  `unlocking_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (3,'petrov@gmail.com','a0b4975197d4e4b2b2880d40aa159d71','Petr','Petrov','\0','CLIENT',NULL,'2013-11-04 10:14:15',0,NULL),(4,'vasiliev@gmail.com','a0b4975197d4e4b2b2880d40aa159d71','Stepan','Vasiliev','','CLIENT',NULL,'2013-11-04 10:14:15',0,NULL),(11,'semerkoff@gmail.com','61707455e9b9010a338dc99624695520','Vladimir','Semerkov','\0','CLIENT','gmail.comsemerkoff.png','2014-12-27 01:56:11',0,NULL),(12,'semerkoff3@gmail.com','a0b4975197d4e4b2b2880d40aa159d71','efff','dfff','','CLIENT','gmail.comsemerkoff3.png','2013-11-14 06:12:30',0,NULL),(13,'kira@gmail.com','a0b4975197d4e4b2b2880d40aa159d71','Kira','Ivanova','\0','CLIENT',NULL,'2014-05-16 08:54:56',0,NULL),(16,'esemerkoff@gmail.com','2476f5c9bda624b6a650c16ec2b573ff','Vladimir','Semerkov','\0','CLIENT',NULL,'2014-06-09 19:19:47',0,NULL),(17,'admin@gmail.com','9c184b16d2c9fc6bd6a920e115770f9c','Vladimir','Semerkov','\0','ADMIN',NULL,'2014-06-19 03:27:10',0,NULL),(18,'ivanov@gmail.com','2490de5737443310439955050c331669','Ivan','Ivanov','','CLIENT',NULL,'2014-06-09 21:50:51',0,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-12-27  6:19:10
