package ua.kharkov.nure.semerkov.Shop.dao.mapper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import ua.kharkov.nure.semerkov.Shop.parameter.MapperParameters;
import ua.kharkov.nure.semerkov.Shop.entity.Category;
import ua.kharkov.nure.semerkov.Shop.entity.Manufacturer;
import ua.kharkov.nure.semerkov.Shop.entity.Product;

/**
 * Product mapper.
 *
 * @author Volodymyr_Semrkov
 */
public class ProductMapper {
    public static Product unMapProduct(ResultSet rs) throws SQLException {
        Product product = new Product(rs.getLong(MapperParameters.ID));
        product.setName(rs.getString(MapperParameters.PRODUCT__NAME));
        product.setPrice(rs.getDouble(MapperParameters.PRODUCT__PRICE));
        product.setNumber(rs.getInt(MapperParameters.PRODUCT__NUMBER));
        product.setPhotoFileName(rs
                .getString(MapperParameters.PRODUCT__PHOTO_FILE_NAME));
        long manufacturerId = rs
                .getLong(MapperParameters.PRODUCT__MANUFACTURER_ID);
        String manufacturerName = rs
                .getString(MapperParameters.PRODUCT__MANUFACTURER_NAME);
        product.setManufacturer(new Manufacturer(manufacturerId,
                manufacturerName));
        long categoryId = rs.getLong(MapperParameters.PRODUCT__CATEGORY_ID);
        String categoryName = rs
                .getString(MapperParameters.PRODUCT__CATEGORY_NAME);
        product.setCategory(new Category(categoryId, categoryName));
        product.setInfo(rs.getString(MapperParameters.PRODUCT__INFO));
        return product;
    }

    public static void mapProduct(Product product, PreparedStatement pstmt)
            throws SQLException {
        pstmt.setString(1, product.getName());
        pstmt.setDouble(2, product.getPrice());
        pstmt.setInt(3, product.getNumber());
        pstmt.setString(4, product.getPhotoFileName());
        pstmt.setLong(5, product.getManufacturer().getId());
        pstmt.setLong(6, product.getCategory().getId());
        pstmt.setString(7, product.getInfo());
    }
}
