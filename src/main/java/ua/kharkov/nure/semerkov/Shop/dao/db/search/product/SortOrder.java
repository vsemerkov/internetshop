package ua.kharkov.nure.semerkov.Shop.dao.db.search.product;

/**
 * The <code>SortOrder</code> class contains the enumeration of sort orders.
 * 
 * @author Volodymyr_Semerkov
 * 
 */
public enum SortOrder {
	ASC, DESC
}
