package ua.kharkov.nure.semerkov.Shop.entity;

import java.io.Serializable;

/**
 * The <code>OrderedProduct</code> class represents ordered product data.
 *
 * @author Volodymyr_Semerkov
 */
public class OrderedProduct implements Serializable, Cloneable {
    private static final long serialVersionUID = -8756849461552098404L;
    private long id;
    private long orderId;
    private long productId;
    private double price;
    private int number;

    public OrderedProduct() {
    }

    public OrderedProduct(long id) {
        this.id = id;
    }

    public OrderedProduct(long id, double price, int number) {
        this(id);
        this.price = price;
        this.number = number;
    }

    public OrderedProduct(long id, long orderId, long productId, double price,
                          int number) {
        this(id, price, number);
        this.orderId = orderId;
        this.productId = productId;
    }

    public OrderedProduct(long orderId, long productId, double price, int number) {
        this.orderId = orderId;
        this.productId = productId;
        this.price = price;
        this.number = number;
    }

    public long getId() {
        return id;
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public long getProductId() {
        return productId;
    }

    public double getPrice() {
        return price;
    }

    public int getNumber() {
        return number;
    }

    public double getTotalSum() {
        return price * number;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("OrderedProduct [id=");
        sb.append(id);
        sb.append(", price=");
        sb.append(price);
        sb.append(", number=");
        sb.append(number);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || (obj.getClass() != this.getClass()))
            return false;
        OrderedProduct otherProduct = (OrderedProduct) obj;
        return this.getId() == otherProduct.getId();
    }

    @Override
    public int hashCode() {
        return new Long(this.getId()).hashCode();
    }

    @Override
    public OrderedProduct clone() throws CloneNotSupportedException {
        return (OrderedProduct) super.clone();
    }
}
