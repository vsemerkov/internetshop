package ua.kharkov.nure.semerkov.Shop.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ua.kharkov.nure.semerkov.Shop.dao.OrderedProductDAO;
import ua.kharkov.nure.semerkov.Shop.dao.db.jdbc.JdbcConnectionHolder;
import ua.kharkov.nure.semerkov.Shop.dao.mapper.OrderedProductMapper;
import ua.kharkov.nure.semerkov.Shop.dao.exception.DAOException;
import ua.kharkov.nure.semerkov.Shop.db.DatabaseManager;
import ua.kharkov.nure.semerkov.Shop.entity.OrderedProduct;

/**
 * Class for access to ordered product data.
 * 
 * @author Volodymyr_Semrkov
 * 
 */
public class MysqlOrderedProductDAO implements OrderedProductDAO {
	private static final String SQL__INSERT_ORDERED_PRODUCT = "INSERT INTO ordered_products(order_id, product_id, price, number) "
			+ "VALUES(?, ?, ?, ?)";
	private static final String SQL__REMOVE_ORDERED_PRODUCT_BY_ID = "DELETE FROM ordered_products WHERE id=?";
	private static final String SQL__REMOVE_ORDERED_PRODUCT_BY_ORDER_ID = "DELETE FROM ordered_products WHERE order_id=?";
	private static final String SQL__FIND_ORDERED_PRODUCT_BY_ORDER_ID = "SELECT * FROM ordered_products WHERE order_id=?";

	@Override
	public boolean addOrderedProduct(OrderedProduct orderedProduct)
			throws DAOException {
		PreparedStatement pstmt = null;
		try {
			Connection connection = JdbcConnectionHolder.getConnection();
			pstmt = connection.prepareStatement(SQL__INSERT_ORDERED_PRODUCT);
			OrderedProductMapper.mapOrderedProduct(orderedProduct, pstmt);
			return pstmt.executeUpdate() != 1;
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DatabaseManager.closeStatement(pstmt);
		}
	}

	@Override
	public boolean addOrderedProducts(List<OrderedProduct> orderedProducts)
			throws DAOException {
		PreparedStatement pstmt = null;
		try {
			Connection connection = JdbcConnectionHolder.getConnection();
			pstmt = connection.prepareStatement(SQL__INSERT_ORDERED_PRODUCT);
			for (OrderedProduct orderedProduct : orderedProducts) {
				OrderedProductMapper.mapOrderedProduct(orderedProduct, pstmt);
				pstmt.addBatch();
			}
			return pstmt.executeBatch().length == orderedProducts.size();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DatabaseManager.closeStatement(pstmt);
		}
	}

	@Override
	public boolean removeOrderedProduct(long id) throws DAOException {
		PreparedStatement pstmt = null;
		try {
			Connection connection = JdbcConnectionHolder.getConnection();
			pstmt = connection
					.prepareStatement(SQL__REMOVE_ORDERED_PRODUCT_BY_ID);
			pstmt.setLong(1, id);
			return pstmt.executeUpdate() != 1;
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DatabaseManager.closeStatement(pstmt);
		}
	}

	@Override
	public boolean removeOrderedProducts(long orderId) throws DAOException {
		PreparedStatement pstmt = null;
		try {
			Connection connection = JdbcConnectionHolder.getConnection();
			pstmt = connection
					.prepareStatement(SQL__REMOVE_ORDERED_PRODUCT_BY_ORDER_ID);
			pstmt.setLong(1, orderId);
			return pstmt.executeUpdate() != 1;
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DatabaseManager.closeStatement(pstmt);
		}
	}

	@Override
	public List<OrderedProduct> getOrderedProducts(long orderId)
			throws DAOException {
		List<OrderedProduct> orderedProducts = new ArrayList<OrderedProduct>();
		PreparedStatement pstmt = null;
		try {
			Connection connection = JdbcConnectionHolder.getConnection();
			pstmt = connection
					.prepareStatement(SQL__FIND_ORDERED_PRODUCT_BY_ORDER_ID);
			pstmt.setLong(1, orderId);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				orderedProducts.add(OrderedProductMapper
						.unMapOrderedProduct(rs));
			}
			return orderedProducts;
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DatabaseManager.closeStatement(pstmt);
		}
	}
}
