package ua.kharkov.nure.semerkov.Shop.listener;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;

import ua.kharkov.nure.semerkov.Shop.parameter.ServiceParameters;
import ua.kharkov.nure.semerkov.Shop.security.captcha.manager.CaptchaManager;
import ua.kharkov.nure.semerkov.Shop.security.captcha.manager.CookiesCaptchaManager;
import ua.kharkov.nure.semerkov.Shop.security.captcha.manager.HiddenCaptchaManager;
import ua.kharkov.nure.semerkov.Shop.security.captcha.manager.SessionCaptchaManager;
import ua.kharkov.nure.semerkov.Shop.dao.CategoryDAO;
import ua.kharkov.nure.semerkov.Shop.dao.ManufacturerDAO;
import ua.kharkov.nure.semerkov.Shop.dao.OrderDAO;
import ua.kharkov.nure.semerkov.Shop.dao.OrderedProductDAO;
import ua.kharkov.nure.semerkov.Shop.dao.ProductDAO;
import ua.kharkov.nure.semerkov.Shop.dao.UserDAO;
import ua.kharkov.nure.semerkov.Shop.dao.mysql.MysqlCategoryDAO;
import ua.kharkov.nure.semerkov.Shop.dao.mysql.MysqlManufacturerDAO;
import ua.kharkov.nure.semerkov.Shop.dao.mysql.MysqlOrderDAO;
import ua.kharkov.nure.semerkov.Shop.dao.mysql.MysqlOrderedProductDAO;
import ua.kharkov.nure.semerkov.Shop.dao.mysql.MysqlProductDAO;
import ua.kharkov.nure.semerkov.Shop.dao.mysql.MysqlUserDAO;
import ua.kharkov.nure.semerkov.Shop.dao.exception.DAOException;
import ua.kharkov.nure.semerkov.Shop.entity.User;
import ua.kharkov.nure.semerkov.Shop.service.CategoryService;
import ua.kharkov.nure.semerkov.Shop.service.OrderService;
import ua.kharkov.nure.semerkov.Shop.service.ProductImageService;
import ua.kharkov.nure.semerkov.Shop.service.UserImageService;
import ua.kharkov.nure.semerkov.Shop.service.ManufacturerService;
import ua.kharkov.nure.semerkov.Shop.service.ProductService;
import ua.kharkov.nure.semerkov.Shop.service.UserService;

/**
 * Context listener.
 * 
 * @author Volodymyr_Semerkov
 * 
 */
public class ContextListener implements ServletContextListener {
	private static final Logger log = Logger.getLogger(ContextListener.class);

	@Override
	public void contextInitialized(ServletContextEvent event) {
		if (log.isDebugEnabled()) {
			log.debug("Servlet context initialization starts");
		}
		ServletContext servletContext = event.getServletContext();
		setUserServiceAttribute(servletContext);
		setCaptchaManagerAttribute(servletContext);
		setUserImageServiceAttribute(servletContext);
		setLocaleAttribute(servletContext);
		setCategoryServiceAttribute(servletContext);
		setManufacturerServiceAttribute(servletContext);
		setProductServiceAttribute(servletContext);
		setProductImageServiceAttribute(servletContext);
		setOrderServiceAttribute(servletContext);
		if (log.isDebugEnabled()) {
			log.debug("Servlet context initialization finished");
		}
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		if (log.isDebugEnabled()) {
			log.debug("Servlet context destruction starts");
			log.debug("Servlet context destruction finished");
		}
	}

	private void setUserServiceAttribute(ServletContext servletContext) {
		UserDAO userDAO = new MysqlUserDAO();
		UserService userService = new UserService(userDAO);
		try {
			createUsers(userService);
		} catch (DAOException e) {
			log.error(e.getMessage());
			throw new IllegalStateException(e.getMessage());
		}
		servletContext.setAttribute(ServiceParameters.USER__SERVICE,
				userService);
		log.debug("UserDAO was created");
	}

	private void createUsers(UserService userService) throws DAOException {
		if (userService.get("ivanov@gmail.com") == null)
			userService.add(new User("Ivan", "Ivanov", "ivanov@gmail.com",
					"Qwerty_123", true));
		if (userService.get("petrov@gmail.com") == null)
			userService.add(new User("Petr", "Petrov", "petrov@gmail.com",
					"Qwerty_124", false));
		if (userService.get("vasiliev@gmail.com") == null)
			userService.add(new User("Stepan", "Vasiliev",
					"vasiliev@gmail.com", "Qwerty_125", true));
	}

	private void setCaptchaManagerAttribute(ServletContext servletContext) {
		String captchaMode = servletContext
				.getInitParameter(ServiceParameters.CAPTCHA__MODE);
		log.trace("captchaMode --> " + captchaMode);
		int captchaTimeout = Integer.parseInt(servletContext
				.getInitParameter(ServiceParameters.CAPTCHA__TIME_OUT));
		log.trace("captchaTimeout --> " + captchaTimeout + " seconds.");
		int checkPeriod = Integer.parseInt(servletContext
				.getInitParameter(ServiceParameters.CHECK_TIME));
		log.trace("checkPeriod --> " + checkPeriod + " seconds.");
		CaptchaManager captchaManager = null;
		switch (captchaMode) {
		case ServiceParameters.CAPTCHA__MODE_SESSION:
			captchaManager = new SessionCaptchaManager(captchaTimeout);
			break;
		case ServiceParameters.CAPTCHA__MODE_COOKIES:
			captchaManager = new CookiesCaptchaManager(captchaTimeout,
					checkPeriod);
			break;
		case ServiceParameters.CAPTCHA__MODE_HIDDEN:
			captchaManager = new HiddenCaptchaManager(captchaTimeout,
					checkPeriod);
			break;
		}
		if (captchaManager == null || captchaTimeout <= 0 || checkPeriod <= 0) {
			log.error("Captcha mode or timeout is incorrect");
			throw new IllegalStateException(
					"Captcha mode or timeout is incorrect");
		}
		servletContext.setAttribute(ServiceParameters.CAPTCHA__MANAGER,
				captchaManager);
	}

	private void setUserImageServiceAttribute(ServletContext servletContext) {
		String imageFolderPath = servletContext
				.getInitParameter(ServiceParameters.IMAGE__USER_FOLDER);
		String defaultImagePath = servletContext
				.getInitParameter(ServiceParameters.IMAGE__DEFAULT);
		int maxHeight = Integer.parseInt(servletContext
				.getInitParameter(ServiceParameters.IMAGE__MAX_HEIGHT));
		int maxWidth = Integer.parseInt(servletContext
				.getInitParameter(ServiceParameters.IMAGE__MAX_WIDTH));
		if (imageFolderPath == null || defaultImagePath == null) {
			log.error("Image paths are not correct");
			throw new IllegalStateException("Image paths are not correct");
		}
		defaultImagePath = servletContext.getRealPath("/") + defaultImagePath;
		UserImageService imageService = new UserImageService(imageFolderPath,
				defaultImagePath, maxHeight, maxWidth);
		servletContext.setAttribute(ServiceParameters.USER_IMAGE__SERVICE,
				imageService);
	}

	private void setLocaleAttribute(ServletContext servletContext) {
		String localesValue = servletContext
				.getInitParameter(ServiceParameters.SUPPORTED_LOCALES);
		log.trace("localesValue --> " + localesValue);
		if (localesValue == null || localesValue.isEmpty()) {
			log.error("Supported locales init parameter is empty");
			throw new IllegalStateException(
					"Supported locales init parameter is empty");
		} else {
			List<Locale> supportedLocales = new ArrayList<Locale>();
			StringTokenizer st = new StringTokenizer(localesValue);
			while (st.hasMoreTokens()) {
				String localeName = st.nextToken();
				supportedLocales.add(new Locale(localeName));
				log.debug("Application attribute set: supportedLocale --> "
						+ localeName);
			}
			servletContext.setAttribute(ServiceParameters.SUPPORTED_LOCALES,
					supportedLocales);
			String defaultLocaleName = servletContext
					.getInitParameter(ServiceParameters.DEFAULT_LOCALE);
			log.trace("defaultLocale --> " + defaultLocaleName);
			if (defaultLocaleName == null || defaultLocaleName.isEmpty()) {
				log.error("Default locale is incorrect");
				throw new IllegalStateException("Default locale is incorrect");
			}
			Locale defaultLocale = new Locale(defaultLocaleName);
			servletContext.setAttribute(ServiceParameters.DEFAULT_LOCALE,
					defaultLocale);
		}
	}

	private void setCategoryServiceAttribute(ServletContext servletContext) {
		CategoryDAO categoryDAO = new MysqlCategoryDAO();
		CategoryService categoryService = new CategoryService(categoryDAO);
		servletContext.setAttribute(ServiceParameters.CATEGORY__SERVICE,
				categoryService);
		log.debug("CategoryDAO was created");
	}

	private void setManufacturerServiceAttribute(ServletContext servletContext) {
		ManufacturerDAO manufacturerDAO = new MysqlManufacturerDAO();
		ManufacturerService manufacturerService = new ManufacturerService(
				manufacturerDAO);
		servletContext.setAttribute(ServiceParameters.MANUFACTURER__SERVICE,
				manufacturerService);
		log.debug("ManufacturerDAO was created");
	}

	private void setProductServiceAttribute(ServletContext servletContext) {
		ProductDAO productDAO = new MysqlProductDAO();
		ProductService productService = new ProductService(productDAO);
		servletContext.setAttribute(ServiceParameters.PRODUCT__SERVICE,
				productService);
		log.debug("ProductDAO was created");
	}

	private void setProductImageServiceAttribute(ServletContext servletContext) {
		String imageFolderPath = servletContext
				.getInitParameter(ServiceParameters.IMAGE__PRODUCT_FOLDER);
		String defaultImagePath = servletContext
				.getInitParameter(ServiceParameters.IMAGE__DEFAULT);
		if (imageFolderPath == null || defaultImagePath == null) {
			log.error("Image paths are not correct");
			throw new IllegalStateException("Image paths are not correct");
		}
		defaultImagePath = servletContext.getRealPath("/") + defaultImagePath;
		ProductImageService imageService = new ProductImageService(
				imageFolderPath, defaultImagePath);
		servletContext.setAttribute(ServiceParameters.PRODUCT_IMAGE__SERVICE,
				imageService);
	}

	private void setOrderServiceAttribute(ServletContext servletContext) {
		OrderDAO orderDAO = new MysqlOrderDAO();
		OrderedProductDAO orderedProductDAO = new MysqlOrderedProductDAO();
		OrderService orderService = new OrderService(orderDAO,
				orderedProductDAO);
		servletContext.setAttribute(ServiceParameters.ORDER__SERVICE,
				orderService);
		log.debug("OrderDAO was created");
		log.debug("OrderedProductDAO was created");
	}
}
