package ua.kharkov.nure.semerkov.Shop.dao.mapper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import ua.kharkov.nure.semerkov.Shop.Role;
import ua.kharkov.nure.semerkov.Shop.parameter.MapperParameters;
import ua.kharkov.nure.semerkov.Shop.entity.User;

/**
 * User mapper.
 * 
 * @author Volodymyr_Semrkov
 * 
 */
public class UserMapper {
	public static User unMapUser(ResultSet rs) throws SQLException {
		User user = new User(rs.getLong(MapperParameters.ID),
				rs.getString(MapperParameters.USER__EMAIL));
		user.setPassword(rs.getString(MapperParameters.USER__PASSWORD));
		user.setFirstName(rs.getString(MapperParameters.USER__FIRST_NAME));
		user.setLastName(rs.getString(MapperParameters.USER__LAST_NAME));
		user.setSubscribe(rs.getBoolean(MapperParameters.USER__SUBSCRIBE));
		user.setRole(Role.valueOf(rs.getString(MapperParameters.USER__ROLE)
				.toUpperCase()));
		user.setImageName(rs.getString(MapperParameters.USER__IMAGE_NAME));
		user.setLastLogin(rs
				.getTimestamp(MapperParameters.USER__LAST_LOGIN));
		user.setAttemptNumber(rs
				.getInt(MapperParameters.USER__ATTEMPT_NUMBER));
		user.setUnlockingDate(rs
				.getTimestamp(MapperParameters.USER__UNLOCKING_DATE));
		return user;
	}

	public static void mapUserForAdd(User user, PreparedStatement pstmt)
			throws SQLException {
		pstmt.setString(1, user.getEmail());
		pstmt.setString(2, user.getPassword());
		pstmt.setString(3, user.getFirstName());
		pstmt.setString(4, user.getLastName());
		pstmt.setBoolean(5, user.isSubscribe());
		pstmt.setString(6, user.getRole().toString());
		pstmt.setString(7, user.getImageName());
		if (user.getLastLogin() != null) {
			pstmt.setTimestamp(8, new Timestamp(user.getLastLogin().getTime()));
		} else {
			pstmt.setTimestamp(8, null);
		}
		pstmt.setInt(9, user.getAttemptNumber());
		if (user.getUnlockingDate() != null) {
			pstmt.setTimestamp(10, new Timestamp(user.getUnlockingDate()
					.getTime()));
		} else {
			pstmt.setTimestamp(10, null);
		}
	}

	public static void mapUserForUpdate(User user, PreparedStatement pstmt)
			throws SQLException {
		pstmt.setString(1, user.getPassword());
		pstmt.setString(2, user.getFirstName());
		pstmt.setString(3, user.getLastName());
		pstmt.setBoolean(4, user.isSubscribe());
		pstmt.setString(5, user.getRole().toString());
		pstmt.setString(6, user.getImageName());
		if (user.getLastLogin() != null) {
			pstmt.setTimestamp(7, new Timestamp(user.getLastLogin().getTime()));
		} else {
			pstmt.setTimestamp(7, null);
		}
		pstmt.setInt(8, user.getAttemptNumber());
		if (user.getUnlockingDate() != null) {
			pstmt.setTimestamp(9, new Timestamp(user.getUnlockingDate()
					.getTime()));
		} else {
			pstmt.setTimestamp(9, null);
		}
		pstmt.setString(10, user.getEmail());
	}
}
