package ua.kharkov.nure.semerkov.Shop.convert;

import ua.kharkov.nure.semerkov.Shop.entity.FormBean;
import ua.kharkov.nure.semerkov.Shop.entity.User;

/**
 * This class converts <code>FormBean</code> class object to
 * <code>UserInfo</code> class object.
 * 
 * @author Volodymyr_Semerkov
 * 
 */
public class FormBeanConverter {
	/**
	 * Converting <code>FormBean</code> class object to <code>UserInfo</code>
	 * class object.
	 * 
	 * @param formBean
	 * @return
	 */
	public static User getUser(FormBean formBean) {
		User user = new User(formBean.getFirstName(), formBean.getLastName(),
				formBean.getEmail(), formBean.getPassword(),
				formBean.isSubscribe());
		return user;
	}
}
