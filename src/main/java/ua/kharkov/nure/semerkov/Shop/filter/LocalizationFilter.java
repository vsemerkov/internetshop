package ua.kharkov.nure.semerkov.Shop.filter;

import java.io.IOException;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.kharkov.nure.semerkov.Shop.parameter.ServiceParameters;
import ua.kharkov.nure.semerkov.Shop.filter.localization.CookiesLocaleManager;
import ua.kharkov.nure.semerkov.Shop.filter.localization.LocaleManager;
import ua.kharkov.nure.semerkov.Shop.filter.localization.RequestLocaleWrapper;
import ua.kharkov.nure.semerkov.Shop.filter.localization.SessionLocaleManager;

/**
 * Localization filter.
 * 
 * @author Volodymyr_Semerkov
 * 
 */
public class LocalizationFilter implements Filter {
	private List<Locale> supportedLocales;
	private Locale defaultLocale;
	private LocaleManager localeManager;
	private static final Logger log = Logger
			.getLogger(LocalizationFilter.class);

	@SuppressWarnings("unchecked")
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		if (log.isDebugEnabled()) {
			log.debug("Filter initialization starts");
		}
		ServletContext servletContext = filterConfig.getServletContext();
		supportedLocales = (List<Locale>) servletContext
				.getAttribute(ServiceParameters.SUPPORTED_LOCALES);
		defaultLocale = (Locale) servletContext
				.getAttribute(ServiceParameters.DEFAULT_LOCALE);
		String localeManagerName = servletContext
				.getInitParameter(ServiceParameters.LOCALE_MANAGER);
		log.trace("localeManagerName --> " + localeManagerName);
		int cookiesTimeout;
		try {
			cookiesTimeout = Integer.parseInt(servletContext
					.getInitParameter(ServiceParameters.COOKIES_TIME_OUT));
		} catch (NumberFormatException e) {
			log.error("Cookies timeout is incorrect");
			throw new IllegalStateException("Cookies timeout is incorrect");
		}
		log.trace("cookiesTimeout --> " + cookiesTimeout + " seconds.");
		switch (localeManagerName) {
		case ServiceParameters.SESSION_MANAGER:
			localeManager = new SessionLocaleManager();
			break;
		case ServiceParameters.COOKIES_MANAGER:
			localeManager = new CookiesLocaleManager(cookiesTimeout);
			break;
		}
		if (localeManager == null) {
			log.error("Local manager parameter is incorrect");
			throw new IllegalStateException(
					"Local manager parameter is incorrect");
		}
		if (log.isDebugEnabled()) {
			log.debug("Filter initialization finished");
		}
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		if (log.isDebugEnabled()) {
			log.debug("Filter starts");
		}
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		Locale userLocale = getLocaleFromParameter(httpRequest);
		if (userLocale == null) {
			userLocale = localeManager.getLocale(httpRequest);
			if (userLocale == null) {
				userLocale = getLocaleFromBrowser(httpRequest);
				userLocale = userLocale == null ? defaultLocale : userLocale;
			}
		}
		localeManager.setLocale(userLocale, httpRequest, httpResponse);
		RequestLocaleWrapper requestWrapper = new RequestLocaleWrapper(
				httpRequest, userLocale);
		chain.doFilter(requestWrapper, httpResponse);
		log.debug("Filter finished");
	}

	@Override
	public void destroy() {
	}

	public Locale getLocaleFromParameter(HttpServletRequest request) {
		String localeParam = request
				.getParameter(ServiceParameters.LANG_PARAMETER);
		if (localeParam == null) {
			return null;
		}
		Locale locale = new Locale(localeParam);
		if (supportedLocales.contains(locale)) {
			return locale;
		}
		return null;
	}

	public Locale getLocaleFromBrowser(HttpServletRequest request) {
		Locale locale = null;
		Enumeration<Locale> browserLocales = request.getLocales();
		while (browserLocales.hasMoreElements()) {
			Locale browserLocale = browserLocales.nextElement();
			if (supportedLocales.contains(browserLocale)) {
				locale = browserLocale;
				break;
			} else {
				for (Locale supportedLocale : supportedLocales) {
					if (supportedLocale.getLanguage().equals(browserLocale)) {
						locale = browserLocale;
					}
				}
			}
		}
		return locale;
	}
}
