package ua.kharkov.nure.semerkov.Shop.servlets.order;

import org.apache.log4j.Logger;
import ua.kharkov.nure.semerkov.Shop.Path;
import ua.kharkov.nure.semerkov.Shop.Role;
import ua.kharkov.nure.semerkov.Shop.dao.exception.DAOException;
import ua.kharkov.nure.semerkov.Shop.entity.*;
import ua.kharkov.nure.semerkov.Shop.parameter.ErrorParameters;
import ua.kharkov.nure.semerkov.Shop.parameter.OrderParameters;
import ua.kharkov.nure.semerkov.Shop.parameter.ServiceParameters;
import ua.kharkov.nure.semerkov.Shop.parameter.UserParameters;
import ua.kharkov.nure.semerkov.Shop.service.OrderService;
import ua.kharkov.nure.semerkov.Shop.service.ProductService;
import ua.kharkov.nure.semerkov.Shop.service.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Servlet implementation class OrderServlet
 */
public class OrderServlet extends HttpServlet {
    private OrderService orderService;
    private ProductService productService;
    private UserService userService;
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(OrderServlet.class);

    /**
     * @see javax.servlet.http.HttpServlet#HttpServlet()
     */
    public OrderServlet() {
        super();
        if (log.isDebugEnabled()) {
            log.debug("Servlet creates");
        }
    }

    @Override
    public void init() throws ServletException {
        ServletContext servletContext = getServletContext();
        orderService = (OrderService) servletContext
                .getAttribute(ServiceParameters.ORDER__SERVICE);
        if (orderService == null) {
            log.error("Order service attribute is not exists.");
            throw new IllegalStateException(
                    "Order service attribute is not exists.");
        }
        productService = (ProductService) servletContext
                .getAttribute(ServiceParameters.PRODUCT__SERVICE);
        if (productService == null) {
            log.error("Product service attribute is not exists.");
            throw new IllegalStateException(
                    "Product service attribute is not exists.");
        }
        userService = (UserService) servletContext
                .getAttribute(ServiceParameters.USER__SERVICE);
        if (userService == null) {
            log.error("User service attribute is not exists.");
            throw new IllegalStateException(
                    "User service attribute is not exists.");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        if (log.isDebugEnabled()) {
            log.debug("GET method starts");
        }
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(UserParameters.USER);
        request.setAttribute(ErrorParameters.ERROR, session.getAttribute(ErrorParameters.ERROR));
        session.removeAttribute(ErrorParameters.ERROR);
        try {
            Order order = null;
            List<OrderItem> orderItems = null;
            long orderId;
            if (request.getParameter(OrderParameters.ORDER_ID) != null) {
                orderId = Long.parseLong(request
                        .getParameter(OrderParameters.ORDER_ID));
            } else {
                orderId = (long) session.getAttribute(OrderParameters.ORDER_ID);
            }
            if (log.isTraceEnabled()) {
                log.trace("orderId --> " + orderId);
                log.trace("user role --> " + user.getRole());
            }
            if (orderService.belong(orderId, user.getId()) || user.getRole() == Role.ADMIN) {
                order = orderService.getOrder(orderId);
                orderItems = new ArrayList<OrderItem>();
                for (OrderedProduct orderedProduct : order.getOrderedProducts()) {
                    Product product = productService.get(orderedProduct.getProductId());
                    OrderItem orderItem = new OrderItem(product, orderId, orderedProduct.getPrice(), orderedProduct.getNumber());
                    orderItems.add(orderItem);
                }
                if (user.getRole() == Role.ADMIN) {
                    User orderOwner = userService.get(order.getUserId());
                    request.setAttribute(OrderParameters.ORDER__OWNER, orderOwner.getFirstName() + " " + orderOwner.getLastName());
                    request.setAttribute(OrderParameters.ORDER__OWNER_EMAIL, orderOwner.getEmail());
                }
            } else {
                response.sendError(423);
                if (log.isDebugEnabled()) {
                    log.debug("Response was sent");
                }
                return;
            }
            request.setAttribute(OrderParameters.ORDER, order);
            request.setAttribute(OrderParameters.ORDER__ITEMS, orderItems);
            RequestDispatcher dispatcher = request
                    .getRequestDispatcher(Path.PAGE__ORDER);
            dispatcher.forward(request, response);
        } catch (NullPointerException | NumberFormatException | DAOException e) {
            log.error(e.getMessage());
            response.sendRedirect(Path.PAGE__ERROR_PAGE);
        }
        if (log.isDebugEnabled()) {
            log.debug("Response was sent");
        }
    }
}
