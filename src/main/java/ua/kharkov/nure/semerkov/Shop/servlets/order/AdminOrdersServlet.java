package ua.kharkov.nure.semerkov.Shop.servlets.order;

import org.apache.log4j.Logger;
import ua.kharkov.nure.semerkov.Shop.OrderStatus;
import ua.kharkov.nure.semerkov.Shop.Path;
import ua.kharkov.nure.semerkov.Shop.Role;
import ua.kharkov.nure.semerkov.Shop.dao.db.search.order.OrderSearchParametersBean;
import ua.kharkov.nure.semerkov.Shop.dao.db.search.order.OrderSearchParametersParser;
import ua.kharkov.nure.semerkov.Shop.dao.exception.DAOException;
import ua.kharkov.nure.semerkov.Shop.entity.Order;
import ua.kharkov.nure.semerkov.Shop.parameter.OrderParameters;
import ua.kharkov.nure.semerkov.Shop.parameter.SearchParameters;
import ua.kharkov.nure.semerkov.Shop.parameter.ServiceParameters;
import ua.kharkov.nure.semerkov.Shop.security.declaration.SecureServlet;
import ua.kharkov.nure.semerkov.Shop.security.declaration.annotation.Allow;
import ua.kharkov.nure.semerkov.Shop.service.OrderService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Servlet implementation class AdminOrdersServlet
 */
@Allow(Role.ADMIN)
public class AdminOrdersServlet extends SecureServlet {
    private OrderService orderService;
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AdminOrdersServlet.class);

    /**
     * @see javax.servlet.http.HttpServlet#HttpServlet()
     */
    public AdminOrdersServlet() {
        super();
        if (log.isDebugEnabled()) {
            log.debug("Servlet creates");
        }
    }

    @Override
    public void init() throws ServletException {
        ServletContext servletContext = getServletContext();
        orderService = (OrderService) servletContext
                .getAttribute(ServiceParameters.ORDER__SERVICE);
        if (orderService == null) {
            log.error("Order service attribute is not exists.");
            throw new IllegalStateException(
                    "Order service attribute is not exists.");
        }
    }

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws ServletException, IOException {
        if (log.isDebugEnabled()) {
            log.debug("GET method starts");
        }
        OrderSearchParametersBean searchParametersBean = OrderSearchParametersParser
                .getSearchParametersBean(request.getParameterMap());
        try {
            setParameters(request);
            int numberOfOrders = orderService
                    .getNumberOfRequiredOrders(searchParametersBean);
            validatePageNumber(numberOfOrders, searchParametersBean);
            setSearchParameters(request, searchParametersBean);
            request.setAttribute(SearchParameters.NUMBER_OF_ELEMENTS, numberOfOrders);
            if (numberOfOrders != 0) {
                List<Order> orders = orderService
                        .findOrders(searchParametersBean);
                request.setAttribute(OrderParameters.ORDERS, orders);
            }
            RequestDispatcher dispatcher = request
                    .getRequestDispatcher(Path.PAGE__ADMIN_ORDERS);
            dispatcher.forward(request, response);
        } catch (DAOException e) {
            log.error(e.getMessage());
            response.sendRedirect(Path.PAGE__ERROR_PAGE);
        }
        if (log.isDebugEnabled()) {
            log.debug("Response was sent");
        }
    }

    private void setParameters(HttpServletRequest request) throws DAOException {
        List<String> orderStatuses = new ArrayList<String>();
        orderStatuses.add(OrderStatus.EXECUTION.name());
        orderStatuses.add(OrderStatus.COMPLETED.name());
        orderStatuses.add(OrderStatus.CANCELED.name());
        request.setAttribute("orderStatuses", orderStatuses);
    }

    private void setSearchParameters(HttpServletRequest request,
                                     OrderSearchParametersBean searchParametersBean) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        if (searchParametersBean.getOrderId() != 0) {
            request.setAttribute(SearchParameters.ORDER_NUMBER,
                    searchParametersBean.getOrderId());
        }
        if (searchParametersBean.getOrderStatus() != null) {
            request.setAttribute(SearchParameters.ORDER_STATUS,
                    searchParametersBean.getOrderStatus());
        }
        if (searchParametersBean.isStartDateSelected()) {
            request.setAttribute(SearchParameters.START_DATE,
                    sdf.format(searchParametersBean.getStartDate()));
        }
        if (searchParametersBean.isEndDateSelected()) {
            request.setAttribute(SearchParameters.END_DATE,
                    sdf.format(searchParametersBean.getEndDate()));
        }
        request.setAttribute(SearchParameters.ELEMENT_NUMBER,
                searchParametersBean.getOrdersOnPage());
        request.setAttribute(SearchParameters.PAGE_NUMBER,
                searchParametersBean.getPageNumber());
    }

    private void validatePageNumber(int numberOfProducts,
                                    OrderSearchParametersBean searchParametersBean) {
        int numberOfPages = numberOfProducts
                / searchParametersBean.getOrdersOnPage()
                + ((numberOfProducts % searchParametersBean.getOrdersOnPage()) != 0 ? 1
                : 0);
        if (searchParametersBean.getPageNumber() < 1
                || searchParametersBean.getPageNumber() > numberOfPages) {
            searchParametersBean.setPageNumber(1);
        }
    }
}
