package ua.kharkov.nure.semerkov.Shop.filter.localization;

import java.util.Locale;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ua.kharkov.nure.semerkov.Shop.parameter.ServiceParameters;

/**
 * Cookies locale manager.
 * 
 * @author Volodymyr_Semerkov
 * 
 */
public class CookiesLocaleManager implements LocaleManager {
	private int cookiesTimeout;

	public CookiesLocaleManager(int cookiesTimeout) {
		this.cookiesTimeout = cookiesTimeout;
	}

	@Override
	public Locale getLocale(HttpServletRequest request) {
		Cookie[] cookies = request.getCookies();
		if (cookies == null) {
			return null;
		}
		Locale locale = null;
		for (Cookie cookie : cookies) {
			if (cookie.getName().equals(ServiceParameters.USER_LOCALE)) {
				String localeLanguage = cookie.getValue();
				locale = new Locale(localeLanguage);
			}
		}
		return locale;
	}

	@Override
	public void setLocale(Locale locale, HttpServletRequest request,
			HttpServletResponse response) {
		Cookie cookie = new Cookie(ServiceParameters.USER_LOCALE,
				locale.getLanguage());
		cookie.setMaxAge(cookiesTimeout);
		cookie.setPath("/");
		response.addCookie(cookie);
	}
}
