package ua.kharkov.nure.semerkov.Shop.security;

/**
 * The <code>LoginStatus</code> class contains the enumeration of login
 * statuses.
 * 
 * @author Volodymyr_Semerkov
 * 
 */
public enum LoginStatus {
	CORRECT, INCORRECT, LOCKED;
}
