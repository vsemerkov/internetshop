package ua.kharkov.nure.semerkov.Shop.entity;

import java.io.Serializable;

public class OrderItem implements Serializable, Cloneable {
	private static final long serialVersionUID = 6900753855779231467L;
	private Product product;
	private long orderId;
	private double price;
	private int number;

	public OrderItem() {
	}
	
	public OrderItem(Product product, double price, int number) {
		this.product = product;
		this.price = price;
		this.number = number;
	}

	public OrderItem(Product product, long orderId, double price, int number) {
		this(product, price, number);
		this.orderId = orderId;
	}

	public Product getProduct() {
		return product;
	}

	public long getOrderId() {
		return orderId;
	}

	public double getPrice() {
		return price;
	}

	public int getNumber() {
		return number;
	}
}
