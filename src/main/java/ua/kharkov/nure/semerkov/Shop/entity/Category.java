package ua.kharkov.nure.semerkov.Shop.entity;

import java.io.Serializable;

public class Category implements Serializable, Cloneable {
	private static final long serialVersionUID = 6953774624246037440L;
	private long id;
	private String name;

	public Category() {
	}

	public Category(long id) {
		this.id = id;
	}

    public Category(String name) {
        this.name = name;
    }

	public Category(long id, String name) {
		this(id);
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Category[id=");
		sb.append(id);
		sb.append(", name=");
		sb.append(name);
		sb.append("]");
		return sb.toString();
	}

	@Override
	public int hashCode() {
		return new Long(this.getId()).hashCode();
	}

	@Override
	public Category clone() throws CloneNotSupportedException {
		return (Category) super.clone();
	}
}
