package ua.kharkov.nure.semerkov.Shop.validator;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Pattern;

import ua.kharkov.nure.semerkov.Shop.parameter.UserParameters;
import ua.kharkov.nure.semerkov.Shop.entity.FormBean;

/**
 * User data validator.
 *
 * @author Volodymyr_Semerkov
 */
public class Validator {
    private Pattern namePattern = Pattern.compile("^[a-zA-Z]{2,25}$");
    private Pattern emailPattern = Pattern
            .compile("(?i)^([a-z0-9_\\.-]+)@([a-z0-9_\\.-]+)\\.([a-z\\.]{2,6})$");
    private Pattern passwordPattern = Pattern
            .compile("(?=^.{8,}$)((?=.*\\d)|(?=.*\\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$");

    public Map<String, String> validateFormBean(FormBean formBean) {
        Map<String, String> paramErrors = new LinkedHashMap<String, String>();
        if (!validateName(formBean.getFirstName())) {
            paramErrors.put(UserParameters.USER__FIRST_NAME,
                    "First name is incorrect!");
        }
        if (!validateName(formBean.getLastName())) {
            paramErrors.put(UserParameters.USER__LAST_NAME,
                    "Last name is incorrect!");
        }
        if (!validateEmail(formBean.getEmail())) {
            paramErrors.put(UserParameters.USER__EMAIL, "Email is incorrect!");
        }
        if (!validatePassword(formBean.getPassword())) {
            paramErrors
                    .put(UserParameters.USER__PASSWORD,
                            "Password must contains more than 8 characters, lower-case and upper-case characters, digits, wildcard characters and not more than 20 percents of space characters!");
        }
        if (formBean.getPassword() != null) {
            boolean fl = formBean.getPassword().equals(
                    formBean.getPasswordRepeat());
            if (!fl) {
                paramErrors.put(UserParameters.USER__PASSWORD_REPEAT,
                        "Passwords don't agree!");
            }
        }
        return paramErrors;
    }

    public Map<String, String> validateLoginData(String email, String password) {
        Map<String, String> paramErrors = new LinkedHashMap<String, String>();
        if (!validateEmail(email)) {
            paramErrors.put(UserParameters.USER__LOGIN_EMAIL,
                    "Email is incorrect!");
        }
        if (!validatePassword(password)) {
            paramErrors
                    .put(UserParameters.USER__LOGIN_PASSWORD,
                            "Password must contains more than 8 characters, lower-case and upper-case characters, digits, wildcard characters  and not more than 20 percents of space characters!!");
        }
        return paramErrors;
    }

    private boolean checkStringValue(String value, Pattern pattern) {
        if (value != null) {
            return pattern.matcher(value).lookingAt();
        } else {
            return false;
        }
    }

    public boolean validateName(String name) {
        return checkStringValue(name, namePattern);
    }

    public boolean validateEmail(String email) {
        return checkStringValue(email, emailPattern);
    }

    public boolean validatePassword(String password) {
        boolean result = checkStringValue(password, passwordPattern);
        int spaceNumber = 0;
        for (int i = 0; i < password.length(); i++) {
            if (password.charAt(i) == ' ') {
                spaceNumber++;
            }
        }
        double spacePercent = (double) spaceNumber / password.length();
        result = result && spacePercent < 0.2;
        return result;
    }
}
