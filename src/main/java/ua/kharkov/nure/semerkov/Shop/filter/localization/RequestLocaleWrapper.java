package ua.kharkov.nure.semerkov.Shop.filter.localization;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

/**
 * Request wrapper for localization.
 * 
 * @author Volodymyr_Semerkov
 * 
 */
public class RequestLocaleWrapper extends HttpServletRequestWrapper {
	private Vector<Locale> locales;

	public RequestLocaleWrapper(HttpServletRequest request, Locale locale) {
		super(request);
		this.locales = new Vector<Locale>();
		this.locales.add(locale);
	}

	@Override
	public Locale getLocale() {
		return locales.get(0);
	}

	@Override
	public Enumeration<Locale> getLocales() {
		return locales.elements();
	}

	@Override
	public String getHeader(String name) {
		if (name.equalsIgnoreCase("accept-language")) {
			return locales.get(0).getLanguage();
		}
		return super.getHeader(name);
	}

	@Override
	public Enumeration<String> getHeaders(String name) {
		if (name.equalsIgnoreCase("accept-language")) {
			List<String> localesLanguageNames = new ArrayList<String>();
			localesLanguageNames.add(locales.get(0).getLanguage());
			return Collections.enumeration(localesLanguageNames);
		}
		return super.getHeaders(name);
	}
}
