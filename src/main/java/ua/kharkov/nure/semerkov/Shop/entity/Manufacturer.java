package ua.kharkov.nure.semerkov.Shop.entity;

import java.io.Serializable;

public class Manufacturer implements Serializable, Cloneable {
    private static final long serialVersionUID = -638273715781337990L;
    private long id;
    private String name;

    public Manufacturer() {
    }

    public Manufacturer(long id) {
        this.id = id;
    }

    public Manufacturer(String name) {
        this.name = name;
    }

    public Manufacturer(long id, String name) {
        this(id);
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Manufacturer[id=");
        sb.append(id);
        sb.append(", name=");
        sb.append(name);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public int hashCode() {
        return new Long(this.getId()).hashCode();
    }

    @Override
    public Manufacturer clone() throws CloneNotSupportedException {
        return (Manufacturer) super.clone();
    }
}
