package ua.kharkov.nure.semerkov.Shop.dao.db.transaction;

import ua.kharkov.nure.semerkov.Shop.dao.exception.DAOException;

/**
 * Main interface for the TransactionManager pattern implementation.
 * 
 * @author Volodymyr_Semerkov
 * 
 */
public interface TransactionManager {
	public Object doInTransaction(TransactionOperation operation)
			throws DAOException;
}
