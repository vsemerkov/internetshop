package ua.kharkov.nure.semerkov.Shop.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import ua.kharkov.nure.semerkov.Shop.parameter.ServiceParameters;
import ua.kharkov.nure.semerkov.Shop.parameter.UserParameters;
import ua.kharkov.nure.semerkov.Shop.dao.exception.DAOException;
import ua.kharkov.nure.semerkov.Shop.service.UserService;

/**
 * Servlet implementation class EmailValidationServlet
 */
public class EmailValidationServlet extends HttpServlet {
	private UserService userService;
	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger
			.getLogger(EmailValidationServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EmailValidationServlet() {
		super();
		if (log.isDebugEnabled()) {
			log.debug("Servlet creates");
		}
	}

	@Override
	public void init() throws ServletException {
		ServletContext servletContext = getServletContext();
		userService = (UserService) servletContext
				.getAttribute(ServiceParameters.USER__SERVICE);
		if (userService == null) {
			log.error("User service attribute is not exists.");
			throw new IllegalStateException(
					"User service attribute is not exists.");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		if (log.isDebugEnabled()) {
			log.debug("POST method starts");
		}
		String email = request.getParameter(UserParameters.USER__EMAIL);
		if (log.isTraceEnabled()) {
			log.trace("email --> " + email);
		}
		boolean result = false;
		String message = null;
		try {
			if (email != null) {
				result = userService.containsUser(email);
				if (result) {
					message = "User with such email address already exists!";
				}
			}
		} catch (DAOException e) {
			log.error(e.getMessage());
		} finally {
			if (log.isTraceEnabled()) {
				log.trace("result --> " + result);
				log.trace("message --> " + message);
			}
			JSONObject resultJson = new JSONObject();
			resultJson.put("result", result);
			resultJson.put("message", message);
			response.setContentType("text/json");
			PrintWriter writer = response.getWriter();
			writer.write(resultJson.toString());
			writer.flush();
			if (log.isDebugEnabled()) {
				log.debug("JSON was sended");
			}
		}
	}
}
