package ua.kharkov.nure.semerkov.Shop.tag;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import ua.kharkov.nure.semerkov.Shop.parameter.ServiceParameters;

/**
 * Localization tag.
 * 
 * @author Volodymyr_Semerkov
 * 
 */
public class LocalizationTag extends SimpleTagSupport {
	private String value;

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public void doTag() throws IOException {
		PageContext pageContext = (PageContext) getJspContext();
		HttpServletRequest request = (HttpServletRequest) pageContext
				.getRequest();
		JspWriter writer = pageContext.getOut();
		String queryString = request.getQueryString();
		QueryStringEditor.addToQueryString(queryString, writer,
				ServiceParameters.LANG_PARAMETER, value);
	}
}
