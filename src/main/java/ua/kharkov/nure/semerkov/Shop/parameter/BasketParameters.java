package ua.kharkov.nure.semerkov.Shop.parameter;

/**
 * Holder for basket parameters names.
 * 
 * @author Volodymyr_Semerkov
 * 
 */
public final class BasketParameters {
	public static final String BASKET = "basket";
	public static final String ID = "id";
	public static final String NUMBER = "number";
	public static final String NUMBER_OF_PRODUCTS = "numberOfProducts";
	public static final String TOTAL_SUM = "totalSum";
}
