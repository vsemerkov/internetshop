package ua.kharkov.nure.semerkov.Shop.dao.db.search.order;

import ua.kharkov.nure.semerkov.Shop.OrderStatus;

import java.util.Date;

/**
 * Bean with search parameters.
 *
 * @author Volodymyr_Semerkov
 */
public class OrderSearchParametersBean {
    private long orderId;
    private OrderStatus orderStatus;
    private Date startDate;
    private Date endDate;
    private int ordersOnPage;
    private int pageNumber;
    private boolean isStartDateSelected;
    private boolean isEndDateSelected;

    public OrderSearchParametersBean() {
    }

    public OrderSearchParametersBean(long orderId, OrderStatus orderStatus, Date startDate, Date endDate, int ordersOnPage) {
        this.orderId = orderId;
        this.orderStatus = orderStatus;
        this.startDate = startDate;
        this.endDate = endDate;
        this.ordersOnPage = ordersOnPage;
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getOrdersOnPage() {
        return ordersOnPage;
    }

    public void setOrdersOnPage(int ordersOnPage) {
        this.ordersOnPage = ordersOnPage;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public boolean isStartDateSelected() {
        return isStartDateSelected;
    }

    public void setStartDateSelected(boolean isStartDateSelected) {
        this.isStartDateSelected = isStartDateSelected;
    }

    public boolean isEndDateSelected() {
        return isEndDateSelected;
    }

    public void setEndDateSelected(boolean isEndDateSelected) {
        this.isEndDateSelected = isEndDateSelected;
    }
}
