package ua.kharkov.nure.semerkov.Shop.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ua.kharkov.nure.semerkov.Shop.dao.db.jdbc.JdbcConnectionHolder;
import ua.kharkov.nure.semerkov.Shop.parameter.MapperParameters;
import ua.kharkov.nure.semerkov.Shop.dao.ManufacturerDAO;
import ua.kharkov.nure.semerkov.Shop.dao.exception.DAOException;
import ua.kharkov.nure.semerkov.Shop.db.DatabaseManager;
import ua.kharkov.nure.semerkov.Shop.entity.Manufacturer;

/**
 * Class for access to manufacturer data.
 *
 * @author Volodymyr_Semrkov
 */
public class MysqlManufacturerDAO implements ManufacturerDAO {
    private static final String SQL__FIND_MANUFACTURER_BY_NAME = "SELECT * FROM manufacturers where name=?";
    private static final String SQL__FIND_MANUFACTURER_BY_ID = "SELECT * FROM manufacturers where id=?";
    private static final String SQL__INSERT_MANUFACTURER = "INSERT INTO manufacturers(name) VALUES(?)";
    private static final String SQL__UPDATE_MANUFACTURER = "UPDATE manufacturers SET name=? WHERE id=?";
    private static final String SQL__REMOVE_MANUFACTURER = "DELETE FROM manufacturers WHERE id=?";
    private static final String SQL__SELECT_ALL = "SELECT * FROM manufacturers ORDER BY name ASC";
    private static final String SQL__FIND_LAST_MANUFACTURER = "SELECT MAX(id) AS max_id FROM manufacturers";

    @Override
    public boolean containsManufacturer(String name) throws DAOException {
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__FIND_MANUFACTURER_BY_NAME);
            pstmt.setString(1, name);
            return pstmt.executeQuery().first();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public boolean addManufacturer(Manufacturer manufacturer)
            throws DAOException {
        if (containsManufacturer(manufacturer.getName()))
            throw new DAOException(
                    "Manufacturer with such name already exists in database!");
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__INSERT_MANUFACTURER);
            pstmt.setString(1, manufacturer.getName());
            return pstmt.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public Manufacturer getManufacturer(long id) throws DAOException {
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__FIND_MANUFACTURER_BY_ID);
            pstmt.setLong(1, id);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                String name = rs.getString(MapperParameters.CATEGORY__NAME);
                return new Manufacturer(id, name);
            }
            return null;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public boolean removeManufacturer(long id) throws DAOException {
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__REMOVE_MANUFACTURER);
            pstmt.setLong(1, id);
            return pstmt.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public boolean updateManufacturer(Manufacturer manufacturer)
            throws DAOException {
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__UPDATE_MANUFACTURER);
            pstmt.setString(1, manufacturer.getName());
            pstmt.setLong(2, manufacturer.getId());
            return pstmt.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public List<Manufacturer> getAllManufacturers() throws DAOException {
        List<Manufacturer> categories = new ArrayList<Manufacturer>();
        Statement stmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(SQL__SELECT_ALL);
            while (rs.next()) {
                Manufacturer manufacturer = new Manufacturer(
                        rs.getInt(MapperParameters.ID));
                manufacturer.setName(rs
                        .getString(MapperParameters.MANUFACTURER__NAME));
                categories.add(manufacturer);
            }
            return categories;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(stmt);
        }
    }

    @Override
    public long getLastManufacturerId() throws DAOException {
        long orderId = -1;
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection
                    .prepareStatement(SQL__FIND_LAST_MANUFACTURER);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                orderId = rs.getLong("max_id");
            }
            return orderId;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }
}
