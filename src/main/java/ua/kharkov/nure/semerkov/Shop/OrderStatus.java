package ua.kharkov.nure.semerkov.Shop;

/**
 * The <code>OrderedStatus</code> class contains the enumeration of order statuses.
 * 
 * @author Volodymyr_Semerkov
 * 
 */
public enum OrderStatus {
    FORMED, EXECUTION, COMPLETED, CANCELED;
}