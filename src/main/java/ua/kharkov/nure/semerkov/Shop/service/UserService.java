package ua.kharkov.nure.semerkov.Shop.service;

import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;

import ua.kharkov.nure.semerkov.Shop.parameter.ServiceParameters;
import ua.kharkov.nure.semerkov.Shop.dao.UserDAO;
import ua.kharkov.nure.semerkov.Shop.dao.db.jdbc.JdbcTransactionManager;
import ua.kharkov.nure.semerkov.Shop.dao.db.transaction.TransactionManager;
import ua.kharkov.nure.semerkov.Shop.dao.db.transaction.TransactionOperation;
import ua.kharkov.nure.semerkov.Shop.dao.exception.DAOException;
import ua.kharkov.nure.semerkov.Shop.entity.User;
import ua.kharkov.nure.semerkov.Shop.security.Hashing;
import ua.kharkov.nure.semerkov.Shop.security.LoginStatus;

/**
 * This class contains methods for actions with users data.
 *
 * @author Volodymyr_Semrkov
 */
public class UserService {
    private UserDAO userDAO;
    private TransactionManager transactionManager;
    private static final Logger log = Logger.getLogger(UserService.class);

    public UserService(UserDAO userDAO) {
        if (log.isDebugEnabled()) {
            log.debug("User service creates");
        }
        this.userDAO = userDAO;
        this.transactionManager = new JdbcTransactionManager();
    }

    /**
     * Find user data in database.
     *
     * @param user User data.
     * @return True if user data contains in database, else False.
     * @throws DAOException
     */
    public boolean containsUser(final User user) throws DAOException {
        return (boolean) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        return userDAO.containsUser(user.getEmail());
                    }
                });
    }

    /**
     * Find user data in database.
     *
     * @param email User email.
     * @return True if user data contains in database, else False.
     * @throws DAOException
     */
    public boolean containsUser(final String email) throws DAOException {
        return (boolean) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        return userDAO.containsUser(email);
                    }
                });
    }

    /**
     * User authentication.
     *
     * @param email    User email.
     * @param password User password.
     * @return
     * @throws DAOException
     */
    public boolean authenticate(final String email, final String password)
            throws DAOException {
        return (boolean) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        User user = userDAO.getUser(email);
                        if (user == null)
                            return false;
                        return user.getPassword().equals(Hashing.salt(password, email));
                    }
                });
    }

    /**
     * Add user data in database.
     *
     * @param user User data.
     * @return True if user data not contains in database, else False.
     * @throws DAOException
     */
    public boolean add(final User user) throws DAOException {
        return (boolean) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        String password = user.getPassword();
                        String email = user.getEmail();
                        user.setPassword(Hashing.salt(password, email));
                        return userDAO.addUser(user);
                    }
                });
    }

    /**
     * Get user by email.
     *
     * @param email User email.
     * @return User object if user contains in database, else {@code null}.
     * @throws DAOException
     */
    public User get(final String email) throws DAOException {
        return (User) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        return userDAO.getUser(email);
                    }
                });
    }

    /**
     * Get user by id.
     *
     * @param email User ID.
     * @return User object if user contains in database, else {@code null}.
     * @throws DAOException
     */
    public User get(final long userId) throws DAOException {
        return (User) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        return userDAO.getUser(userId);
                    }
                });
    }

    /**
     * Remove user from database.
     *
     * @param email User email.
     * @return True if user data was removed from database, else False.
     * @throws DAOException
     */
    public void remove(final String email) throws DAOException {
        transactionManager.doInTransaction(new TransactionOperation() {
            @Override
            public Object execute() throws DAOException {
                userDAO.removeUser(email);
                return null;
            }
        });
    }

    /**
     * Update user data.
     *
     * @param user User data.
     * @return True if user data was updated in database, else False.
     * @throws DAOException
     */
    public boolean updateUser(final User user) throws DAOException {
        return (boolean) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        return userDAO.updateUser(user);
                    }
                });
    }

    /**
     * Check and set user locking parameters then user enters in the system from
     * login form.
     *
     * @param email    User email.
     * @param password User password.
     * @return
     * @throws DAOException
     */
    public LoginStatus login(final String email, final String password)
            throws DAOException {
        return (LoginStatus) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        User user = userDAO.getUser(email);
                        if (user == null)
                            return LoginStatus.INCORRECT;
                        LoginStatus status = LoginStatus.CORRECT;
                        boolean isLocked = false;
                        if (user.getUnlockingDate() != null) {
                            isLocked = user.getUnlockingDate().after(
                                    Calendar.getInstance().getTime());
                        }
                        if (isLocked) {
                            status = LoginStatus.LOCKED;
                        }
                        if (user.getPassword() != null
                                && user.getPassword().equals(Hashing.salt(password, email))
                                && status == LoginStatus.CORRECT) {
                            user.setLastLogin(Calendar.getInstance().getTime());
                            user.setAttemptNumber(0);
                        } else {
                            status = status == LoginStatus.CORRECT ? LoginStatus.INCORRECT
                                    : status;
                            if (status == LoginStatus.INCORRECT) {
                                int attemptNumber = user.getAttemptNumber() + 1;
                                if (attemptNumber > 2) {
                                    user.setAttemptNumber(0);
                                    Date unlockingDate = new Date(Calendar
                                            .getInstance().getTimeInMillis()
                                            + ServiceParameters.LOCKING_TIME);
                                    user.setUnlockingDate(unlockingDate);
                                    status = LoginStatus.LOCKED;
                                } else {
                                    user.setAttemptNumber(attemptNumber);
                                }
                            }
                        }
                        userDAO.updateUser(user);
                        return status;
                    }
                });
    }
}
