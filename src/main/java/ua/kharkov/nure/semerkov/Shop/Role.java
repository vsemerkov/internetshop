package ua.kharkov.nure.semerkov.Shop;

/**
 * The <code>Role</code> class contains the enumeration of user roles.
 * 
 * @author Volodymyr_Semerkov
 * 
 */
public enum Role {
	ADMIN, CLIENT, VISITOR;
}