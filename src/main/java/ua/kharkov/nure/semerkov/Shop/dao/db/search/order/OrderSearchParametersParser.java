package ua.kharkov.nure.semerkov.Shop.dao.db.search.order;

import ua.kharkov.nure.semerkov.Shop.OrderStatus;
import ua.kharkov.nure.semerkov.Shop.parameter.SearchParameters;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * Search parameters parser. Gets search parameters from request.
 *
 * @author Volodymyr_Semerkov
 */
public class OrderSearchParametersParser {
    public static OrderSearchParametersBean getSearchParametersBean(
            Map<String, String[]> requestParameters) {
        OrderSearchParametersBean searchParametersBean = new OrderSearchParametersBean();
        if (requestParameters.containsKey(SearchParameters.ORDER_NUMBER)) {
            try {
                long orderId = Long.parseLong(requestParameters
                        .get(SearchParameters.ORDER_NUMBER)[0]);
                searchParametersBean.setOrderId(orderId);
            } catch (NumberFormatException e) {
            }
        }
        if (requestParameters.containsKey(SearchParameters.ORDER_STATUS)) {
            try {
                searchParametersBean.setOrderStatus(OrderStatus
                        .valueOf(requestParameters
                                .get(SearchParameters.ORDER_STATUS)[0]
                                .toUpperCase()));
            } catch (IllegalArgumentException e) {
            }
        }
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        if (requestParameters.containsKey(SearchParameters.START_DATE)) {
            try {
                Date startDate = sdf.parse(requestParameters.get(SearchParameters.START_DATE)[0]);
                searchParametersBean.setStartDate(startDate);
                searchParametersBean.setStartDateSelected(true);
            } catch (NumberFormatException | ParseException e) {
            }
        }
        if (requestParameters.containsKey(SearchParameters.END_DATE)) {
            try {
                Date endDate = sdf.parse(requestParameters.get(SearchParameters.END_DATE)[0]);
                searchParametersBean.setEndDate(endDate);
                searchParametersBean.setEndDateSelected(true);
            } catch (NumberFormatException | ParseException e) {
            }
        }
        int ordersOnPage = 5;
        if (requestParameters.containsKey(SearchParameters.ELEMENT_NUMBER)) {
            try {
                ordersOnPage = Integer.parseInt(requestParameters
                        .get(SearchParameters.ELEMENT_NUMBER)[0]);
                if (ordersOnPage <= 0) {
                    ordersOnPage = 5;
                }
            } catch (NumberFormatException e) {
            }
        }
        searchParametersBean.setOrdersOnPage(ordersOnPage);
        int page = 1;
        if (requestParameters.containsKey(SearchParameters.PAGE)) {
            try {
                page = Integer.parseInt(requestParameters
                        .get(SearchParameters.PAGE)[0]);
            } catch (NumberFormatException e) {
            }
        }
        searchParametersBean.setPageNumber(page);
        return searchParametersBean;
    }
}
