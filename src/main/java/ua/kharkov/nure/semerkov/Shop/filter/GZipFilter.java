package ua.kharkov.nure.semerkov.Shop.filter;

import java.io.IOException;
import java.util.zip.GZIPOutputStream;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

/**
 * This class represents compression filter for response with text content.
 * 
 * @author Volodymyr_Semerkov
 * 
 */
public class GZipFilter implements Filter {
	private static final Logger log = Logger.getLogger(GZipFilter.class);

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		log.debug("Filter initialization starts");
		log.debug("Filter initialization finished");
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		log.debug("Filter starts");
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		if (acceptGZipEncoding(httpRequest)) {
			log.debug("Browser accept gzip encoding");
			ResponseWrapper responseWrapper = new ResponseWrapper(httpResponse);
			chain.doFilter(httpRequest, responseWrapper);
			if (isTextData(responseWrapper)) {
				httpResponse.setHeader("Content-Encoding", "gzip");
				GZIPOutputStream gzipOutputStream = new GZIPOutputStream(
						httpResponse.getOutputStream());
				if (responseWrapper.isWriterUsed()) {
					gzipOutputStream.write(responseWrapper.toString().getBytes(
							responseWrapper.getCharacterEncoding()));
				} else {
					gzipOutputStream.write(responseWrapper.getBytes());
				}
				gzipOutputStream.flush();
				gzipOutputStream.close();
			} else {
				if (responseWrapper.isWriterUsed()) {
					httpResponse.getWriter().write(responseWrapper.toString());
					httpResponse.getWriter().flush();
					httpResponse.getWriter().close();
				} else {
					httpResponse.getOutputStream().write(
							responseWrapper.getBytes());
					httpResponse.getOutputStream().flush();
					httpResponse.getOutputStream().close();
				}
			}
		} else {
			log.debug("Browser don`t accept gzip encoding or type is not a text");
			chain.doFilter(httpRequest, httpResponse);
		}
		log.debug("Filter finished");
	}

	@Override
	public void destroy() {
	}

	private boolean acceptGZipEncoding(HttpServletRequest httpRequest) {
		String acceptEncoding = httpRequest.getHeader("Accept-Encoding");
		return acceptEncoding != null && acceptEncoding.contains("gzip");
	}

	private boolean isTextData(HttpServletResponse response) {
		String contentType = response.getContentType();
		if (contentType == null)
			return false;
		return contentType.startsWith("text/")
				|| contentType.contains("javascript");
	}
}
