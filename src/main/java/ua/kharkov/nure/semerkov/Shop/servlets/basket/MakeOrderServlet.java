package ua.kharkov.nure.semerkov.Shop.servlets.basket;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.kharkov.nure.semerkov.Shop.parameter.BasketParameters;
import ua.kharkov.nure.semerkov.Shop.parameter.OrderParameters;
import ua.kharkov.nure.semerkov.Shop.Path;
import ua.kharkov.nure.semerkov.Shop.dao.db.DeliveryType;
import ua.kharkov.nure.semerkov.Shop.dao.db.PaymentType;
import ua.kharkov.nure.semerkov.Shop.entity.Basket;
import ua.kharkov.nure.semerkov.Shop.entity.Order;
import ua.kharkov.nure.semerkov.Shop.entity.OrderItem;

/**
 * Servlet implementation class MakeOrderServlet
 */
public class MakeOrderServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(MakeOrderServlet.class);

    /**
     * @see HttpServlet#HttpServlet()
     */
    public MakeOrderServlet() {
        super();
        if (log.isDebugEnabled()) {
            log.debug("Servlet creates");
        }
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        if (log.isDebugEnabled()) {
            log.debug("GET method starts");
        }
        HttpSession session = request.getSession();
        Basket basket = (Basket) session.getAttribute(BasketParameters.BASKET);
        if (log.isTraceEnabled()) {
            log.trace("basket --> " + basket);
        }
        boolean result = basket != null;
        List<OrderItem> items;
        List<OrderItem> orderItems = null;
        if (result) {
            items = basket.getListOfProducts();
            orderItems = new ArrayList<OrderItem>();
            for (OrderItem orderItem : items) {
                if (orderItem.getNumber() > 0) {
                    orderItems.add(orderItem);
                }
            }
            result = orderItems.size() > 0;
        }
        if (result) {
            request.setAttribute("items", orderItems);
            List<String> deliveryTypes = new ArrayList<String>();
            deliveryTypes.add(DeliveryType.COURIER.name().toLowerCase());
            deliveryTypes.add(DeliveryType.SELF_DELIVERY.name().toLowerCase());
            request.setAttribute(OrderParameters.ORDER__DELIVERY_TYPES,
                    deliveryTypes);
            List<String> paymentTypes = new ArrayList<String>();
            paymentTypes.add(PaymentType.CASH.name().toLowerCase());
            paymentTypes.add(PaymentType.NON_CASH.name().toLowerCase());
            request.setAttribute(OrderParameters.ORDER__PAYMENT_TYPES,
                    paymentTypes);
            Order order = (Order) session.getAttribute(OrderParameters.ORDER);
            if (order != null) {
                setOrderParameters(request, order);
            }
            request.setAttribute("error", session.getAttribute("error"));
            session.removeAttribute("error");
        } else {
            response.sendRedirect(Path.COMMAND__BASKET);
            if (log.isDebugEnabled()) {
                log.debug("Response was sent");
            }
            return;
        }
        RequestDispatcher dispatcher = request
                .getRequestDispatcher(Path.PAGE__MAKE_ORDER);
        dispatcher.forward(request, response);
        if (log.isDebugEnabled()) {
            log.debug("Response was sent");
        }
    }

    private void setOrderParameters(HttpServletRequest request, Order order) {
        request.setAttribute(OrderParameters.ORDER__ADDRESS, order.getAddress());
        request.setAttribute(OrderParameters.ORDER__DELIVERY_TYPE, order
                .getDeliveryType().name().toLowerCase());
        request.setAttribute(OrderParameters.ORDER__PAYMENT_TYPE, order
                .getPaymentType().name().toLowerCase());
        request.setAttribute(OrderParameters.ORDER__CARD_NUMBER,
                order.getCardNumber());
    }
}
