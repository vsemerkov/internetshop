package ua.kharkov.nure.semerkov.Shop.parameter;

/**
 * Holder for database parameters names.
 * 
 * @author Volodymyr_Semerkov
 * 
 */
public final class DatabaseParameters {
	public static final String DB_DRIVER_NAME = "com.mysql.jdbc.Driver";
	public static final String DB_SERVER_URL = "jdbc:mysql://localhost:3306";
	public static final String DB_NAME = "shop_semerkov";
	public static final String TEST_DB_NAME = "test_shop_semerkov";
	public static final String TEST_DB_FILE_PATH = "/sql/create_test_database.sql";
	public static final String DB_URL = "jdbc:mysql://localhost:3306/shop_semerkov";
	public static final String TEST_DB_URL = "jdbc:mysql://localhost:3306/test_shop_semerkov";
	public static final String USER = "root";
	public static final String PASSWORD = "root";
}
