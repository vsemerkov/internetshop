package ua.kharkov.nure.semerkov.Shop.security.path;

import java.util.List;

import org.apache.log4j.Logger;

import ua.kharkov.nure.semerkov.Shop.Role;

/**
 * Security manager.
 * 
 * @author Volodymyr_Semrkov
 * 
 */
public class SecurityManager {
	List<Constraint> constraints;
	private static final Logger log = Logger.getLogger(SecurityManager.class);

	public SecurityManager(List<Constraint> constraints) {
		for (Constraint constraint : constraints) {
			String urlPattern = constraint.getURLPattern();
			urlPattern = urlPattern.replaceAll("\\*", ".*");
			constraint.setURLPattern(urlPattern);
		}
		this.constraints = constraints;
	}

	public boolean accept(String pagePath, Role role) {
		log.trace("pagePath --> " + pagePath);
		log.trace("role --> " + role.name());
		for (Constraint constraint : constraints) {
			if (constraint.getRoles().contains(role)
					&& pagePath.matches(constraint.getURLPattern())) {
				return false;
			}
		}
		return true;
	}
}
