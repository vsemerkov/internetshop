package ua.kharkov.nure.semerkov.Shop.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;

/**
 * This class represents basket of products.
 * 
 * @author Volodymyr_Semerkov
 * 
 */
public class Basket implements Serializable {
	private static final long serialVersionUID = -3440809838158912711L;
	private LinkedHashMap<Product, Integer> products = new LinkedHashMap<Product, Integer>();

	public synchronized void addProduct(Product product) {
		addProduct(product, 1);
	}

	public synchronized void addProduct(Product product, int number) {
		products.put(product, number);
	}

	public synchronized void changeNumberOfProduct(long id, int number) {
		Iterator<Entry<Product, Integer>> it = products.entrySet().iterator();
		while (it.hasNext()) {
			Entry<Product, Integer> entry = it.next();
			if (entry.getKey().getId() == id) {
				Product product = entry.getKey();
				products.put(product, number);
				break;
			}
		}
	}

	public synchronized double getPriceOfProduct(long id) {
		Iterator<Entry<Product, Integer>> it = products.entrySet().iterator();
		while (it.hasNext()) {
			Entry<Product, Integer> entry = it.next();
			if (entry.getKey().getId() == id) {
				return entry.getKey().getPrice();
			}
		}
		return 0;
	}

	public synchronized void removeProduct(long id) {
		Iterator<Entry<Product, Integer>> it = products.entrySet().iterator();
		while (it.hasNext()) {
			Entry<Product, Integer> entry = it.next();
			if (entry.getKey().getId() == id) {
				products.remove(entry.getKey());
				break;
			}
		}
	}

	public synchronized List<OrderItem> getListOfProducts() {
		List<OrderItem> productsList = new ArrayList<OrderItem>();
		Iterator<Entry<Product, Integer>> it = products.entrySet().iterator();
		while (it.hasNext()) {
			Entry<Product, Integer> entry = it.next();
			Product product = entry.getKey();
			int number = entry.getValue();
			productsList
					.add(new OrderItem(product, product.getPrice(), number));
		}
		return productsList;
	}

	public synchronized int getNumberOfProducts() {
		int productsNumber = 0;
		Iterator<Entry<Product, Integer>> it = products.entrySet().iterator();
		while (it.hasNext()) {
			Entry<Product, Integer> entry = it.next();
			productsNumber += entry.getValue();
		}
		return productsNumber;
	}

	public synchronized double getTotalSum() {
		double totalSum = 0;
		Iterator<Entry<Product, Integer>> it = products.entrySet().iterator();
		while (it.hasNext()) {
			Entry<Product, Integer> entry = it.next();
			totalSum += entry.getKey().getPrice() * entry.getValue();
		}
		return totalSum;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Basket [numberOfProducts=");
		sb.append(getNumberOfProducts());
		sb.append(", totalSum=");
		sb.append(getTotalSum());
		sb.append("]");
		return sb.toString();
	}
}
