package ua.kharkov.nure.semerkov.Shop.parameter;

/**
 * Holder for user parameters names.
 * 
 * @author Volodymyr_Semerkov
 * 
 */
public final class UserParameters {
	// registration fields
	public static final String USER = "user";
	public static final String USER__FIRST_NAME = "firstName";
	public static final String USER__LAST_NAME = "lastName";
	public static final String USER__EMAIL = "email";
	public static final String USER__PASSWORD = "password";
	public static final String USER__PASSWORD_REPEAT = "passwordRepeat";
	public static final String USER__SUBSCRIBE = "subscribe";
	public static final String USER__CAPTCHA = "captcha";
	public static final String USER__IMAGE = "image";

	// login fields
	public static final String USER__LOGIN_EMAIL = "loginEmail";
	public static final String USER__LOGIN_PASSWORD = "loginPassword";
	public static final String USER__LOGIN_ERROR = "loginError";

    // login form fields
    public static final String USER_FORM__LOGIN_EMAIL = "formLoginEmail";
    public static final String USER_FORM__LOGIN_PASSWORD = "formLoginPassword";
    public static final String USER_FORM__LOGIN_ERROR = "formLoginError";
}
