package ua.kharkov.nure.semerkov.Shop.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ua.kharkov.nure.semerkov.Shop.CanceledOrderStatus;
import ua.kharkov.nure.semerkov.Shop.OrderStatus;
import ua.kharkov.nure.semerkov.Shop.dao.db.DeliveryType;
import ua.kharkov.nure.semerkov.Shop.dao.db.PaymentType;

/**
 * The <code>Order</code> class represents order data.
 *
 * @author Volodymyr_Semerkov
 */
public class Order implements Serializable, Cloneable {
    private static final long serialVersionUID = -4794651973558939282L;
    private long id;
    private OrderStatus orderStatus;
    private CanceledOrderStatus canceledOrderStatus;
    private Date orderDate;
    private long userId;
    private List<OrderedProduct> orderedProducts = new ArrayList<OrderedProduct>();
    private DeliveryType deliveryType;
    private PaymentType paymentType;
    private String cardNumber;
    private String CVV;
    private String address;

    public Order() {
    }

    public Order(long id) {
        this.id = id;
    }

    public Order(long id, OrderStatus orderedStatus,
                 CanceledOrderStatus canceledOrderStatus, Date orderDate,
                 long userId, List<OrderedProduct> orderedProducts,
                 DeliveryType deliveryType, PaymentType paymentType,
                 String cardNumber, String address) {
        this(id);
        this.orderStatus = orderedStatus;
        this.canceledOrderStatus = canceledOrderStatus;
        this.orderDate = orderDate;
        this.userId = userId;
        this.orderedProducts = orderedProducts;
        this.deliveryType = deliveryType;
        this.paymentType = paymentType;
        this.cardNumber = cardNumber;
        this.address = address;
    }

    public long getId() {
        return id;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public CanceledOrderStatus getCanceledOrderStatus() {
        return canceledOrderStatus;
    }

    public void setCanceledOrderStatus(CanceledOrderStatus canceledOrderStatus) {
        this.canceledOrderStatus = canceledOrderStatus;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public List<OrderedProduct> getOrderedProducts() {
        return orderedProducts;
    }

    public void setOrderedProducts(List<OrderedProduct> orderedProducts) {
        this.orderedProducts = orderedProducts;
    }

    public DeliveryType getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(DeliveryType deliveryType) {
        this.deliveryType = deliveryType;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCVV() {
        return CVV;
    }

    public void setCVV(String CVV) {
        this.CVV = CVV;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getTotalSum() {
        double totalSum = 0;
        for (OrderedProduct orderedProduct : orderedProducts) {
            totalSum += orderedProduct.getTotalSum();
        }
        return totalSum;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || (obj.getClass() != this.getClass()))
            return false;
        Order otherOrder = (Order) obj;
        return this.getId() == otherOrder.getId();
    }

    @Override
    public int hashCode() {
        return new Long(this.getId()).hashCode();
    }

    @Override
    public Order clone() throws CloneNotSupportedException {
        return (Order) super.clone();
    }
}
