package ua.kharkov.nure.semerkov.Shop.servlets.category;

import org.apache.log4j.Logger;
import ua.kharkov.nure.semerkov.Shop.Path;
import ua.kharkov.nure.semerkov.Shop.dao.exception.DAOException;
import ua.kharkov.nure.semerkov.Shop.entity.Category;
import ua.kharkov.nure.semerkov.Shop.parameter.ErrorParameters;
import ua.kharkov.nure.semerkov.Shop.parameter.OrderParameters;
import ua.kharkov.nure.semerkov.Shop.parameter.ServiceParameters;
import ua.kharkov.nure.semerkov.Shop.service.CategoryService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Servlet implementation class CategoryServlet
 */
public class CategoryServlet extends HttpServlet {
    private CategoryService categoryService;
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(CategoryServlet.class);

    /**
     * @see javax.servlet.http.HttpServlet#HttpServlet()
     */
    public CategoryServlet() {
        super();
        if (log.isDebugEnabled()) {
            log.debug("Servlet creates");
        }
    }

    @Override
    public void init() throws ServletException {
        ServletContext servletContext = getServletContext();
        categoryService = (CategoryService) servletContext
                .getAttribute(ServiceParameters.CATEGORY__SERVICE);
        if (categoryService == null) {
            log.error("Category service attribute is not exists.");
            throw new IllegalStateException(
                    "Category service attribute is not exists.");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        if (log.isDebugEnabled()) {
            log.debug("GET method starts");
        }
        HttpSession session = request.getSession();
        request.setAttribute(ErrorParameters.ERROR, session.getAttribute(ErrorParameters.ERROR));
        session.removeAttribute(ErrorParameters.ERROR);
        try {
            long categoryId = 0;
            if (request.getParameter(OrderParameters.CATEGORY_ID) != null) {
                categoryId = Long.parseLong(request
                        .getParameter(OrderParameters.CATEGORY_ID));
            }
            if (categoryId != 0) {
                Category category = categoryService.getCategory(categoryId);
                request.setAttribute(OrderParameters.CATEGORY_ID, categoryId);
                request.setAttribute(OrderParameters.CATEGORY_NAME, category.getName());
            }
            RequestDispatcher dispatcher = request
                    .getRequestDispatcher(Path.PAGE__CATEGORY);
            dispatcher.forward(request, response);
        } catch (DAOException e) {
            log.error(e.getMessage());
            response.sendRedirect(Path.PAGE__ERROR_PAGE);
        }
        if (log.isDebugEnabled()) {
            log.debug("Response was sent");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {
        if (log.isDebugEnabled()) {
            log.debug("POST method starts");
        }
        HttpSession session = request.getSession();
        try {
            long categoryId = 0;
            if (request.getParameter(OrderParameters.CATEGORY_ID) != null) {
                categoryId = Long.parseLong(request
                        .getParameter(OrderParameters.CATEGORY_ID));
            }
            String categoryName = request.getParameter(OrderParameters.CATEGORY_NAME);
            if (categoryName != null && categoryName.trim().length() != 0) {
                categoryName = categoryName.trim();
                if (!categoryService.contains(categoryName)) {
                    if (categoryId == 0) {
                        categoryService.addCategory(new Category(categoryName));
                        categoryId = categoryService.getLastCategoryId();
                    } else {
                        categoryService.updateCategory(new Category(categoryId, categoryName));
                    }
                } else {
                    session.setAttribute(ErrorParameters.ERROR, ErrorParameters.CATEGORY_EXISTING_ERROR);
                }
            } else {
                session.setAttribute(ErrorParameters.ERROR, ErrorParameters.CATEGORY_INCORRECT_ERROR);
            }
            response.sendRedirect(Path.COMMAND__CATEGORY + "?" + OrderParameters.CATEGORY_ID + "=" + categoryId);
        } catch (DAOException e) {
            log.error(e.getMessage());
            response.sendRedirect(Path.PAGE__ERROR_PAGE);
        }
        if (log.isDebugEnabled()) {
            log.debug("Response was sent");
        }
    }
}
