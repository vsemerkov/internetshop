package ua.kharkov.nure.semerkov.Shop.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ua.kharkov.nure.semerkov.Shop.dao.db.jdbc.JdbcConnectionHolder;
import ua.kharkov.nure.semerkov.Shop.dao.db.search.product.ProductSearchParametersBean;
import ua.kharkov.nure.semerkov.Shop.dao.db.search.product.SortOrder;
import ua.kharkov.nure.semerkov.Shop.dao.db.search.product.SortParameter;
import ua.kharkov.nure.semerkov.Shop.dao.mapper.ProductMapper;
import ua.kharkov.nure.semerkov.Shop.parameter.MapperParameters;
import ua.kharkov.nure.semerkov.Shop.dao.ProductDAO;
import ua.kharkov.nure.semerkov.Shop.dao.exception.DAOException;
import ua.kharkov.nure.semerkov.Shop.db.DatabaseManager;
import ua.kharkov.nure.semerkov.Shop.entity.Product;

/**
 * Class for access to product data.
 *
 * @author Volodymyr_Semrkov
 */
public class MysqlProductDAO implements ProductDAO {
    private static final String SQL__FIND_PRODUCT_BY_ID = "SELECT products.id AS id, products.name AS name, "
            + "products.price AS price, products.number AS number, products.photo_file_name AS photo_file_name, "
            + "products.manufacturer_id AS manufacturer_id, manufacturers.name AS manufacturer_name, "
            + "products.category_id AS category_id, categories.name AS category_name, "
            + "products.info AS info FROM products "
            + "INNER JOIN manufacturers ON products.manufacturer_id=manufacturers.id "
            + "INNER JOIN categories ON products.category_id=categories.id AND products.id=?";
    private static final String SQL__INSERT_PRODUCT = "INSERT INTO products(name, price, number, photo_file_name, manufacturer_id, category_id, info) "
            + "VALUES(?, ?, ?, ?, ?, ?, ?)";
    private static final String SQL__UPDATE_PRODUCT = "UPDATE products SET name=?, price=?, number=?, photo_file_name=?, "
            + "manufacturer_id=?, category_id=?, info=? WHERE id=?";
    private static final String SQL__REMOVE_PRODUCT = "DELETE FROM products WHERE id=?";
    private static final String SQL__GET_MAX_PRICE = "SELECT MAX(price) AS max_price FROM products";
    private static final String SQL__GET_IMAGE_PATH_BY_ID = "SELECT photo_file_name FROM products WHERE id=?";
    private static final String SQL__SELECT_ALL = "SELECT products.id AS id, products.name AS name, "
            + "products.price AS price, products.number AS number, products.photo_file_name AS photo_file_name, "
            + "products.manufacturer_id AS manufacturer_id, manufacturers.name AS manufacturer_name, "
            + "products.category_id AS category_id, categories.name AS category_name, "
            + "products.info AS info FROM products "
            + "INNER JOIN manufacturers ON products.manufacturer_id=manufacturers.id "
            + "INNER JOIN categories ON products.category_id=categories.id ORDER BY id ASC";

    @Override
    public boolean containsProduct(long id) throws DAOException {
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__FIND_PRODUCT_BY_ID);
            pstmt.setLong(1, id);
            return pstmt.executeQuery().first();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public boolean addProduct(Product product) throws DAOException {
        if (containsProduct(product.getId()))
            throw new DAOException(
                    "Product with such id already exists in database!");
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__INSERT_PRODUCT);
            ProductMapper.mapProduct(product, pstmt);
            return pstmt.executeUpdate() != 1;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public Product getProduct(long id) throws DAOException {
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__FIND_PRODUCT_BY_ID);
            pstmt.setLong(1, id);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                return ProductMapper.unMapProduct(rs);
            }
            return null;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public boolean removeProduct(long id) throws DAOException {
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__REMOVE_PRODUCT);
            pstmt.setLong(1, id);
            return pstmt.executeUpdate() != 1;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public boolean updateProduct(Product product) throws DAOException {
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__UPDATE_PRODUCT);
            ProductMapper.mapProduct(product, pstmt);
            pstmt.setLong(8, product.getId());
            return pstmt.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public double getMaxPrice() throws DAOException {
        Statement stmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(SQL__GET_MAX_PRICE);
            if (rs.next()) {
                return rs.getDouble("max_price");
            }
            return 0;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(stmt);
        }
    }

    @Override
    public String getImagePath(long id) throws DAOException {
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__GET_IMAGE_PATH_BY_ID);
            pstmt.setLong(1, id);
            ResultSet rs = pstmt.executeQuery();
            String imagePath = null;
            if (rs.next()) {
                imagePath = rs
                        .getString(MapperParameters.PRODUCT__PHOTO_FILE_NAME);
            }
            return imagePath;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public int getNumberOfRequiredProducts(
            ProductSearchParametersBean searchParameterBean) throws DAOException {
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            String query = prepareCountQuery(searchParameterBean);
            pstmt = connection.prepareStatement(query);
            if (searchParameterBean.getProductName() != null) {
                pstmt.setString(1, searchParameterBean.getProductName());
            }
            ResultSet rs = pstmt.executeQuery();
            int numberOfProducts = 0;
            if (rs.next()) {
                numberOfProducts = rs.getInt("number");
            }
            return numberOfProducts;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public List<Product> findProducts(ProductSearchParametersBean searchParameterBean)
            throws DAOException {
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            String query = prepareSearchingQuery(searchParameterBean);
            pstmt = connection.prepareStatement(query);
            if (searchParameterBean.getProductName() != null) {
                pstmt.setString(1, searchParameterBean.getProductName());
            }
            ResultSet rs = pstmt.executeQuery();
            List<Product> products = new ArrayList<Product>();
            while (rs.next()) {
                Product product = ProductMapper.unMapProduct(rs);
                products.add(product);
            }
            return products;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public List<Product> getAllProducts() throws DAOException {
        List<Product> products = new ArrayList<Product>();
        Statement stmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(SQL__SELECT_ALL);
            while (rs.next()) {
                Product product = ProductMapper.unMapProduct(rs);
                products.add(product);
            }
            return products;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(stmt);
        }
    }

    private String prepareCountQuery(ProductSearchParametersBean searchParameterBean) {
        StringBuilder query = new StringBuilder(
                "SELECT COUNT(id) AS number FROM products");
        String mainQueryPart = prepareMainQueryPart(searchParameterBean);
        if (!mainQueryPart.trim().isEmpty()) {
            query.append(" WHERE ");
            query.append(mainQueryPart);
        }
        return query.toString();
    }

    private String prepareSearchingQuery(
            ProductSearchParametersBean searchParametersBean) {
        StringBuilder query = new StringBuilder(
                "SELECT products.id AS id, products.name AS name, "
                        + "products.price AS price, products.number AS number, products.photo_file_name AS photo_file_name, "
                        + "products.manufacturer_id AS manufacturer_id, manufacturers.name AS manufacturer_name, "
                        + "products.category_id AS category_id, categories.name AS category_name, "
                        + "products.info AS info FROM products "
                        + "INNER JOIN manufacturers ON products.manufacturer_id=manufacturers.id "
                        + "INNER JOIN categories ON products.category_id=categories.id");
        String mainQueryPart = prepareMainQueryPart(searchParametersBean);
        if (!mainQueryPart.trim().isEmpty()) {
            query.append(" AND ");
            query.append(mainQueryPart);
        }
        SortParameter sortParameter = searchParametersBean.getSortParameter();
        SortOrder sortOrder = searchParametersBean.getSortOrder();
        if (sortParameter != null && sortOrder != null) {
            query.append(" ORDER BY ");
            query.append("products.");
            query.append(sortParameter.name());
            query.append(" ");
            query.append(sortOrder.name());
        }
        int productsOnPage = searchParametersBean.getProductsOnPage();
        int pageNumber = searchParametersBean.getPageNumber();
        query.append(" LIMIT ");
        query.append((pageNumber - 1) * productsOnPage);
        query.append(", ");
        query.append(productsOnPage);
        return query.toString();
    }

    private String prepareMainQueryPart(
            ProductSearchParametersBean searchParametersBean) {
        StringBuilder query = new StringBuilder();
        List<String> queryParts = new ArrayList<String>();
        if (searchParametersBean.isMinPriceSelected())
            queryParts.add(String.format("(price>%s)",
                    searchParametersBean.getMinPrice()));
        if (searchParametersBean.isMaxPriceSelected())
            queryParts.add(String.format("(price<%s)",
                    searchParametersBean.getMaxPrice()));
        List<Long> categories = searchParametersBean.getCategories();
        if (categories != null && categories.size() != 0) {
            StringBuilder categoryList = new StringBuilder(
                    String.valueOf(categories.get(0)));
            for (int i = 1; i < categories.size(); i++) {
                categoryList.append(", ");
                categoryList.append(categories.get(i));
            }
            queryParts.add(String.format("(category_id IN(%s))",
                    categoryList.toString()));
        }
        List<Long> manufacturers = searchParametersBean.getManufacturers();
        if (manufacturers != null && manufacturers.size() != 0) {
            StringBuilder manufacturerList = new StringBuilder(
                    String.valueOf(manufacturers.get(0)));
            for (int i = 1; i < manufacturers.size(); i++) {
                manufacturerList.append(", ");
                manufacturerList.append(manufacturers.get(i));
            }
            queryParts.add(String.format("(manufacturer_id IN(%s))",
                    manufacturerList.toString()));
        }
        String productName = searchParametersBean.getProductName();
        if (productName != null) {
            queryParts.add("(products.name LIKE concat('%', ?, '%'))");
        }
        if (queryParts.size() != 0) {
            query.append(queryParts.get(0));
            for (int i = 1; i < queryParts.size(); i++) {
                query.append(" AND ");
                query.append(queryParts.get(i));
            }
        }
        return query.toString();
    }
}
