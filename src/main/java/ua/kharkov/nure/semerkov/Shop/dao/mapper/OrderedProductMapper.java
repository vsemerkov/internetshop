package ua.kharkov.nure.semerkov.Shop.dao.mapper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import ua.kharkov.nure.semerkov.Shop.parameter.MapperParameters;
import ua.kharkov.nure.semerkov.Shop.entity.OrderedProduct;

/**
 * Ordered product mapper.
 * 
 * @author Volodymyr_Semrkov
 * 
 */
public class OrderedProductMapper {
	public static OrderedProduct unMapOrderedProduct(ResultSet rs)
			throws SQLException {
		long id = rs.getLong(MapperParameters.ID);
		long orderId = rs.getLong(MapperParameters.ORDERED_PRODUCT__ORDER_ID);
		long productId = rs
				.getLong(MapperParameters.ORDERED_PRODUCT__PRODUCT_ID);
		double price = rs.getDouble(MapperParameters.ORDERED_PRODUCT__PRICE);
		int number = rs.getInt(MapperParameters.ORDERED_PRODUCT__NUMBER);
		OrderedProduct orderedProduct = new OrderedProduct(id, orderId,
				productId, price, number);
		return orderedProduct;
	}

	public static void mapOrderedProduct(OrderedProduct orderedProduct,
			PreparedStatement pstmt) throws SQLException {
		pstmt.setLong(1, orderedProduct.getOrderId());
		pstmt.setLong(2, orderedProduct.getProductId());
		pstmt.setDouble(3, orderedProduct.getPrice());
		pstmt.setInt(4, orderedProduct.getNumber());
	}
}
