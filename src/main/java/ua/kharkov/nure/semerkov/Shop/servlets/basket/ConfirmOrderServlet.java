package ua.kharkov.nure.semerkov.Shop.servlets.basket;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.kharkov.nure.semerkov.Shop.parameter.BasketParameters;
import ua.kharkov.nure.semerkov.Shop.parameter.OrderParameters;
import ua.kharkov.nure.semerkov.Shop.OrderStatus;
import ua.kharkov.nure.semerkov.Shop.Path;
import ua.kharkov.nure.semerkov.Shop.parameter.ServiceParameters;
import ua.kharkov.nure.semerkov.Shop.parameter.UserParameters;
import ua.kharkov.nure.semerkov.Shop.dao.exception.DAOException;
import ua.kharkov.nure.semerkov.Shop.entity.Order;
import ua.kharkov.nure.semerkov.Shop.entity.User;
import ua.kharkov.nure.semerkov.Shop.service.OrderService;

/**
 * Servlet implementation class ConfirmOrderServlet
 */
public class ConfirmOrderServlet extends HttpServlet {
    private OrderService orderService;
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger
            .getLogger(ConfirmOrderServlet.class);

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConfirmOrderServlet() {
        super();
        if (log.isDebugEnabled()) {
            log.debug("Servlet creates");
        }
    }

    @Override
    public void init() throws ServletException {
        ServletContext servletContext = getServletContext();
        orderService = (OrderService) servletContext
                .getAttribute(ServiceParameters.ORDER__SERVICE);
        if (orderService == null) {
            log.error("Order service attribute is not exists.");
            throw new IllegalStateException(
                    "Order service attribute is not exists.");
        }
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        if (log.isDebugEnabled()) {
            log.debug("GET method starts");
        }
        HttpSession session = request.getSession();
        Order order = (Order) session.getAttribute(OrderParameters.ORDER);
        session.removeAttribute(OrderParameters.ORDER);
        if (log.isTraceEnabled()) {
            log.trace("order --> " + order);
        }
        if (order != null) {
            try {
                User user = (User) session.getAttribute(UserParameters.USER);
                long orderId = orderService.getLastOrderId(user.getId());
                request.setAttribute("orderId", orderId);
                RequestDispatcher dispatcher = request
                        .getRequestDispatcher(Path.PAGE__CONFIRM_ORDER);
                dispatcher.forward(request, response);
            } catch (DAOException e) {
                log.error(e.getMessage());
                response.sendRedirect(Path.PAGE__ERROR_PAGE);
            }
        } else {
            response.sendRedirect(Path.COMMAND__BASKET);
        }
        if (log.isDebugEnabled()) {
            log.debug("Response was sent");
        }
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {
        if (log.isDebugEnabled()) {
            log.debug("POST method starts");
        }
        HttpSession session = request.getSession();
        Order order = (Order) session.getAttribute(OrderParameters.ORDER);
        try {
            if (order != null) {
                session.removeAttribute(BasketParameters.BASKET);
                order.setOrderStatus(OrderStatus.EXECUTION);
                orderService.add(order);
                response.sendRedirect(Path.COMMAND__CONFIRM_ORDER);
            } else {
                response.sendRedirect(Path.COMMAND__BASKET);
            }
        } catch (DAOException e) {
            log.error(e.getMessage());
            response.sendRedirect(Path.PAGE__ERROR_PAGE);
        }
        if (log.isDebugEnabled()) {
            log.debug("Response was sent");
        }
    }
}
