package ua.kharkov.nure.semerkov.Shop.parameter;

/**
 * Holder for error parameters names.
 *
 * @author Volodymyr_Semerkov
 */
public final class ErrorParameters {
    public static final String ERROR = "error";
    public static final String ADDRESS_ERROR = "error.address";
    public static final String CARD_NUMBER_ERROR = "error.card.number";
    public static final String CVV_ERROR = "error.cvv";
    public static final String ORDER_EXECUTING_ERROR = "error.order.executing";
    public static final String CATEGORY_EXISTING_ERROR = "error.category.existing";
    public static final String CATEGORY_INCORRECT_ERROR = "error.category.incorrect";
    public static final String MANUFACTURER_EXISTING_ERROR = "error.manufacturer.existing";
    public static final String MANUFACTURER_INCORRECT_ERROR = "error.manufacturer.incorrect";
}
