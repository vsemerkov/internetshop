package ua.kharkov.nure.semerkov.Shop.dao.db.search.product;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ua.kharkov.nure.semerkov.Shop.parameter.SearchParameters;

/**
 * Search parameters parser. Gets search parameters from request.
 *
 * @author Volodymyr_Semerkov
 */
public class ProductSearchParametersParser {
    public static ProductSearchParametersBean getSearchParametersBean(
            Map<String, String[]> requestParameters) {
        ProductSearchParametersBean searchParametersBean = new ProductSearchParametersBean();
        if (requestParameters.containsKey(SearchParameters.MIN_PRICE)) {
            try {
                double minPrice = Double.parseDouble(requestParameters
                        .get(SearchParameters.MIN_PRICE)[0]);
                searchParametersBean.setMinPrice(minPrice);
                searchParametersBean.setMinPriceSelected(true);
            } catch (NumberFormatException e) {
            }
        }
        if (requestParameters.containsKey(SearchParameters.MAX_PRICE)) {
            try {
                double maxPrice = Double.parseDouble(requestParameters
                        .get(SearchParameters.MAX_PRICE)[0]);
                searchParametersBean.setMaxPrice(maxPrice);
                searchParametersBean.setMaxPriceSelected(true);
            } catch (NumberFormatException e) {
            }
        }
        if (requestParameters.containsKey(SearchParameters.CATEGORY)) {
            List<Long> categories = new ArrayList<Long>();
            String[] categoriesIdValues = requestParameters
                    .get(SearchParameters.CATEGORY);
            for (String categoryId : categoriesIdValues) {
                try {
                    categories.add(Long.parseLong(categoryId));
                } catch (NumberFormatException e) {
                }
            }
            searchParametersBean.setCategories(categories);
        }
        if (requestParameters.containsKey(SearchParameters.MANUFACTURER)) {
            List<Long> manufacturers = new ArrayList<Long>();
            String[] manufacturersIdValues = requestParameters
                    .get(SearchParameters.MANUFACTURER);
            for (String manufacturerId : manufacturersIdValues) {
                try {
                    manufacturers.add(Long.parseLong(manufacturerId));
                } catch (NumberFormatException e) {
                }
            }
            searchParametersBean.setManufacturers(manufacturers);
        }
        if (requestParameters.containsKey(SearchParameters.NAME)) {
            if (!requestParameters.get(SearchParameters.NAME)[0].trim()
                    .isEmpty()) {
                searchParametersBean.setProductName(requestParameters
                        .get(SearchParameters.NAME)[0]);
            }
        }
        if (requestParameters.containsKey(SearchParameters.SORT_PARAMETER)) {
            try {
                searchParametersBean.setSortParameter(SortParameter
                        .valueOf(requestParameters
                                .get(SearchParameters.SORT_PARAMETER)[0]
                                .toUpperCase()));
                if (requestParameters.containsKey(SearchParameters.SORT_ORDER)) {
                    try {
                        searchParametersBean.setSortOrder(SortOrder
                                .valueOf(requestParameters
                                        .get(SearchParameters.SORT_ORDER)[0]
                                        .toUpperCase()));
                    } catch (IllegalArgumentException e) {
                    }
                }
            } catch (IllegalArgumentException e) {
            }
        }
        int productsOnPage = 5;
        if (requestParameters.containsKey(SearchParameters.ELEMENT_NUMBER)) {
            try {
                productsOnPage = Integer.parseInt(requestParameters
                        .get(SearchParameters.ELEMENT_NUMBER)[0]);
                if (productsOnPage <= 0) {
                    productsOnPage = 5;
                }
            } catch (NumberFormatException e) {
            }
        }
        searchParametersBean.setProductsOnPage(productsOnPage);
        int page = 1;
        if (requestParameters.containsKey(SearchParameters.PAGE)) {
            try {
                page = Integer.parseInt(requestParameters
                        .get(SearchParameters.PAGE)[0]);
            } catch (NumberFormatException e) {
            }
        }
        searchParametersBean.setPageNumber(page);
        return searchParametersBean;
    }
}
