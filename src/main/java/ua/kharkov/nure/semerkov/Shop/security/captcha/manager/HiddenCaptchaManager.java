package ua.kharkov.nure.semerkov.Shop.security.captcha.manager;

import java.util.concurrent.atomic.AtomicLong;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.kharkov.nure.semerkov.Shop.parameter.ServiceParameters;
import ua.kharkov.nure.semerkov.Shop.parameter.UserParameters;
import ua.kharkov.nure.semerkov.Shop.security.captcha.CaptchaTimeoutCheck;
import ua.kharkov.nure.semerkov.Shop.security.captcha.container.CaptchaContainer;

public class HiddenCaptchaManager implements CaptchaManager {
	private AtomicLong counter = new AtomicLong();
	private CaptchaContainer captchaContainer = new CaptchaContainer();
	private Thread captchaTimeoutCheck;
	private static final Logger log = Logger
			.getLogger(HiddenCaptchaManager.class);

	public HiddenCaptchaManager(int timeout, int checkPeriod) {
		captchaTimeoutCheck = new Thread(new CaptchaTimeoutCheck(checkPeriod,
				timeout, captchaContainer));
		captchaTimeoutCheck.start();
	}

	@Override
	public void setCaptchaCode(HttpServletRequest request,
			HttpServletResponse response, String code) {
		try {
			long id = Long.parseLong(request
					.getParameter(ServiceParameters.CAPTCHA__ID));
			if (log.isTraceEnabled()) {
				log.trace("id --> " + id);
				log.trace("code --> " + code);
			}
			captchaContainer.addCaptcha(id, code);
		} catch (NullPointerException | NumberFormatException e) {
			throw new IllegalStateException("Captcha id is null!");
		}
	}

	@Override
	public boolean validateCaptcha(HttpServletRequest request) {
		try {
			long id = Long.parseLong(request
					.getParameter(ServiceParameters.CAPTCHA__ID));
			if (log.isTraceEnabled()) {
				log.trace("id --> " + id);
			}
			String userCaptchaCode = (String) request
					.getParameter(UserParameters.USER__CAPTCHA);
			return captchaContainer.containsCaptcha(id, userCaptchaCode);
		} catch (NullPointerException | NumberFormatException e) {
			log.error("NullPointerException");
			return false;
		}
	}

	@Override
	public void setId(HttpServletRequest request, HttpServletResponse response) {
		request.setAttribute(ServiceParameters.CAPTCHA__ID,
				counter.getAndIncrement());
	}
}
