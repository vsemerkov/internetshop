package ua.kharkov.nure.semerkov.Shop.dao;

import java.util.List;

import ua.kharkov.nure.semerkov.Shop.dao.exception.DAOException;
import ua.kharkov.nure.semerkov.Shop.entity.Manufacturer;

/**
 * Interface for access to manufacturer data.
 * 
 * @author Volodymyr_Semrkov
 * 
 */
public interface ManufacturerDAO {
	public boolean containsManufacturer(String name) throws DAOException;

	public boolean addManufacturer(Manufacturer manufacturer)
			throws DAOException;

	public Manufacturer getManufacturer(long id) throws DAOException;

	public boolean removeManufacturer(long id) throws DAOException;

	public boolean updateManufacturer(Manufacturer manufacturer)
			throws DAOException;

	public List<Manufacturer> getAllManufacturers() throws DAOException;

    public long getLastManufacturerId() throws DAOException;
}
