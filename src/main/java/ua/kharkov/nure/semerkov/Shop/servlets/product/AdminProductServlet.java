package ua.kharkov.nure.semerkov.Shop.servlets.product;

import org.apache.log4j.Logger;
import ua.kharkov.nure.semerkov.Shop.Path;
import ua.kharkov.nure.semerkov.Shop.dao.exception.DAOException;
import ua.kharkov.nure.semerkov.Shop.entity.Category;
import ua.kharkov.nure.semerkov.Shop.entity.Manufacturer;
import ua.kharkov.nure.semerkov.Shop.entity.Product;
import ua.kharkov.nure.semerkov.Shop.parameter.ErrorParameters;
import ua.kharkov.nure.semerkov.Shop.parameter.OrderParameters;
import ua.kharkov.nure.semerkov.Shop.parameter.ServiceParameters;
import ua.kharkov.nure.semerkov.Shop.service.CategoryService;
import ua.kharkov.nure.semerkov.Shop.service.ManufacturerService;
import ua.kharkov.nure.semerkov.Shop.service.ProductService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Servlet implementation class ProductServlet
 */
public class AdminProductServlet extends HttpServlet {
    private ProductService productService;
    private ManufacturerService manufacturerService;
    private CategoryService categoryService;
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AdminProductServlet.class);

    /**
     * @see javax.servlet.http.HttpServlet#HttpServlet()
     */
    public AdminProductServlet() {
        super();
        if (log.isDebugEnabled()) {
            log.debug("Servlet creates");
        }
    }

    @Override
    public void init() throws ServletException {
        ServletContext servletContext = getServletContext();
        productService = (ProductService) servletContext
                .getAttribute(ServiceParameters.PRODUCT__SERVICE);
        if (productService == null) {
            log.error("Product service attribute is not exists.");
            throw new IllegalStateException(
                    "Product service attribute is not exists.");
        }
        manufacturerService = (ManufacturerService) servletContext
                .getAttribute(ServiceParameters.MANUFACTURER__SERVICE);
        if (productService == null) {
            log.error("Manufacturer service attribute is not exists.");
            throw new IllegalStateException(
                    "Manufacturer service attribute is not exists.");
        }
        categoryService = (CategoryService) servletContext
                .getAttribute(ServiceParameters.CATEGORY__SERVICE);
        if (productService == null) {
            log.error("Category service attribute is not exists.");
            throw new IllegalStateException(
                    "Category service attribute is not exists.");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        if (log.isDebugEnabled()) {
            log.debug("GET method starts");
        }
        HttpSession session = request.getSession();
        request.setAttribute(ErrorParameters.ERROR, session.getAttribute(ErrorParameters.ERROR));
        session.removeAttribute(ErrorParameters.ERROR);
        try {
            long productId = 0;
            if (request.getParameter(OrderParameters.PRODUCT_ID) != null) {
                productId = Long.parseLong(request
                        .getParameter(OrderParameters.PRODUCT_ID));
            }
            if (productId != 0) {
                Product product = productService.get(productId);
                request.setAttribute(OrderParameters.PRODUCT_ID, productId);
                request.setAttribute(OrderParameters.PRODUCT, product);
            }
            List<Manufacturer> manufacturers = manufacturerService.getAllManufacturers();
            request.setAttribute(OrderParameters.MANUFACTURERS, manufacturers);
            List<Category> categories = categoryService.getAllCategories();
            request.setAttribute(OrderParameters.CATEGORIES, categories);
            RequestDispatcher dispatcher = request
                    .getRequestDispatcher(Path.PAGE__PRODUCT);
            dispatcher.forward(request, response);
        } catch (DAOException e) {
            log.error(e.getMessage());
            response.sendRedirect(Path.PAGE__ERROR_PAGE);
        }
        if (log.isDebugEnabled()) {
            log.debug("Response was sent");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {
        if (log.isDebugEnabled()) {
            log.debug("POST method starts");
        }
        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("Response was sent");
        }
    }
}
