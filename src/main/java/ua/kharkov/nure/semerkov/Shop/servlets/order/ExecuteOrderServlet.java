package ua.kharkov.nure.semerkov.Shop.servlets.order;

import org.apache.log4j.Logger;
import ua.kharkov.nure.semerkov.Shop.Path;
import ua.kharkov.nure.semerkov.Shop.Role;
import ua.kharkov.nure.semerkov.Shop.dao.exception.DAOException;
import ua.kharkov.nure.semerkov.Shop.entity.*;
import ua.kharkov.nure.semerkov.Shop.parameter.ErrorParameters;
import ua.kharkov.nure.semerkov.Shop.parameter.OrderParameters;
import ua.kharkov.nure.semerkov.Shop.parameter.ServiceParameters;
import ua.kharkov.nure.semerkov.Shop.parameter.UserParameters;
import ua.kharkov.nure.semerkov.Shop.service.OrderService;
import ua.kharkov.nure.semerkov.Shop.service.ProductService;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Servlet implementation class ExecuteOrderServlet
 */
public class ExecuteOrderServlet extends HttpServlet {
    private OrderService orderService;
    private ProductService productService;
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ExecuteOrderServlet.class);

    /**
     * @see javax.servlet.http.HttpServlet#HttpServlet()
     */
    public ExecuteOrderServlet() {
        super();
        if (log.isDebugEnabled()) {
            log.debug("Servlet creates");
        }
    }

    @Override
    public void init() throws ServletException {
        ServletContext servletContext = getServletContext();
        orderService = (OrderService) servletContext
                .getAttribute(ServiceParameters.ORDER__SERVICE);
        if (orderService == null) {
            log.error("Order service attribute is not exists.");
            throw new IllegalStateException(
                    "Order service attribute is not exists.");
        }
        productService = (ProductService) servletContext
                .getAttribute(ServiceParameters.PRODUCT__SERVICE);
        if (productService == null) {
            log.error("Product service attribute is not exists.");
            throw new IllegalStateException(
                    "Product service attribute is not exists.");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {
        if (log.isDebugEnabled()) {
            log.debug("GET method starts");
        }
        HttpSession session = request.getSession();
        try {
            Order order = null;
            List<OrderItem> orderItems = null;
            long orderId = Long.parseLong(request
                    .getParameter(OrderParameters.ORDER_ID));
            order = orderService.getOrder(orderId);
            boolean result = true;
            for (OrderedProduct orderedProduct : order.getOrderedProducts()) {
                long productId = orderedProduct.getProductId();
                Product product = productService.get(productId);
                int totalNumber = product.getNumber();
                if (orderedProduct.getNumber() > totalNumber) {
                    result = false;
                }
            }
            if (result) {
                for (OrderedProduct orderedProduct : order.getOrderedProducts()) {
                    long productId = orderedProduct.getProductId();
                    Product product = productService.get(productId);
                    product.setNumber(product.getNumber() - orderedProduct.getNumber());
                    productService.update(product);
                }
                orderService.executeOrder(orderId);
            } else {
                session.setAttribute(ErrorParameters.ERROR, ErrorParameters.ORDER_EXECUTING_ERROR);
            }
            session.setAttribute(OrderParameters.ORDER_ID, orderId);
            response.sendRedirect(Path.COMMAND__ORDER);
        } catch (NullPointerException | NumberFormatException | DAOException e) {
            log.error(e.getMessage());
            response.sendRedirect(Path.PAGE__ERROR_PAGE);
        }
        if (log.isDebugEnabled()) {
            log.debug("Response was sent");
        }
    }
}
