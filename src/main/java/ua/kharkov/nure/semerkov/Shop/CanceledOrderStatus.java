package ua.kharkov.nure.semerkov.Shop;

/**
 * The <code>CanceledOrderStatus</code> class contains the enumeration of cancel order statuses.
 * 
 * @author Volodymyr_Semerkov
 * 
 */
public enum CanceledOrderStatus {
	CLIENT, SHOP;
}