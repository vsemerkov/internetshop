package ua.kharkov.nure.semerkov.Shop.parameter;

/**
 * Holder for service parameters names.
 * 
 * @author Volodymyr_Semerkov
 * 
 */
public final class ServiceParameters {
	// services
	public static final String USER__SERVICE = "userService";
	public static final String USER_IMAGE__SERVICE = "userImageService";
	public static final String PRODUCT_IMAGE__SERVICE = "productImageService";
	public static final String CATEGORY__SERVICE = "categoryService";
	public static final String MANUFACTURER__SERVICE = "manufacturerService";
	public static final String PRODUCT__SERVICE = "productService";
	public static final String ORDER__SERVICE = "orderService";

	// captcha parameters
	public static final String CAPTCHA__MODE = "captchaMode";
	public static final String CAPTCHA__MODE_SESSION = "session";
	public static final String CAPTCHA__MODE_COOKIES = "cookies";
	public static final String CAPTCHA__MODE_HIDDEN = "hidden";
	public static final String CAPTCHA__MANAGER = "captchaManager";
	public static final String CAPTCHA__ID = "captchaId";
	public static final String CAPTCHA__CODE = "captchaCode";
	public static final String CAPTCHA__TIME_OUT = "captchaTimeout";

	// captcha thread parameters
	public static final String CHECK_TIME = "checkPeriod";

	// image parameters
	public static final String IMAGE__USER_FOLDER = "userImageFolder";
	public static final String IMAGE__DEFAULT = "defaultImage";
	public static final String IMAGE__MAX_HEIGHT = "maxHeight";
	public static final String IMAGE__MAX_WIDTH = "maxWidth";

	// locales parameters
	public static final String SUPPORTED_LOCALES = "supportedLocales";
	public static final String DEFAULT_LOCALE = "defaultLocale";
	public static final String USER_LOCALE = "locale";
	public static final String LOCALE_MANAGER = "localeManager";
	public static final String SESSION_MANAGER = "session";
	public static final String COOKIES_MANAGER = "cookies";
	public static final String COOKIES_TIME_OUT = "cookiesTimeout";
	public static final String LANG_PARAMETER = "lang";

	// user service parameters
	public static final long LOCKING_TIME = 1800000;

	// security parameters
	public static final String SECURITY_XML = "securityXML";
	public static final String SECURITY_MANAGER = "securityManager";

	// product parameters
	public static final String IMAGE__PRODUCT_FOLDER = "productImageFolder";
	public static final String PAGE_PARAMETER = "page";
}
