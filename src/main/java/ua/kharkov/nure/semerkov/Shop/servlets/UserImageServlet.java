package ua.kharkov.nure.semerkov.Shop.servlets;

import java.io.IOException;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.kharkov.nure.semerkov.Shop.parameter.ServiceParameters;
import ua.kharkov.nure.semerkov.Shop.parameter.UserParameters;
import ua.kharkov.nure.semerkov.Shop.entity.Image;
import ua.kharkov.nure.semerkov.Shop.entity.User;
import ua.kharkov.nure.semerkov.Shop.service.UserImageService;

/**
 * Servlet implementation class UserImageServlet
 */
public class UserImageServlet extends HttpServlet {
	private UserImageService imageService;
	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(UserImageServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserImageServlet() {
		super();
		if (log.isDebugEnabled()) {
			log.debug("Servlet creates");
		}
	}

	@Override
	public void init() throws ServletException {
		ServletContext servletContext = getServletContext();
		imageService = (UserImageService) servletContext
				.getAttribute(ServiceParameters.USER_IMAGE__SERVICE);
		if (imageService == null) {
			log.error("User image service attribute is not exists.");
			throw new IllegalStateException(
					"User image service attribute is not exists.");
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		if (log.isDebugEnabled()) {
			log.debug("GET request");
		}
		response.setContentType("image/png");
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute(UserParameters.USER);
		Image image = imageService.get(user.getImageName());
		ImageIO.write(image.getBufferedImage(), "png",
				response.getOutputStream());
		if (log.isDebugEnabled()) {
			log.debug("Response was sent");
		}
	}
}
