package ua.kharkov.nure.semerkov.Shop.service;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import ua.kharkov.nure.semerkov.Shop.entity.Image;

/**
 * This class contains methods for actions with products images.
 * 
 * @author Volodymyr_Semrkov
 * 
 */
public class ProductImageService {
	private String productImageFolderPath;
	private String defaultImagePath;

	public ProductImageService(String productImageFolderPath,
			String defaultImagePath) {
		this.productImageFolderPath = productImageFolderPath;
		File imageFolder = new File(productImageFolderPath);
		if (!(imageFolder.exists() && imageFolder.isDirectory())) {
			imageFolder.mkdir();
		}
		this.defaultImagePath = defaultImagePath;
	}

	public Image get(String imageName) throws IOException {
		File imageFile = null;
		boolean correctImageName = true;
		if (imageName != null) {
			imageFile = new File(productImageFolderPath + "//" + imageName);
		} else {
			correctImageName = false;
		}
		if (!correctImageName || !imageFile.exists()) {
			imageFile = new File(defaultImagePath);
		}
		BufferedImage bufferedImage = ImageIO.read(imageFile);
		return new Image(imageName, bufferedImage);
	}
}
