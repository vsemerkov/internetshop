package ua.kharkov.nure.semerkov.Shop.filter.localization;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ua.kharkov.nure.semerkov.Shop.parameter.ServiceParameters;

/**
 * Session locale manager.
 * 
 * @author Volodymyr_Semerkov
 * 
 */
public class SessionLocaleManager implements LocaleManager {
	@Override
	public Locale getLocale(HttpServletRequest request) {
		HttpSession session = request.getSession();
		return (Locale) session.getAttribute(ServiceParameters.USER_LOCALE);
	}

	@Override
	public void setLocale(Locale locale, HttpServletRequest request,
			HttpServletResponse response) {
		HttpSession session = request.getSession();
		session.setAttribute(ServiceParameters.USER_LOCALE, locale);
	}
}
