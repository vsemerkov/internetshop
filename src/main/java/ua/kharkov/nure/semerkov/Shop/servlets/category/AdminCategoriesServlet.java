package ua.kharkov.nure.semerkov.Shop.servlets.category;

import org.apache.log4j.Logger;
import ua.kharkov.nure.semerkov.Shop.Path;
import ua.kharkov.nure.semerkov.Shop.dao.exception.DAOException;
import ua.kharkov.nure.semerkov.Shop.entity.Category;
import ua.kharkov.nure.semerkov.Shop.parameter.OrderParameters;
import ua.kharkov.nure.semerkov.Shop.parameter.ServiceParameters;
import ua.kharkov.nure.semerkov.Shop.service.CategoryService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Servlet implementation class AdminCategoriesServlet
 */
public class AdminCategoriesServlet extends HttpServlet {
    private CategoryService categoryService;
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AdminCategoriesServlet.class);

    /**
     * @see javax.servlet.http.HttpServlet#HttpServlet()
     */
    public AdminCategoriesServlet() {
        super();
        if (log.isDebugEnabled()) {
            log.debug("Servlet creates");
        }
    }

    @Override
    public void init() throws ServletException {
        ServletContext servletContext = getServletContext();
        categoryService = (CategoryService) servletContext
                .getAttribute(ServiceParameters.CATEGORY__SERVICE);
        if (categoryService == null) {
            log.error("Category service attribute is not exists.");
            throw new IllegalStateException(
                    "Category service attribute is not exists.");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        if (log.isDebugEnabled()) {
            log.debug("GET method starts");
        }
        try {
            List<Category> categories = categoryService.getAllCategories();
            request.setAttribute(OrderParameters.CATEGORIES, categories);
            RequestDispatcher dispatcher = request
                    .getRequestDispatcher(Path.PAGE__ADMIN_CATEGORIES);
            dispatcher.forward(request, response);
        } catch (DAOException e) {
            log.error(e.getMessage());
            response.sendRedirect(Path.PAGE__ERROR_PAGE);
        }
        if (log.isDebugEnabled()) {
            log.debug("Response was sent");
        }
    }
}
