package ua.kharkov.nure.semerkov.Shop.entity;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import ua.kharkov.nure.semerkov.Shop.Role;

/**
 * The <code>User</code> class represents user registration data.
 * 
 * @author Volodymyr_Semerkov
 * 
 */
public class User implements Serializable, Cloneable {
	private static final long serialVersionUID = -2585731144077953211L;
	private long id;
	private String firstName;
	private String lastName;
	private String email;
	private String password;
	private boolean subscribe;
	private Role role;
	private String imageName;
	private Date lastLogin;
	private int attemptNumber;
	private Date unlockingDate;

	public User() {
	}

	public User(long id, String email) {
		this.id = id;
		this.email = email;
	}

	public User(String firstName, String lastName, String email,
			String password, boolean subscribe) {
		this(firstName, lastName, email, password, subscribe, Role.CLIENT,
				Calendar.getInstance().getTime(), 0);
	}

	public User(String firstName, String lastName, String email,
			String password, boolean subscribe, Role role, Date lastLogin,
			int attemptNumber) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.subscribe = subscribe;
		this.role = role;
		this.lastLogin = lastLogin;
		this.attemptNumber = attemptNumber;
	}

	public long getId() {
		return id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isSubscribe() {
		return subscribe;
	}

	public void setSubscribe(boolean subscribe) {
		this.subscribe = subscribe;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imagePath) {
		this.imageName = imagePath;
	}

	public Date getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	public int getAttemptNumber() {
		return attemptNumber;
	}

	public void setAttemptNumber(int attemptNumber) {
		this.attemptNumber = attemptNumber;
	}

	public Date getUnlockingDate() {
		return unlockingDate;
	}

	public void setUnlockingDate(Date unlockingDate) {
		this.unlockingDate = unlockingDate;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("User [id=");
		sb.append(id);
		sb.append(", firstName=");
		sb.append(firstName);
		sb.append(", lastName=");
		sb.append(lastName);
		sb.append(", email=");
		sb.append(email);
		sb.append(", subscribe=");
		sb.append(subscribe);
		sb.append(", role=");
		sb.append(role);
		sb.append(", imageName=");
		sb.append(imageName);
		sb.append(", previousEntryDate=");
		sb.append(lastLogin);
		sb.append(", attemptNumber=");
		sb.append(attemptNumber);
		sb.append(", unlockingDate=");
		sb.append(unlockingDate);
		sb.append("]");
		return sb.toString();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || (obj.getClass() != this.getClass()))
			return false;
		User otherUser = (User) obj;
		return this.getEmail().equals(otherUser.getEmail());
	}

	@Override
	public int hashCode() {
		return this.email.hashCode() * 31;
	}

	@Override
	public User clone() throws CloneNotSupportedException {
		return (User) super.clone();
	}
}
