package ua.kharkov.nure.semerkov.Shop.service;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import ua.kharkov.nure.semerkov.Shop.CanceledOrderStatus;
import ua.kharkov.nure.semerkov.Shop.OrderStatus;
import ua.kharkov.nure.semerkov.Shop.Role;
import ua.kharkov.nure.semerkov.Shop.dao.OrderDAO;
import ua.kharkov.nure.semerkov.Shop.dao.OrderedProductDAO;
import ua.kharkov.nure.semerkov.Shop.dao.ProductDAO;
import ua.kharkov.nure.semerkov.Shop.dao.db.jdbc.JdbcTransactionManager;
import ua.kharkov.nure.semerkov.Shop.dao.db.search.order.OrderSearchParametersBean;
import ua.kharkov.nure.semerkov.Shop.dao.db.transaction.TransactionManager;
import ua.kharkov.nure.semerkov.Shop.dao.db.transaction.TransactionOperation;
import ua.kharkov.nure.semerkov.Shop.dao.exception.DAOException;
import ua.kharkov.nure.semerkov.Shop.entity.Order;
import ua.kharkov.nure.semerkov.Shop.entity.OrderedProduct;
import ua.kharkov.nure.semerkov.Shop.entity.Product;

/**
 * This class contains methods for actions with orders data.
 *
 * @author Volodymyr_Semrkov
 */
public class OrderService {
    private OrderDAO orderDAO;
    private OrderedProductDAO orderedProductDAO;
    private TransactionManager transactionManager;
    private static final Logger log = Logger.getLogger(OrderService.class);

    public OrderService(OrderDAO orderDAO, OrderedProductDAO orderedProductDAO) {
        if (log.isDebugEnabled()) {
            log.debug("Order service creates");
        }
        this.orderDAO = orderDAO;
        this.orderedProductDAO = orderedProductDAO;
        this.transactionManager = new JdbcTransactionManager();
    }

    /**
     * Find order data in database.
     *
     * @param id Order id.
     * @return True if order data contains in database, else False.
     * @throws DAOException
     */
    public boolean contains(final long id) throws DAOException {
        return (boolean) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        return orderDAO.containsOrder(id);
                    }
                });
    }

    /**
     * Add order data in database.
     *
     * @param order Order data.
     * @return True if order data not contains in database, else False.
     * @throws DAOException
     */
    public void add(final Order order) throws DAOException {
        transactionManager.doInTransaction(new TransactionOperation() {
            @Override
            public Object execute() throws DAOException {
                orderDAO.addOrder(order);
                long orderId = orderDAO.getLastOrderId(order.getUserId());
                List<OrderedProduct> orderedProducts = order
                        .getOrderedProducts();
                for (OrderedProduct orderedProduct : orderedProducts) {
                    orderedProduct.setOrderId(orderId);
                }
                orderedProductDAO.addOrderedProducts(order.getOrderedProducts());
                return null;
            }
        });
    }

    /**
     * Remove order from database.
     *
     * @param id Order id.
     * @return True if order data was removed from database, else False.
     * @throws DAOException
     */
    public boolean remove(final long id) throws DAOException {
        return (boolean) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        return orderDAO.removeOrder(id);
                    }
                });
    }

    /**
     * Update order status.
     *
     * @param order Order data.
     * @return True if order status was updated in database, else False.
     * @throws DAOException
     */
    public boolean updateOrderStatus(final Order order) throws DAOException {
        return (boolean) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        return orderDAO.updateOrderStatus(order.getId(),
                                order.getOrderStatus(), order.getCanceledOrderStatus());
                    }
                });
    }

    /**
     * Get orders by user id.
     *
     * @param userId User id.
     * @return List of orders.
     * @throws DAOException
     */
    @SuppressWarnings("unchecked")
    public List<Order> getOrders(final long userId) throws DAOException {
        return (List<Order>) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        List<Order> orders = orderDAO.getOrders(userId);
                        for (Order order : orders) {
                            List<OrderedProduct> orderedProducts = orderedProductDAO.getOrderedProducts(order.getId());
                            order.setOrderedProducts(orderedProducts);
                        }
                        return orders;
                    }
                });
    }

    /**
     * Get order by order id.
     *
     * @param orderId Order id.
     * @return Order.
     * @throws DAOException
     */
    public Order getOrder(final long orderId) throws DAOException {
        return (Order) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        Order order = orderDAO.getOrder(orderId);
                        List<OrderedProduct> orderedProducts = orderedProductDAO.getOrderedProducts(orderId);
                        order.setOrderedProducts(orderedProducts);
                        return order;
                    }
                });
    }

    /**
     * Get orders by user id.
     *
     * @param userId User id.
     * @param from   First date.
     * @param to     End date.
     * @return List of orders.
     * @throws DAOException
     */
    @SuppressWarnings("unchecked")
    public List<Order> getOrders(final long userId, final Date from,
                                 final Date to) throws DAOException {
        return (List<Order>) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        return orderDAO.getOrders(userId, from, to);
                    }
                });
    }

    /**
     * Get last order id by user id.
     *
     * @param userId User id.
     * @return Last order id.
     * @throws DAOException
     */
    public long getLastOrderId(final long userId) throws DAOException {
        return (long) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        return orderDAO.getLastOrderId(userId);
                    }
                });
    }

    /**
     * Find order data in database by order ID and user ID.
     *
     * @param orderId Order id.
     * @param userId  User id.
     * @return True if order data contains in database, else False.
     * @throws DAOException
     */
    public boolean belong(final long orderId, final long userId) throws DAOException {
        return (boolean) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        return orderDAO.belong(orderId, userId);
                    }
                });
    }

    /**
     * Cancel order.
     *
     * @param orderId Order id.
     * @return True if order canceled, else False.
     * @throws DAOException
     */
    public boolean cancelOrder(final long orderId, final Role userRole) throws DAOException {
        return (boolean) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        Order order = getOrder(orderId);
                        order.setOrderStatus(OrderStatus.CANCELED);
                        if (userRole == Role.CLIENT) {
                            order.setCanceledOrderStatus(CanceledOrderStatus.CLIENT);
                        } else {
                            order.setCanceledOrderStatus(CanceledOrderStatus.SHOP);
                        }
                        return updateOrderStatus(order);
                    }
                });
    }

    /**
     * Execute order.
     *
     * @param orderId Order id.
     * @return True if order executed, else False.
     * @throws DAOException
     */
    public boolean executeOrder(final long orderId) throws DAOException {
        return (boolean) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        Order order = getOrder(orderId);
                        order.setOrderStatus(OrderStatus.COMPLETED);
                        return updateOrderStatus(order);
                    }
                });
    }

    /**
     * Get number of required orders by search parameter bean.
     *
     * @param searchParametersBean Search parameter bean.
     * @return Number of required orders.
     * @throws DAOException
     */
    public int getNumberOfRequiredOrders(
            final OrderSearchParametersBean searchParametersBean)
            throws DAOException {
        return (int) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        return orderDAO
                                .getNumberOfRequiredOrders(searchParametersBean);
                    }
                });
    }

    /**
     * Find orders by search parameter bean.
     *
     * @param searchParametersBean Search parameter bean.
     * @return List of orders.
     * @throws DAOException
     */
    @SuppressWarnings("unchecked")
    public List<Order> findOrders(
            final OrderSearchParametersBean searchParametersBean)
            throws DAOException {
        return (List<Order>) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        List<Order> orders = orderDAO.findOrders(searchParametersBean);
                        for (Order order : orders) {
                            List<OrderedProduct> orderedProducts = orderedProductDAO.getOrderedProducts(order.getId());
                            order.setOrderedProducts(orderedProducts);
                        }
                        return orders;
                    }
                });
    }
}
