package ua.kharkov.nure.semerkov.Shop.security;

public class PaymentValidator {

    public static boolean checkCardNumber(String cardNumber) {
        if (cardNumber == null || cardNumber.isEmpty() || cardNumber.length() != 16)
            return false;
        int n = cardNumber.length();
        int sum = 0;
        for (int i = 0; i < n; i++) {
            int number = cardNumber.charAt(i) - 48;
            if (number < 0 || number > 9) return false;
            if (n % 2 == 0) {
                if (i % 2 == 0) {
                    number *= 2;
                }
            } else {
                if (i % 2 != 0) {
                    number *= 2;
                }
            }
            sum += number < 10 ? number : number - 9;
        }
        return sum % 10 == 0;
    }

    public static boolean checkCVV(String cvv) {
        if (cvv == null || cvv.isEmpty() || cvv.length() != 3) return false;
        try {
            Integer.parseInt(cvv);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }
}
