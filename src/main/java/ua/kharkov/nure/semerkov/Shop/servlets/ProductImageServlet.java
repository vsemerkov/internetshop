package ua.kharkov.nure.semerkov.Shop.servlets;

import java.io.IOException;

import javax.imageio.ImageIO;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.kharkov.nure.semerkov.Shop.Path;
import ua.kharkov.nure.semerkov.Shop.parameter.ServiceParameters;
import ua.kharkov.nure.semerkov.Shop.dao.exception.DAOException;
import ua.kharkov.nure.semerkov.Shop.entity.Image;
import ua.kharkov.nure.semerkov.Shop.service.ProductImageService;
import ua.kharkov.nure.semerkov.Shop.service.ProductService;

/**
 * Servlet implementation class ProductImageServlet
 */
public class ProductImageServlet extends HttpServlet {
	private ProductImageService imageService;
	private ProductService productService;
	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger
			.getLogger(ProductImageServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ProductImageServlet() {
		super();
		if (log.isDebugEnabled()) {
			log.debug("Servlet creates");
		}
	}

	@Override
	public void init() throws ServletException {
		ServletContext servletContext = getServletContext();
		imageService = (ProductImageService) servletContext
				.getAttribute(ServiceParameters.PRODUCT_IMAGE__SERVICE);
		if (imageService == null) {
			log.error("Product image service attribute is not exists.");
			throw new IllegalStateException(
					"Product image service attribute is not exists.");
		}
		productService = (ProductService) servletContext
				.getAttribute(ServiceParameters.PRODUCT__SERVICE);
		if (productService == null) {
			log.error("Product service attribute is not exists.");
			throw new IllegalStateException(
					"Product service attribute is not exists.");
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		if (log.isDebugEnabled()) {
			log.debug("GET request");
		}
		response.setContentType("image/png");
		try {
			long productId = Long.valueOf(request.getParameter("productId"));
			String imageName = productService.getImagePath(productId);
			Image image = imageService.get(imageName);
			ImageIO.write(image.getBufferedImage(), "png",
					response.getOutputStream());
			if (log.isDebugEnabled()) {
				log.debug("Response was sent");
			}
		} catch (NumberFormatException | DAOException e) {
			log.error("NumberFormatException: " + e.getMessage());
			RequestDispatcher dispatcher = request
					.getRequestDispatcher(Path.PAGE__ERROR_PAGE);
			dispatcher.forward(request, response);
		}
	}
}
