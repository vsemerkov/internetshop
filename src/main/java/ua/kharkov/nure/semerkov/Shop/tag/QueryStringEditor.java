package ua.kharkov.nure.semerkov.Shop.tag;

import java.io.IOException;
import java.util.StringTokenizer;

import javax.servlet.jsp.JspWriter;

/**
 * Query string editor.
 * 
 * @author Volodymyr_Semerkov
 * 
 */
public class QueryStringEditor {
	public static void addToQueryString(String queryString, JspWriter writer,
			String parameter, String value) throws IOException {
		String pageParam = parameter + "=" + value;
		String paramsString = null;
		if (queryString == null) {
			paramsString = "?" + pageParam;
		} else {
			StringTokenizer strTokenizer = new StringTokenizer(queryString, "&");
			if (strTokenizer.hasMoreElements()) {
				while (strTokenizer.hasMoreElements()) {
					String param = strTokenizer.nextToken();
					if (param.startsWith(parameter)) {
						queryString = queryString.replaceAll(param, "");
						queryString = queryString.replaceAll("&" + param, "");
					}
				}
			}
			if (!queryString.isEmpty()) {
				paramsString = "?" + queryString + "&" + pageParam;
			} else {
				paramsString = "?" + pageParam;
			}
		}
		writer.write(paramsString);
		writer.flush();
	}
}
