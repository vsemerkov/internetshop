package ua.kharkov.nure.semerkov.Shop.dao.db.jdbc;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import ua.kharkov.nure.semerkov.Shop.dao.db.transaction.TransactionManager;
import ua.kharkov.nure.semerkov.Shop.dao.db.transaction.TransactionOperation;
import ua.kharkov.nure.semerkov.Shop.dao.exception.DAOException;

/**
 * Transaction manager for JDBC technology.
 * 
 * @author Volodymyr_Semrkov
 * 
 */
public class JdbcTransactionManager implements TransactionManager {
	private static final Logger log = Logger
			.getLogger(JdbcTransactionManager.class);

	@Override
	public Object doInTransaction(TransactionOperation operation)
			throws DAOException {
		Connection connection = null;
		Object res = null;
		try {
			connection = JdbcHelper.getConnection();
			JdbcConnectionHolder.setConnection(connection);
			res = operation.execute();
			connection.commit();
		} catch (SQLException e) {
			log.error(e.getMessage());
			try {
				if (connection != null) {
					connection.rollback();
				}
			} catch (SQLException ex) {
				throw new DAOException(ex);
			}
		} finally {
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException e) {
				throw new DAOException(e);
			}
			JdbcConnectionHolder.removeConnection();
		}
		return res;
	}
}
