package ua.kharkov.nure.semerkov.Shop.servlets;

import java.io.IOException;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.log4j.Logger;

import ua.kharkov.nure.semerkov.Shop.Path;
import ua.kharkov.nure.semerkov.Shop.parameter.ServiceParameters;
import ua.kharkov.nure.semerkov.Shop.parameter.UserParameters;
import ua.kharkov.nure.semerkov.Shop.security.captcha.manager.CaptchaManager;
import ua.kharkov.nure.semerkov.Shop.convert.FormBeanConverter;
import ua.kharkov.nure.semerkov.Shop.dao.exception.DAOException;
import ua.kharkov.nure.semerkov.Shop.entity.FormBean;
import ua.kharkov.nure.semerkov.Shop.entity.Image;
import ua.kharkov.nure.semerkov.Shop.entity.User;
import ua.kharkov.nure.semerkov.Shop.service.UserImageService;
import ua.kharkov.nure.semerkov.Shop.service.UserService;
import ua.kharkov.nure.semerkov.Shop.validator.Validator;

/**
 * Servlet implementation class <code>RegistrationServlet</code>
 */
public class RegistrationServlet extends HttpServlet {
	private Validator validator = new Validator();
	private UserService userService;
	private UserImageService imageService;
	private CaptchaManager captchaManager;
	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger
			.getLogger(RegistrationServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RegistrationServlet() {
		super();
		if (log.isDebugEnabled()) {
			log.debug("Servlet creates");
		}
	}

	@Override
	public void init() throws ServletException {
		ServletContext servletContext = getServletContext();
		userService = (UserService) servletContext
				.getAttribute(ServiceParameters.USER__SERVICE);
		if (userService == null) {
			log.error("User service attribute is not exists.");
			throw new IllegalStateException(
					"User service attribute is not exists.");
		}
		imageService = (UserImageService) servletContext
				.getAttribute(ServiceParameters.USER_IMAGE__SERVICE);
		if (imageService == null) {
			log.error("Image service attribute is not exists.");
			throw new IllegalStateException(
					"Image service attribute is not exists.");
		}
		captchaManager = (CaptchaManager) servletContext
				.getAttribute(ServiceParameters.CAPTCHA__MANAGER);
		if (captchaManager == null) {
			log.error("Captcha manager attribute is not exists.");
			throw new IllegalStateException(
					"Captcha manager attribute is not exists.");
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		if (log.isDebugEnabled()) {
			log.debug("GET request");
		}
		setCaptchaId(request, response);
		RequestDispatcher dispatcher = request
				.getRequestDispatcher(Path.PAGE__SIGN_UP);
		dispatcher.forward(request, response);
		if (log.isDebugEnabled()) {
			log.debug("Response was sended");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		if (log.isDebugEnabled()) {
			log.debug("POST method starts");
		}
		FormBean formBean = getFormBean(request);
		Map<String, String> paramErrors = validator.validateFormBean(formBean);
		boolean positiveResponse = true;
		if (!captchaManager.validateCaptcha(request)) {
			paramErrors
					.put(UserParameters.USER__CAPTCHA,
							"Captcha length is not equals to 5 or captcha lifetime is out!");
			log.trace("Captcha length is not equals to 5 or captcha lifetime is out!");
			positiveResponse = false;
		}
		if (paramErrors.size() != 0 || !positiveResponse) {
			positiveResponse = false;
		}
		if (positiveResponse) {
			User user = FormBeanConverter.getUser(formBean);
			boolean isConsists = false;
			try {
				isConsists = userService.containsUser(user);
			} catch (DAOException e) {
				log.error(e.getMessage());
				response.sendRedirect(Path.PAGE__ERROR_PAGE);
				return;
			}
			if (isConsists == false) {
				if (log.isTraceEnabled()) {
					log.trace("User " + user.getEmail()
							+ " not exists in database");
				}
				try {
					Part part = request.getPart(UserParameters.USER__IMAGE);
					if (part != null
							&& part.getContentType().startsWith("image")
							&& part.getSize() != 0) {
						Image image = imageService.create(
								part.getInputStream(), user.getEmail());
						imageService.save(image);
						user.setImageName(image.getImageName());
					}
				} catch (IOException | IllegalStateException | ServletException e) {
					log.error("Image format is incorrect");
					paramErrors.put(UserParameters.USER__IMAGE,
							"Image format is incorrect");
					positiveResponse = false;
				}
				if (positiveResponse) {
					try {
						userService.add(user);
						if (log.isTraceEnabled()) {
							log.trace("User " + user.getEmail()
									+ " was added in database");
						}
						response.sendRedirect(Path.COMMAND__INDEX);
						if (log.isDebugEnabled()) {
							log.debug("Response was sent");
						}
						return;
					} catch (DAOException e) {
						paramErrors.put(UserParameters.USER, "Server error");
						positiveResponse = false;
					}
				}
			} else {
				if (log.isTraceEnabled()) {
					log.trace("User " + user.getEmail()
							+ " has existed in database");
				}
				paramErrors
						.put(UserParameters.USER,
								"User with such email address already exists in the shop!");
			}
		}
		if (log.isDebugEnabled()) {
			log.debug("Parameters not validate");
		}
		request.setAttribute("formBean", formBean);
		request.setAttribute("paramErrors", paramErrors);
		setCaptchaId(request, response);
		RequestDispatcher dispatcher = request
				.getRequestDispatcher(Path.PAGE__SIGN_UP);
		dispatcher.forward(request, response);
		if (log.isDebugEnabled()) {
			log.debug("Response was sent");
		}
	}

	/**
	 * Get <code>FormBean</code> object from request.
	 * 
	 * @param request
	 * @return <code>FormBean</code> object.
	 */
	private FormBean getFormBean(HttpServletRequest request) {
		FormBean formBean = new FormBean();
		formBean.setFirstName(request
				.getParameter(UserParameters.USER__FIRST_NAME));
		if (log.isTraceEnabled()) {
			log.trace("firstName --> " + formBean.getFirstName());
		}
		formBean.setLastName(request
				.getParameter(UserParameters.USER__LAST_NAME));
		if (log.isTraceEnabled()) {
			log.trace("lastName --> " + formBean.getLastName());
		}
		formBean.setEmail(request.getParameter(UserParameters.USER__EMAIL));
		if (log.isTraceEnabled()) {
			log.trace("email --> " + formBean.getEmail());
		}
		formBean.setPassword(request
				.getParameter(UserParameters.USER__PASSWORD));
		formBean.setPasswordRepeat(request
				.getParameter(UserParameters.USER__PASSWORD_REPEAT));
		formBean.setSubscribe(request
				.getParameter(UserParameters.USER__SUBSCRIBE) != null);
		if (log.isTraceEnabled()) {
			log.trace("subscribe --> " + formBean.isSubscribe());
		}
		return formBean;
	}

	private void setCaptchaId(HttpServletRequest request,
			HttpServletResponse response) {
		captchaManager.setId(request, response);
	}
}
