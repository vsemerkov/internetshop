package ua.kharkov.nure.semerkov.Shop.entity;

/**
 * The <code>FormBean</code> class represents user registration data from input
 * form.
 * 
 * @author Volodymyr_Semerkov
 * 
 */
public class FormBean {
	private String firstName;
	private String lastName;
	private String email;
	private String password;
	private String passwordRepeat;
	private boolean subscribe;

	public FormBean() {
	}

	public FormBean(String firstName, String lastName, String email,
			String password, String passwordRepeat, boolean subscribe) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.passwordRepeat = passwordRepeat;
		this.subscribe = subscribe;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordRepeat() {
		return passwordRepeat;
	}

	public void setPasswordRepeat(String passwordRepeat) {
		this.passwordRepeat = passwordRepeat;
	}

	public boolean isSubscribe() {
		return subscribe;
	}

	public void setSubscribe(boolean subscribe) {
		this.subscribe = subscribe;
	}
}
