package ua.kharkov.nure.semerkov.Shop.servlets.basket;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.kharkov.nure.semerkov.Shop.parameter.BasketParameters;
import ua.kharkov.nure.semerkov.Shop.Path;
import ua.kharkov.nure.semerkov.Shop.entity.Basket;
import ua.kharkov.nure.semerkov.Shop.entity.OrderItem;

/**
 * Servlet implementation class BasketServlet
 */
public class BasketServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(BasketServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public BasketServlet() {
		super();
		if (log.isDebugEnabled()) {
			log.debug("Servlet creates");
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		if (log.isDebugEnabled()) {
			log.debug("GET method starts");
		}
		HttpSession session = request.getSession();
		Basket basket = (Basket) session.getAttribute(BasketParameters.BASKET);
		if (log.isTraceEnabled()) {
			log.trace("basket --> " + basket);
		}
		if (basket != null) {
			List<OrderItem> items = basket.getListOfProducts();
			request.setAttribute("items", items);
		}
		RequestDispatcher dispatcher = request
				.getRequestDispatcher(Path.PAGE__BASKET);
		dispatcher.forward(request, response);
		if (log.isDebugEnabled()) {
			log.debug("Response was sent");
		}
	}
}
