package ua.kharkov.nure.semerkov.Shop.service;

import java.util.List;

import org.apache.log4j.Logger;

import ua.kharkov.nure.semerkov.Shop.dao.CategoryDAO;
import ua.kharkov.nure.semerkov.Shop.dao.db.jdbc.JdbcTransactionManager;
import ua.kharkov.nure.semerkov.Shop.dao.db.transaction.TransactionManager;
import ua.kharkov.nure.semerkov.Shop.dao.db.transaction.TransactionOperation;
import ua.kharkov.nure.semerkov.Shop.dao.exception.DAOException;
import ua.kharkov.nure.semerkov.Shop.entity.Category;

/**
 * This class contains methods for actions with categories data.
 *
 * @author Volodymyr_Semrkov
 */
public class CategoryService {
    private CategoryDAO categoryDAO;
    private TransactionManager transactionManager;
    private static final Logger log = Logger.getLogger(CategoryService.class);

    public CategoryService(CategoryDAO categoryDAO) {
        if (log.isDebugEnabled()) {
            log.debug("Category service creates");
        }
        this.categoryDAO = categoryDAO;
        this.transactionManager = new JdbcTransactionManager();
    }

    @SuppressWarnings("unchecked")
    public List<Category> getAllCategories() throws DAOException {
        return (List<Category>) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        return categoryDAO.getAllCategories();
                    }
                });
    }

    @SuppressWarnings("unchecked")
    public boolean contains(final String categoryName) throws DAOException {
        return (boolean) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        return categoryDAO.containsCategory(categoryName);
                    }
                });
    }

    @SuppressWarnings("unchecked")
    public boolean addCategory(final Category category) throws DAOException {
        return (boolean) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        return categoryDAO.addCategory(category);
                    }
                });
    }

    @SuppressWarnings("unchecked")
    public boolean updateCategory(final Category category) throws DAOException {
        return (boolean) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        return categoryDAO.updateCategory(category);
                    }
                });
    }

    @SuppressWarnings("unchecked")
    public Category getCategory(final long id) throws DAOException {
        return (Category) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        return categoryDAO.getCategory(id);
                    }
                });
    }

    @SuppressWarnings("unchecked")
    public long getLastCategoryId() throws DAOException {
        return (long) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        return categoryDAO.getLastCategoryId();
                    }
                });
    }
}
