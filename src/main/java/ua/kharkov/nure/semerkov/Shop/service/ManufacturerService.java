package ua.kharkov.nure.semerkov.Shop.service;

import java.util.List;

import org.apache.log4j.Logger;

import ua.kharkov.nure.semerkov.Shop.dao.ManufacturerDAO;
import ua.kharkov.nure.semerkov.Shop.dao.db.jdbc.JdbcTransactionManager;
import ua.kharkov.nure.semerkov.Shop.dao.db.transaction.TransactionManager;
import ua.kharkov.nure.semerkov.Shop.dao.db.transaction.TransactionOperation;
import ua.kharkov.nure.semerkov.Shop.dao.exception.DAOException;
import ua.kharkov.nure.semerkov.Shop.entity.Manufacturer;

/**
 * This class contains methods for actions with manufacturers data.
 *
 * @author Volodymyr_Semrkov
 */
public class ManufacturerService {
    private ManufacturerDAO manufacturerDAO;
    private TransactionManager transactionManager;
    private static final Logger log = Logger
            .getLogger(ManufacturerService.class);

    public ManufacturerService(ManufacturerDAO manufacturerDAO) {
        if (log.isDebugEnabled()) {
            log.debug("Manufacturer service creates");
        }
        this.manufacturerDAO = manufacturerDAO;
        this.transactionManager = new JdbcTransactionManager();
    }

    @SuppressWarnings("unchecked")
    public List<Manufacturer> getAllManufacturers() throws DAOException {
        return (List<Manufacturer>) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        return manufacturerDAO.getAllManufacturers();
                    }
                });
    }

    @SuppressWarnings("unchecked")
    public boolean contains(final String manufacturerName) throws DAOException {
        return (boolean) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        return manufacturerDAO.containsManufacturer(manufacturerName);
                    }
                });
    }

    @SuppressWarnings("unchecked")
    public boolean addManufacturer(final Manufacturer manufacturer) throws DAOException {
        return (boolean) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        return manufacturerDAO.addManufacturer(manufacturer);
                    }
                });
    }

    @SuppressWarnings("unchecked")
    public boolean updateManufacturer(final Manufacturer manufacturer) throws DAOException {
        return (boolean) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        return manufacturerDAO.updateManufacturer(manufacturer);
                    }
                });
    }

    @SuppressWarnings("unchecked")
    public Manufacturer getManufacturer(final long id) throws DAOException {
        return (Manufacturer) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        return manufacturerDAO.getManufacturer(id);
                    }
                });
    }

    @SuppressWarnings("unchecked")
    public long getLastManufacturerId() throws DAOException {
        return (long) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        return manufacturerDAO.getLastManufacturerId();
                    }
                });
    }
}
