package ua.kharkov.nure.semerkov.Shop.servlets.basket;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import ua.kharkov.nure.semerkov.Shop.parameter.BasketParameters;
import ua.kharkov.nure.semerkov.Shop.entity.Basket;

/**
 * Servlet implementation class BasketParametersServlet
 */
public class BasketParametersServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger
			.getLogger(BasketParametersServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public BasketParametersServlet() {
		super();
		if (log.isDebugEnabled()) {
			log.debug("Servlet creates");
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		if (log.isDebugEnabled()) {
			log.debug("GET method starts");
		}
		HttpSession session = request.getSession();
		Basket basket = (Basket) session.getAttribute(BasketParameters.BASKET);
		int numberOfProducts = 0;
		double totalSum = 0;
		if (basket != null) {
			numberOfProducts = basket.getNumberOfProducts();
			totalSum = basket.getTotalSum();
		}
		if (log.isTraceEnabled()) {
			log.trace("numberOfProducts --> " + numberOfProducts);
			log.trace("totalSum --> " + totalSum);
		}
		JSONObject resultJson = new JSONObject();
		resultJson.put(BasketParameters.NUMBER_OF_PRODUCTS, numberOfProducts);
		resultJson.put(BasketParameters.TOTAL_SUM, totalSum);
		response.setContentType("text/json");
		PrintWriter writer = response.getWriter();
		writer.write(resultJson.toString());
		writer.flush();
		if (log.isDebugEnabled()) {
			log.debug("JSON was sent");
		}
	}
}
