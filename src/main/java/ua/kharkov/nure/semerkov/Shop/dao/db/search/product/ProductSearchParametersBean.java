package ua.kharkov.nure.semerkov.Shop.dao.db.search.product;

import java.util.List;

/**
 * Bean with search parameters.
 * 
 * @author Volodymyr_Semerkov
 * 
 */
public class ProductSearchParametersBean {
	private double minPrice;
	private double maxPrice;
	private List<Long> categories;
	private List<Long> manufacturers;
	private String productName;
	private SortParameter sortParameter;
	private SortOrder sortOrder;
	private int productsOnPage;
	private int pageNumber;
	private boolean isMinPriceSelected;
	private boolean isMaxPriceSelected;

	public ProductSearchParametersBean() {
	}

	public ProductSearchParametersBean(double minPrice, double maxPrice,
                                       List<Long> categories, List<Long> manufacturers,
                                       String productName, SortParameter sortParameter,
                                       SortOrder sortOrder, int productsOnPage) {
		this.minPrice = minPrice;
		this.maxPrice = maxPrice;
		this.categories = categories;
		this.manufacturers = manufacturers;
		this.productName = productName;
		this.sortParameter = sortParameter;
		this.sortOrder = sortOrder;
		this.productsOnPage = productsOnPage;
	}

	public double getMinPrice() {
		return minPrice;
	}

	public void setMinPrice(double minPrice) {
		this.minPrice = minPrice;
	}

	public double getMaxPrice() {
		return maxPrice;
	}

	public void setMaxPrice(double maxPrice) {
		this.maxPrice = maxPrice;
	}

	public List<Long> getCategories() {
		return categories;
	}

	public void setCategories(List<Long> categories) {
		this.categories = categories;
	}

	public List<Long> getManufacturers() {
		return manufacturers;
	}

	public void setManufacturers(List<Long> manufacturers) {
		this.manufacturers = manufacturers;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public SortParameter getSortParameter() {
		return sortParameter;
	}

	public void setSortParameter(SortParameter sortParameter) {
		this.sortParameter = sortParameter;
	}

	public SortOrder getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}

	public int getProductsOnPage() {
		return productsOnPage;
	}

	public void setProductsOnPage(int productsOnPage) {
		this.productsOnPage = productsOnPage;
	}

	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public boolean isMinPriceSelected() {
		return isMinPriceSelected;
	}

	public void setMinPriceSelected(boolean isMinPriceSelected) {
		this.isMinPriceSelected = isMinPriceSelected;
	}

	public boolean isMaxPriceSelected() {
		return isMaxPriceSelected;
	}

	public void setMaxPriceSelected(boolean isMaxPriceSelected) {
		this.isMaxPriceSelected = isMaxPriceSelected;
	}
}
