package ua.kharkov.nure.semerkov.Shop.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ua.kharkov.nure.semerkov.Shop.dao.db.jdbc.JdbcConnectionHolder;
import ua.kharkov.nure.semerkov.Shop.parameter.MapperParameters;
import ua.kharkov.nure.semerkov.Shop.dao.CategoryDAO;
import ua.kharkov.nure.semerkov.Shop.dao.exception.DAOException;
import ua.kharkov.nure.semerkov.Shop.db.DatabaseManager;
import ua.kharkov.nure.semerkov.Shop.entity.Category;

/**
 * Class for access to category data.
 * 
 * @author Volodymyr_Semrkov
 * 
 */
public class MysqlCategoryDAO implements CategoryDAO {
	private static final String SQL__FIND_CATEGORY_BY_NAME = "SELECT * FROM categories where name=?";
    private static final String SQL__FIND_CATEGORY_BY_ID = "SELECT * FROM categories where id=?";
	private static final String SQL__INSERT_CATEGORY = "INSERT INTO categories(name) VALUES(?)";
	private static final String SQL__UPDATE_CATEGORY = "UPDATE categories SET name=? WHERE id=?";
	private static final String SQL__REMOVE_CATEGORY = "DELETE FROM categories WHERE id=?";
	private static final String SQL__SELECT_ALL = "SELECT * FROM categories ORDER BY name ASC";
    private static final String SQL__FIND_LAST_CATEGORY = "SELECT MAX(id) AS max_id FROM categories";

	@Override
	public boolean containsCategory(String name) throws DAOException {
		PreparedStatement pstmt = null;
		try {
			Connection connection = JdbcConnectionHolder.getConnection();
			pstmt = connection.prepareStatement(SQL__FIND_CATEGORY_BY_NAME);
			pstmt.setString(1, name);
			return pstmt.executeQuery().first();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DatabaseManager.closeStatement(pstmt);
		}
	}

	@Override
	public boolean addCategory(Category category) throws DAOException {
		if (containsCategory(category.getName()))
			throw new DAOException(
					"Category with such name already exists in database!");
		PreparedStatement pstmt = null;
		try {
			Connection connection = JdbcConnectionHolder.getConnection();
			pstmt = connection.prepareStatement(SQL__INSERT_CATEGORY);
			pstmt.setString(1, category.getName());
			return pstmt.executeUpdate() == 1;
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DatabaseManager.closeStatement(pstmt);
		}
	}

	@Override
	public Category getCategory(long id) throws DAOException {
		PreparedStatement pstmt = null;
		try {
			Connection connection = JdbcConnectionHolder.getConnection();
			pstmt = connection.prepareStatement(SQL__FIND_CATEGORY_BY_ID);
			pstmt.setLong(1, id);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				String name = rs.getString(MapperParameters.CATEGORY__NAME);
				return new Category(id, name);
			}
			return null;
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DatabaseManager.closeStatement(pstmt);
		}
	}

	@Override
	public boolean removeCategory(long id) throws DAOException {
		PreparedStatement pstmt = null;
		try {
			Connection connection = JdbcConnectionHolder.getConnection();
			pstmt = connection.prepareStatement(SQL__REMOVE_CATEGORY);
			pstmt.setLong(1, id);
			return pstmt.executeUpdate() != 1;
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DatabaseManager.closeStatement(pstmt);
		}
	}

	@Override
	public boolean updateCategory(Category category) throws DAOException {
		PreparedStatement pstmt = null;
		try {
			Connection connection = JdbcConnectionHolder.getConnection();
			pstmt = connection.prepareStatement(SQL__UPDATE_CATEGORY);
			pstmt.setString(1, category.getName());
            pstmt.setLong(2, category.getId());
			return pstmt.executeUpdate() == 1;
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DatabaseManager.closeStatement(pstmt);
		}
	}

	@Override
	public List<Category> getAllCategories() throws DAOException {
		List<Category> categories = new ArrayList<Category>();
		Statement stmt = null;
		try {
			Connection connection = JdbcConnectionHolder.getConnection();
			stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(SQL__SELECT_ALL);
			while (rs.next()) {
				Category category = new Category(rs.getInt(MapperParameters.ID));
				category.setName(rs.getString(MapperParameters.CATEGORY__NAME));
				categories.add(category);
			}
			return categories;
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DatabaseManager.closeStatement(stmt);
		}
	}

    @Override
    public long getLastCategoryId() throws DAOException {
        long orderId = -1;
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection
                    .prepareStatement(SQL__FIND_LAST_CATEGORY);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                orderId = rs.getLong("max_id");
            }
            return orderId;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }
}
