package ua.kharkov.nure.semerkov.Shop.servlets.order;

import org.apache.log4j.Logger;
import ua.kharkov.nure.semerkov.Shop.Path;
import ua.kharkov.nure.semerkov.Shop.dao.exception.DAOException;
import ua.kharkov.nure.semerkov.Shop.entity.Order;
import ua.kharkov.nure.semerkov.Shop.entity.User;
import ua.kharkov.nure.semerkov.Shop.parameter.OrderParameters;
import ua.kharkov.nure.semerkov.Shop.parameter.ServiceParameters;
import ua.kharkov.nure.semerkov.Shop.parameter.UserParameters;
import ua.kharkov.nure.semerkov.Shop.service.OrderService;
import ua.kharkov.nure.semerkov.Shop.service.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Servlet implementation class ClientOrdersServlet
 */
public class ClientOrdersServlet extends HttpServlet {
    private UserService userService;
    private OrderService orderService;
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ClientOrdersServlet.class);

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ClientOrdersServlet() {
        super();
        if (log.isDebugEnabled()) {
            log.debug("Servlet creates");
        }
    }

    @Override
    public void init() throws ServletException {
        ServletContext servletContext = getServletContext();
        userService = (UserService) servletContext
                .getAttribute(ServiceParameters.USER__SERVICE);
        if (userService == null) {
            log.error("User service attribute is not exists.");
            throw new IllegalStateException(
                    "User service attribute is not exists.");
        }
        orderService = (OrderService) servletContext
                .getAttribute(ServiceParameters.ORDER__SERVICE);
        if (orderService == null) {
            log.error("Order service attribute is not exists.");
            throw new IllegalStateException(
                    "Order service attribute is not exists.");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        if (log.isDebugEnabled()) {
            log.debug("GET method starts");
        }
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(UserParameters.USER);
        try {
            List<Order> orders = orderService.getOrders(user.getId());
            request.setAttribute(OrderParameters.ORDERS, orders);
            RequestDispatcher dispatcher = request
                    .getRequestDispatcher(Path.PAGE__CLIENT_ORDERS);
            dispatcher.forward(request, response);
        } catch (DAOException e) {
            log.error(e.getMessage());
            response.sendRedirect(Path.PAGE__ERROR_PAGE);
        }
        if (log.isDebugEnabled()) {
            log.debug("Response was sent");
        }
    }
}
