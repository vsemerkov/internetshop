package ua.kharkov.nure.semerkov.Shop.filter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.servlet.ServletOutputStream;

/**
 * Response output stream.
 * 
 * @author Volodymyr_Semerkov
 * 
 */
public class ResponseOutputStream extends ServletOutputStream {
	private ByteArrayOutputStream byteStream;

	public ResponseOutputStream(ByteArrayOutputStream byteSteam) {
		this.byteStream = byteSteam;
	}

	@Override
	public void write(int b) throws IOException {
		byteStream.write(b);
	}
}
