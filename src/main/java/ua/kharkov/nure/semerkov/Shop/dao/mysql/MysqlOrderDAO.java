package ua.kharkov.nure.semerkov.Shop.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ua.kharkov.nure.semerkov.Shop.CanceledOrderStatus;
import ua.kharkov.nure.semerkov.Shop.OrderStatus;
import ua.kharkov.nure.semerkov.Shop.dao.OrderDAO;
import ua.kharkov.nure.semerkov.Shop.dao.db.jdbc.JdbcConnectionHolder;
import ua.kharkov.nure.semerkov.Shop.dao.db.search.order.OrderSearchParametersBean;
import ua.kharkov.nure.semerkov.Shop.dao.mapper.OrderMapper;
import ua.kharkov.nure.semerkov.Shop.dao.exception.DAOException;
import ua.kharkov.nure.semerkov.Shop.db.DatabaseManager;
import ua.kharkov.nure.semerkov.Shop.entity.Order;

/**
 * Class for access to order data.
 *
 * @author Volodymyr_Semrkov
 */
public class MysqlOrderDAO implements OrderDAO {
    private static final String SQL__FIND_ORDER_BY_ID = "SELECT * FROM orders WHERE id=?";
    private static final String SQL__FIND_ORDER_BY_ID_AND_USER_ID = "SELECT * FROM orders WHERE id=? AND user_id=?";
    private static final String SQL__FIND_ORDER_BY_USER_ID = "SELECT * FROM orders WHERE user_id=?";
    private static final String SQL__FIND_LAST_ORDER_ID_BY_USER_ID = "SELECT MAX(id) AS max_id FROM orders WHERE user_id=?";
    private static final String SQL__FIND_ORDER_BY_USER_ID_AND_DATE = "SELECT * FROM orders WHERE user_id=? AND "
            + "order_date >= ? AND order_date <= ?";
    private static final String SQL__INSERT_ORDER = "INSERT INTO orders(order_status, canceled_order_status, order_date, user_id, delivery_type, payment_type, address) "
            + "VALUES(?, ?, ?, ?, ?, ?, ?)";
    private static final String SQL__REMOVE_ORDER = "DELETE FROM orders WHERE id=?";
    private static final String SQL__UPDATE_ORDER_STATUS = "UPDATE orders SET order_status=?, canceled_order_status=? WHERE id=?";

    @Override
    public boolean containsOrder(long id) throws DAOException {
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__FIND_ORDER_BY_ID);
            pstmt.setLong(1, id);
            return pstmt.executeQuery().first();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public boolean addOrder(Order order) throws DAOException {
        if (containsOrder(order.getId()))
            throw new DAOException(
                    "Order with such id already exists in database!");
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__INSERT_ORDER);
            OrderMapper.mapOrder(order, pstmt);
            return pstmt.executeUpdate() != 1;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public boolean removeOrder(long id) throws DAOException {
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__REMOVE_ORDER);
            pstmt.setLong(1, id);
            return pstmt.executeUpdate() != 1;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public boolean updateOrderStatus(long id, OrderStatus orderStatus, CanceledOrderStatus canceledOrderStatus)
            throws DAOException {
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__UPDATE_ORDER_STATUS);
            pstmt.setString(1, orderStatus.name());
            pstmt.setString(2, canceledOrderStatus != null ? canceledOrderStatus.name() : null);
            pstmt.setLong(3, id);
            return pstmt.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public List<Order> getOrders(long userId) throws DAOException {
        List<Order> orders = new ArrayList<Order>();
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__FIND_ORDER_BY_USER_ID);
            pstmt.setLong(1, userId);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                orders.add(OrderMapper.unMapOrder(rs));
            }
            return orders;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public Order getOrder(long orderId) throws DAOException {
        Order order = null;
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__FIND_ORDER_BY_ID);
            pstmt.setLong(1, orderId);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                order = OrderMapper.unMapOrder(rs);
            }
            return order;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public List<Order> getOrders(long userId, Date from, Date to)
            throws DAOException {
        List<Order> orders = new ArrayList<Order>();
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection
                    .prepareStatement(SQL__FIND_ORDER_BY_USER_ID_AND_DATE);
            pstmt.setLong(1, userId);
            pstmt.setTimestamp(2, new Timestamp(from.getTime()));
            pstmt.setTimestamp(3, new Timestamp(to.getTime()));
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                orders.add(OrderMapper.unMapOrder(rs));
            }
            return orders;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public long getLastOrderId(long userId) throws DAOException {
        long orderId = -1;
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection
                    .prepareStatement(SQL__FIND_LAST_ORDER_ID_BY_USER_ID);
            pstmt.setLong(1, userId);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                orderId = rs.getLong("max_id");
            }
            return orderId;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public boolean belong(long orderId, long userId) throws DAOException {
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__FIND_ORDER_BY_ID_AND_USER_ID);
            pstmt.setLong(1, orderId);
            pstmt.setLong(2, userId);
            return pstmt.executeQuery().first();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public int getNumberOfRequiredOrders(
            OrderSearchParametersBean searchParametersBean) throws DAOException {
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            String query = prepareCountQuery(searchParametersBean);
            pstmt = connection.prepareStatement(query);
            if (searchParametersBean.getOrderId() == 0) {
                int parameterNumber = 1;
                if (searchParametersBean.getOrderStatus() != null) {
                    pstmt.setString(parameterNumber, searchParametersBean.getOrderStatus().name());
                    parameterNumber++;
                }
                if (searchParametersBean.isStartDateSelected()) {
                    pstmt.setTimestamp(parameterNumber, new Timestamp(searchParametersBean.getStartDate().getTime()));
                    parameterNumber++;
                }
                if (searchParametersBean.isEndDateSelected()) {
                    pstmt.setTimestamp(parameterNumber, new Timestamp(searchParametersBean.getEndDate().getTime()));
                    parameterNumber++;
                }
            }
            ResultSet rs = pstmt.executeQuery();
            int numberOfProducts = 0;
            if (rs.next()) {
                numberOfProducts = rs.getInt("number");
            }
            return numberOfProducts;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public List<Order> findOrders(OrderSearchParametersBean searchParametersBean)
            throws DAOException {
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            String query = prepareSearchingQuery(searchParametersBean);
            pstmt = connection.prepareStatement(query);
            if (searchParametersBean.getOrderId() == 0) {
                int parameterNumber = 1;
                if (searchParametersBean.getOrderStatus() != null) {
                    pstmt.setString(parameterNumber, searchParametersBean.getOrderStatus().name());
                    parameterNumber++;
                }
                if (searchParametersBean.isStartDateSelected()) {
                    pstmt.setTimestamp(parameterNumber, new Timestamp(searchParametersBean.getStartDate().getTime()));
                    parameterNumber++;
                }
                if (searchParametersBean.isEndDateSelected()) {
                    pstmt.setTimestamp(parameterNumber, new Timestamp(searchParametersBean.getEndDate().getTime()));
                    parameterNumber++;
                }
            }
            ResultSet rs = pstmt.executeQuery();
            List<Order> orders = new ArrayList<Order>();
            while (rs.next()) {
                Order order = OrderMapper.unMapOrder(rs);
                orders.add(order);
            }
            return orders;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    private String prepareCountQuery(OrderSearchParametersBean searchParameterBean) {
        StringBuilder query = new StringBuilder(
                "SELECT COUNT(id) AS number FROM orders");
        String mainQueryPart = prepareMainQueryPart(searchParameterBean);
        if (!mainQueryPart.trim().isEmpty()) {
            query.append(" WHERE ");
            query.append(mainQueryPart);
        }
        return query.toString();
    }

    private String prepareSearchingQuery(
            OrderSearchParametersBean searchParametersBean) {
        StringBuilder query = new StringBuilder(
                "SELECT * FROM orders");
        String mainQueryPart = prepareMainQueryPart(searchParametersBean);
        if (!mainQueryPart.trim().isEmpty()) {
            query.append(" WHERE ");
            query.append(mainQueryPart);
        }
        int productsOnPage = searchParametersBean.getOrdersOnPage();
        int pageNumber = searchParametersBean.getPageNumber();
        query.append(" LIMIT ");
        query.append((pageNumber - 1) * productsOnPage);
        query.append(", ");
        query.append(productsOnPage);
        return query.toString();
    }

    private String prepareMainQueryPart(
            OrderSearchParametersBean searchParametersBean) {
        StringBuilder query = new StringBuilder();
        List<String> queryParts = new ArrayList<String>();
        long orderId = searchParametersBean.getOrderId();
        if (orderId != 0) {
            queryParts.add(String.format("(id=%d)",
                    orderId));
        } else {
            if (searchParametersBean.getOrderStatus() != null) {
                queryParts.add("(order_status=?)");
            }
            if (searchParametersBean.isStartDateSelected())
                queryParts.add("(order_date>?)");
            if (searchParametersBean.isEndDateSelected())
                queryParts.add("(order_date<?)");
        }
        if (queryParts.size() != 0) {
            query.append(queryParts.get(0));
            for (int i = 1; i < queryParts.size(); i++) {
                query.append(" AND ");
                query.append(queryParts.get(i));
            }
        }
        return query.toString();
    }
}
