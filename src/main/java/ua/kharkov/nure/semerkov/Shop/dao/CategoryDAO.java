package ua.kharkov.nure.semerkov.Shop.dao;

import java.util.List;

import ua.kharkov.nure.semerkov.Shop.dao.exception.DAOException;
import ua.kharkov.nure.semerkov.Shop.entity.Category;

/**
 * Interface for access to category data.
 * 
 * @author Volodymyr_Semrkov
 * 
 */
public interface CategoryDAO {
	public boolean containsCategory(String name) throws DAOException;

	public boolean addCategory(Category category) throws DAOException;

	public Category getCategory(long id) throws DAOException;

	public boolean removeCategory(long id) throws DAOException;

	public boolean updateCategory(Category category) throws DAOException;

	public List<Category> getAllCategories() throws DAOException;

    public long getLastCategoryId() throws DAOException;
}
