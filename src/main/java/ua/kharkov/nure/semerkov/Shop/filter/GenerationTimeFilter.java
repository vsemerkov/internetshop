package ua.kharkov.nure.semerkov.Shop.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

/**
 * Generation time filter.
 * 
 * @author Volodymyr_Semerkov
 * 
 */
public class GenerationTimeFilter implements Filter {
	private static final Logger log = Logger
			.getLogger(GenerationTimeFilter.class);

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		log.debug("Filter initialization starts");
		log.debug("Filter initialization finished");
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		ResponseWrapper responseWrapper = new ResponseWrapper(httpResponse);
		long startTime = System.currentTimeMillis();
		chain.doFilter(httpRequest, responseWrapper);
		long generationTime = System.currentTimeMillis() - startTime;
		if (responseWrapper.isWriterUsed()) {
			String writerText = responseWrapper.toString();
			String timeString = "<p>Generation time: " + generationTime
					+ " ms.</p>";
			if (generationTime > 2000) {
				writerText = writerText
						.replace("<div id=\"footer\" class=\"grey_footer\">",
								"<div id=\"footer\" class=\"red_footer\">"
										+ timeString);
			} else {
				writerText = writerText.replace(
						"<div id=\"footer\" class=\"grey_footer\">",
						"<div id=\"footer\" class=\"grey_footer\">"
								+ timeString);
			}
			httpResponse.getWriter().write(writerText);
			httpResponse.getWriter().flush();
			httpResponse.getWriter().close();
		} else {
			response.getOutputStream().write(responseWrapper.getBytes());
			response.getOutputStream().flush();
			response.getOutputStream().close();
		}
	}

	@Override
	public void destroy() {
	}
}
