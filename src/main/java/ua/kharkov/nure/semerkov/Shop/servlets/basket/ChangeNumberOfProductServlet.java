package ua.kharkov.nure.semerkov.Shop.servlets.basket;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import ua.kharkov.nure.semerkov.Shop.parameter.BasketParameters;
import ua.kharkov.nure.semerkov.Shop.entity.Basket;

/**
 * Servlet implementation class ChangeNumberOfProductServlet
 */
public class ChangeNumberOfProductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger
			.getLogger(ChangeNumberOfProductServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ChangeNumberOfProductServlet() {
		super();
		if (log.isDebugEnabled()) {
			log.debug("Servlet creates");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		if (log.isDebugEnabled()) {
			log.debug("POST method starts");
		}
		boolean result = true;
		double sum = 0;
		int numberOfProducts = 0;
		double totalSum = 0;
		long id = -1;
		int number = 0;
		if (log.isTraceEnabled()) {
			log.trace("product id --> "
					+ request.getParameter(BasketParameters.ID));
			log.trace("product number --> "
					+ request.getParameter(BasketParameters.NUMBER));
		}
		if (request.getParameter(BasketParameters.ID) != null
				&& request.getParameter(BasketParameters.NUMBER) != null) {
			try {
				id = Long.parseLong(request.getParameter(BasketParameters.ID));
				number = Integer.parseInt(request
						.getParameter(BasketParameters.NUMBER));
			} catch (NumberFormatException e) {
				result = false;
				log.error(e.getMessage());
			}
		} else {
			result = false;
		}
		if (result) {
			HttpSession session = request.getSession();
			Basket basket = (Basket) session
					.getAttribute(BasketParameters.BASKET);
			if (log.isTraceEnabled()) {
				log.trace("basket --> " + basket);
			}
			if (basket != null) {
				basket.changeNumberOfProduct(id, number);
				double price = basket.getPriceOfProduct(id);
				sum = price * number;
				numberOfProducts = basket.getNumberOfProducts();
				totalSum = basket.getTotalSum();
				session.setAttribute(BasketParameters.BASKET, basket);
				if (log.isTraceEnabled()) {
					log.trace("basket --> " + basket);
				}
			} else {
				result = false;
			}
		}
		if (log.isTraceEnabled()) {
			log.trace("result --> " + result);
			log.trace("sum --> " + sum);
			log.trace("numberOfProducts --> " + numberOfProducts);
			log.trace("totalSum --> " + totalSum);
		}
		JSONObject resultJson = new JSONObject();
		resultJson.put("result", result);
		resultJson.put("sum", sum);
		resultJson.put(BasketParameters.NUMBER_OF_PRODUCTS, numberOfProducts);
		resultJson.put(BasketParameters.TOTAL_SUM, totalSum);
		response.setContentType("text/json");
		PrintWriter writer = response.getWriter();
		writer.write(resultJson.toString());
		writer.flush();
		if (log.isDebugEnabled()) {
			log.debug("JSON was sent");
		}
	}
}
