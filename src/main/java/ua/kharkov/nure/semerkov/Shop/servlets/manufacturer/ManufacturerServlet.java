package ua.kharkov.nure.semerkov.Shop.servlets.manufacturer;

import org.apache.log4j.Logger;
import ua.kharkov.nure.semerkov.Shop.Path;
import ua.kharkov.nure.semerkov.Shop.dao.exception.DAOException;
import ua.kharkov.nure.semerkov.Shop.entity.Manufacturer;
import ua.kharkov.nure.semerkov.Shop.parameter.ErrorParameters;
import ua.kharkov.nure.semerkov.Shop.parameter.OrderParameters;
import ua.kharkov.nure.semerkov.Shop.parameter.ServiceParameters;
import ua.kharkov.nure.semerkov.Shop.service.ManufacturerService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Servlet implementation class ManufacturerServlet
 */
public class ManufacturerServlet extends HttpServlet {
    private ManufacturerService manufacturerService;
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ManufacturerServlet.class);

    /**
     * @see javax.servlet.http.HttpServlet#HttpServlet()
     */
    public ManufacturerServlet() {
        super();
        if (log.isDebugEnabled()) {
            log.debug("Servlet creates");
        }
    }

    @Override
    public void init() throws ServletException {
        ServletContext servletContext = getServletContext();
        manufacturerService = (ManufacturerService) servletContext
                .getAttribute(ServiceParameters.MANUFACTURER__SERVICE);
        if (manufacturerService == null) {
            log.error("Manufacturer service attribute is not exists.");
            throw new IllegalStateException(
                    "Manufacturer service attribute is not exists.");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        if (log.isDebugEnabled()) {
            log.debug("GET method starts");
        }
        HttpSession session = request.getSession();
        request.setAttribute(ErrorParameters.ERROR, session.getAttribute(ErrorParameters.ERROR));
        session.removeAttribute(ErrorParameters.ERROR);
        try {
            long manufacturerId = 0;
            if (request.getParameter(OrderParameters.MANUFACTURER_ID) != null) {
                manufacturerId = Long.parseLong(request
                        .getParameter(OrderParameters.MANUFACTURER_ID));
            }
            if (manufacturerId != 0) {
                Manufacturer manufacturer = manufacturerService.getManufacturer(manufacturerId);
                request.setAttribute(OrderParameters.MANUFACTURER_ID, manufacturerId);
                request.setAttribute(OrderParameters.MANUFACTURER_NAME, manufacturer.getName());
            }
            RequestDispatcher dispatcher = request
                    .getRequestDispatcher(Path.PAGE__MANUFACTURER);
            dispatcher.forward(request, response);
        } catch (DAOException e) {
            log.error(e.getMessage());
            response.sendRedirect(Path.PAGE__ERROR_PAGE);
        }
        if (log.isDebugEnabled()) {
            log.debug("Response was sent");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {
        if (log.isDebugEnabled()) {
            log.debug("POST method starts");
        }
        HttpSession session = request.getSession();
        try {
            long manufacturerId = 0;
            if (request.getParameter(OrderParameters.MANUFACTURER_ID) != null) {
                manufacturerId = Long.parseLong(request
                        .getParameter(OrderParameters.MANUFACTURER_ID));
            }
            String manufacturerName = request.getParameter(OrderParameters.MANUFACTURER_NAME);
            if (manufacturerName != null && manufacturerName.trim().length() != 0) {
                manufacturerName = manufacturerName.trim();
                if (!manufacturerService.contains(manufacturerName)) {
                    if (manufacturerId == 0) {
                        manufacturerService.addManufacturer(new Manufacturer(manufacturerName));
                        manufacturerId = manufacturerService.getLastManufacturerId();
                    } else {
                        manufacturerService.updateManufacturer(new Manufacturer(manufacturerId, manufacturerName));
                    }
                } else {
                    session.setAttribute(ErrorParameters.ERROR, ErrorParameters.MANUFACTURER_EXISTING_ERROR);
                }
            } else {
                session.setAttribute(ErrorParameters.ERROR, ErrorParameters.MANUFACTURER_INCORRECT_ERROR);
            }
            response.sendRedirect(Path.COMMAND__MANUFACTURER + "?" + OrderParameters.MANUFACTURER_ID + "=" + manufacturerId);
        } catch (DAOException e) {
            log.error(e.getMessage());
            response.sendRedirect(Path.PAGE__ERROR_PAGE);
        }
        if (log.isDebugEnabled()) {
            log.debug("Response was sent");
        }
    }
}
