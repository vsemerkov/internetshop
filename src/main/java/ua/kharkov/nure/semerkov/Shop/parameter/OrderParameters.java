package ua.kharkov.nure.semerkov.Shop.parameter;

/**
 * Holder for order parameters names.
 * 
 * @author Volodymyr_Semerkov
 * 
 */
public final class OrderParameters {
	public static final String ORDER = "order";
	public static final String ORDER__ADDRESS = "address";
	public static final String ORDER__DELIVERY_TYPE = "deliveryType";
	public static final String ORDER__PAYMENT_TYPE = "paymentType";
	public static final String ORDER__CARD_NUMBER = "cardNumber";
    public static final String ORDER__CVV = "cvv";
	public static final String ORDER__DELIVERY_TYPES = "deliveryTypes";
	public static final String ORDER__PAYMENT_TYPES = "paymentTypes";
    public static final String ORDERS = "orders";
    public static final String CATEGORIES = "categories";
    public static final String MANUFACTURERS = "manufacturers";
    public static final String CATEGORY = "category";
    public static final String MANUFACTURER = "manufacturer";
    public static final String ORDER_ID = "orderId";
    public static final String ORDER__STATUS = "status";
    public static final String ORDER__DATE = "date";
    public static final String ORDER__USER_ID = "user_id";
    public static final String ORDER__ITEMS = "orderItems";
    public static final String ORDER__OWNER = "orderOwner";
    public static final String ORDER__OWNER_EMAIL = "orderOwnerEmail";
    public static final String CATEGORY_ID = "categoryId";
    public static final String CATEGORY_NAME = "categoryName";
    public static final String MANUFACTURER_ID = "manufacturerId";
    public static final String MANUFACTURER_NAME = "manufacturerName";
    public static final String PRODUCT = "product";
    public static final String PRODUCT_ID = "productId";
}
