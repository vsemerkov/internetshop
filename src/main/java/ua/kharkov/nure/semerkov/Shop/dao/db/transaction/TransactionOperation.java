package ua.kharkov.nure.semerkov.Shop.dao.db.transaction;

import ua.kharkov.nure.semerkov.Shop.dao.exception.DAOException;

/**
 * Main interface for the TransactionOperation pattern implementation.
 * 
 * @author Volodymyr_Semerkov
 * 
 */
public interface TransactionOperation {
	public Object execute() throws DAOException;
}
