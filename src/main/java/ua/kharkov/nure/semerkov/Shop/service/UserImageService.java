package ua.kharkov.nure.semerkov.Shop.service;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;

import org.apache.log4j.Logger;

import ua.kharkov.nure.semerkov.Shop.entity.Image;

/**
 * This class contains methods for actions with users images.
 * 
 * @author Volodymyr_Semrkov
 * 
 */
public class UserImageService {
	private String imageFolderPath;
	private String defaultImagePath;
	private int maxHeight;
	private int maxWidth;
	private static final Logger log = Logger.getLogger(UserImageService.class);

	public UserImageService(String imageFolderPath, String defaultImagePath,
			int maxHeight, int maxWidth) {
		this.imageFolderPath = imageFolderPath;
		File imageFolder = new File(imageFolderPath);
		if (!(imageFolder.exists() && imageFolder.isDirectory())) {
			imageFolder.mkdir();
		}
		this.defaultImagePath = defaultImagePath;
		this.maxHeight = maxHeight;
		this.maxWidth = maxWidth;
	}

	public Image create(InputStream inputStream, String email)
			throws IllegalStateException, IOException, ServletException {
		int pos = email.indexOf("@");
		StringBuilder imageName = new StringBuilder();
		imageName.append(email.substring(pos + 1));
		imageName.append(email.substring(0, pos));
		imageName.append(".png");
		BufferedImage bufferedImage = ImageIO.read(inputStream);
		if (bufferedImage == null) {
			throw new IllegalStateException();
		}
		BufferedImage newBufferedImage = resize(bufferedImage);
		return new Image(imageName.toString(), newBufferedImage);
	}

	public boolean save(Image image) throws IOException {
		if (image == null)
			throw new IllegalStateException();
		File imageFile = new File(imageFolderPath + "//" + image.getImageName());
		ImageIO.write(image.getBufferedImage(), "png", imageFile);
		log.debug("Image was saved");
		return true;
	}

	public Image get(String imageName) throws IOException {
		File imageFile = null;
		boolean correctImageName = true;
		if (imageName != null) {
			imageFile = new File(imageFolderPath + "//" + imageName);
		} else {
			correctImageName = false;
		}
		if (!correctImageName || !imageFile.exists()) {
			imageFile = new File(defaultImagePath);
		}
		BufferedImage bufferedImage = ImageIO.read(imageFile);
		return new Image(imageName, bufferedImage);
	}

	private BufferedImage resize(BufferedImage oldBufferedImage) {
		int imageType;
		if (oldBufferedImage.getType() == 0) {
			imageType = BufferedImage.TYPE_INT_ARGB_PRE;
		} else {
			imageType = oldBufferedImage.getType();
		}
		BufferedImage newBufferedImage = new BufferedImage(maxWidth, maxHeight,
				imageType);
		Graphics gr = newBufferedImage.createGraphics();
		gr.drawImage(oldBufferedImage, 0, 0, maxWidth, maxHeight, null);
		return newBufferedImage;
	}
}
