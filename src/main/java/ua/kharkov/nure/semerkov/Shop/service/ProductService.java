package ua.kharkov.nure.semerkov.Shop.service;

import java.util.List;

import org.apache.log4j.Logger;

import ua.kharkov.nure.semerkov.Shop.dao.ProductDAO;
import ua.kharkov.nure.semerkov.Shop.dao.db.jdbc.JdbcTransactionManager;
import ua.kharkov.nure.semerkov.Shop.dao.db.search.product.ProductSearchParametersBean;
import ua.kharkov.nure.semerkov.Shop.dao.db.transaction.TransactionManager;
import ua.kharkov.nure.semerkov.Shop.dao.db.transaction.TransactionOperation;
import ua.kharkov.nure.semerkov.Shop.dao.exception.DAOException;
import ua.kharkov.nure.semerkov.Shop.entity.Product;

/**
 * This class contains methods for actions with products data.
 *
 * @author Volodymyr_Semrkov
 */
public class ProductService {
    private ProductDAO productDAO;
    private TransactionManager transactionManager;
    private static final Logger log = Logger.getLogger(ProductService.class);

    public ProductService(ProductDAO productDAO) {
        if (log.isDebugEnabled()) {
            log.debug("Manufacturer service creates");
        }
        this.productDAO = productDAO;
        this.transactionManager = new JdbcTransactionManager();
    }

    /**
     * Find product data in database.
     *
     * @param id Product id.
     * @return True if product data contains in database, else False.
     * @throws DAOException
     */
    public boolean contains(final long id) throws DAOException {
        return (boolean) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        return productDAO.containsProduct(id);
                    }
                });
    }

    /**
     * Add product data in database.
     *
     * @param product Product data.
     * @return True if product data not contains in database, else False.
     * @throws DAOException
     */
    public boolean add(final Product product) throws DAOException {
        return (boolean) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        return productDAO.addProduct(product);
                    }
                });
    }

    /**
     * Get product by id.
     *
     * @param id Product id.
     * @return Product object if product contains in database, else {@code null}
     * .
     * @throws DAOException
     */
    public Product get(final long id) throws DAOException {
        return (Product) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        return productDAO.getProduct(id);
                    }
                });
    }

    /**
     * Remove product from database.
     *
     * @param id Product id.
     * @return True if product data was removed from database, else False.
     * @throws DAOException
     */
    public boolean remove(final long id) throws DAOException {
        return (boolean) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        return productDAO.removeProduct(id);
                    }
                });
    }

    /**
     * Update product data.
     *
     * @param product Product data.
     * @return True if product data was updated in database, else False.
     * @throws DAOException
     */
    public boolean update(final Product product) throws DAOException {
        return (boolean) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        return productDAO.updateProduct(product);
                    }
                });
    }

    /**
     * Get maximal product price in database.
     *
     * @return Maximal product price.
     * @throws DAOException
     */
    public double getMaxPrice() throws DAOException {
        return (double) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        return productDAO.getMaxPrice();
                    }
                });
    }

    /**
     * Get product image path by id.
     *
     * @param id Product id.
     * @return Product image path.
     * @throws DAOException
     */
    public String getImagePath(final long id) throws DAOException {
        return (String) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        return productDAO.getImagePath(id);
                    }
                });
    }

    /**
     * Get number of required products by search parameter bean.
     *
     * @param searchParametersBean Search parameter bean.
     * @return Number of required products.
     * @throws DAOException
     */
    public int getNumberOfRequiredProducts(
            final ProductSearchParametersBean searchParametersBean)
            throws DAOException {
        return (int) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        return productDAO
                                .getNumberOfRequiredProducts(searchParametersBean);
                    }
                });
    }

    /**
     * Find products by search parameter bean.
     *
     * @param searchParametersBean Search parameter bean.
     * @return List of products.
     * @throws DAOException
     */
    @SuppressWarnings("unchecked")
    public List<Product> findProducts(
            final ProductSearchParametersBean searchParametersBean)
            throws DAOException {
        return (List<Product>) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        return productDAO.findProducts(searchParametersBean);
                    }
                });
    }

    /**
     * Get all products.
     *
     * @return List of products.
     * @throws DAOException
     */
    @SuppressWarnings("unchecked")
    public List<Product> getAllProducts()
            throws DAOException {
        return (List<Product>) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        return productDAO.getAllProducts();
                    }
                });
    }
}
