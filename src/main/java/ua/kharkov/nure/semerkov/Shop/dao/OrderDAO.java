package ua.kharkov.nure.semerkov.Shop.dao;

import java.util.Date;
import java.util.List;

import ua.kharkov.nure.semerkov.Shop.CanceledOrderStatus;
import ua.kharkov.nure.semerkov.Shop.OrderStatus;
import ua.kharkov.nure.semerkov.Shop.dao.db.search.order.OrderSearchParametersBean;
import ua.kharkov.nure.semerkov.Shop.dao.exception.DAOException;
import ua.kharkov.nure.semerkov.Shop.entity.Order;

/**
 * Interface for access to order data.
 *
 * @author Volodymyr_Semrkov
 */
public interface OrderDAO {
    public boolean containsOrder(long id) throws DAOException;

    public boolean addOrder(Order order) throws DAOException;

    public boolean removeOrder(long id) throws DAOException;

    public boolean updateOrderStatus(long id, OrderStatus orderStatus, CanceledOrderStatus canceledOrderStatus)
            throws DAOException;

    public List<Order> getOrders(long userId) throws DAOException;

    public Order getOrder(long orderId) throws DAOException;

    public List<Order> getOrders(long userId, Date from, Date to)
            throws DAOException;

    public long getLastOrderId(long userId) throws DAOException;

    public boolean belong(long orderId, long userId) throws DAOException;

    public int getNumberOfRequiredOrders(
            OrderSearchParametersBean searchParameterBean) throws DAOException;

    public List<Order> findOrders(OrderSearchParametersBean searchParameterBean)
            throws DAOException;
}
