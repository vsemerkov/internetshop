package ua.kharkov.nure.semerkov.Shop.dao;

import ua.kharkov.nure.semerkov.Shop.dao.exception.DAOException;
import ua.kharkov.nure.semerkov.Shop.entity.User;

/**
 * Interface for access to user data.
 * 
 * @author Volodymyr_Semrkov
 * 
 */
public interface UserDAO {
	public boolean containsUser(String email) throws DAOException;

	public boolean addUser(User user) throws DAOException;

	public User getUser(String email) throws DAOException;

    public User getUser(long userId) throws DAOException;

	public boolean removeUser(String email) throws DAOException;

	public boolean updateUser(User user) throws DAOException;
}
