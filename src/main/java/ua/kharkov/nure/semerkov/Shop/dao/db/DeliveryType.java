package ua.kharkov.nure.semerkov.Shop.dao.db;

/**
 * The <code>Delivery</code> class contains the enumeration of delivery types.
 * 
 * @author Volodymyr_Semerkov
 * 
 */
public enum DeliveryType {
	COURIER, SELF_DELIVERY
}
