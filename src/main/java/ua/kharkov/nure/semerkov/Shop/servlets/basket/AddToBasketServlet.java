package ua.kharkov.nure.semerkov.Shop.servlets.basket;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import ua.kharkov.nure.semerkov.Shop.parameter.BasketParameters;
import ua.kharkov.nure.semerkov.Shop.parameter.ServiceParameters;
import ua.kharkov.nure.semerkov.Shop.dao.exception.DAOException;
import ua.kharkov.nure.semerkov.Shop.entity.Basket;
import ua.kharkov.nure.semerkov.Shop.entity.Product;
import ua.kharkov.nure.semerkov.Shop.service.ProductService;

/**
 * Servlet implementation class AddToBasketServlet
 */
public class AddToBasketServlet extends HttpServlet {
	private ProductService productService;
	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger
			.getLogger(AddToBasketServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddToBasketServlet() {
		super();
		if (log.isDebugEnabled()) {
			log.debug("Servlet creates");
		}
	}

	@Override
	public void init() throws ServletException {
		ServletContext servletContext = getServletContext();
		productService = (ProductService) servletContext
				.getAttribute(ServiceParameters.PRODUCT__SERVICE);
		if (productService == null) {
			log.error("Product service attribute is not exists.");
			throw new IllegalStateException(
					"Product service attribute is not exists.");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		if (log.isDebugEnabled()) {
			log.debug("POST method starts");
		}
		boolean result = true;
		int numberOfProducts = 0;
		double totalSum = 0;
		long id = -1;
		if (log.isTraceEnabled()) {
			log.trace("product id --> "
					+ request.getParameter(BasketParameters.ID));
		}
		if (request.getParameter(BasketParameters.ID) != null) {
			try {
				id = Long.parseLong(request.getParameter(BasketParameters.ID));
			} catch (NumberFormatException e) {
				result = false;
				log.error(e.getMessage());
			}
		} else {
			result = false;
		}
		try {
			if (result) {
				HttpSession session = request.getSession();
				Product product = productService.get(id);
				if (product != null) {
					Basket basket = (Basket) session
							.getAttribute(BasketParameters.BASKET);
					if (log.isTraceEnabled()) {
						log.trace("basket --> " + basket);
					}
					if (basket == null) {
						basket = new Basket();
					}
					basket.addProduct(product);
					numberOfProducts = basket.getNumberOfProducts();
					totalSum = basket.getTotalSum();
					session.setAttribute(BasketParameters.BASKET, basket);
					if (log.isTraceEnabled()) {
						log.trace("basket --> " + basket);
					}
				} else {
					result = false;
				}
			}
		} catch (DAOException e) {
			log.error(e.getMessage());
		} finally {
			if (log.isTraceEnabled()) {
				log.trace("result --> " + result);
				log.trace("numberOfProducts --> " + numberOfProducts);
				log.trace("totalSum --> " + totalSum);
			}
			JSONObject resultJson = new JSONObject();
			resultJson.put("result", result);
			resultJson
					.put(BasketParameters.NUMBER_OF_PRODUCTS, numberOfProducts);
			resultJson.put(BasketParameters.TOTAL_SUM, totalSum);
			response.setContentType("text/json");
			PrintWriter writer = response.getWriter();
			writer.write(resultJson.toString());
			writer.flush();
			if (log.isDebugEnabled()) {
				log.debug("JSON was sent");
			}
		}
	}
}
