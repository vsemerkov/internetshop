package ua.kharkov.nure.semerkov.Shop;

/**
 * Path holder.
 *
 * @author Volodymyr_Semerkov
 */
public final class Path {
    // pages
    public static final String PAGE__INDEX = "/WEB-INF/pages/common/index.jsp";
    public static final String PAGE__BASKET = "/WEB-INF/pages/common/basket.jsp";
    public static final String PAGE__MAKE_ORDER = "/WEB-INF/pages/internal/make_order.jsp";
    public static final String PAGE__CONFIRM_ORDER = "/WEB-INF/pages/internal/confirm_order.jsp";
    public static final String PAGE__ADD_ORDER_INFO = "/WEB-INF/pages/internal/add_order_info.jsp";
    public static final String PAGE__SIGN_UP = "/WEB-INF/pages/external/signup.jsp";
    public static final String PAGE__SIGN_IN = "/WEB-INF/pages/external/signin.jsp";
    public static final String PAGE__LOGOUT = "logout.jsp";
    public static final String PAGE__ERROR_PAGE = "error_page.jsp";
    public static final String PAGE__ACCESS_ERROR = "/WEB-INF/pages/internal/access_error_page.jsp";
    public static final String PAGE__CLIENT_ORDERS = "/WEB-INF/pages/internal/client_orders.jsp";
    public static final String PAGE__ADMIN_ORDERS = "/WEB-INF/pages/internal/admin_orders.jsp";
    public static final String PAGE__ORDER = "/WEB-INF/pages/internal/order.jsp";
    public static final String PAGE__ADMIN_CATEGORIES = "/WEB-INF/pages/internal/admin_categories.jsp";
    public static final String PAGE__ADMIN_MANUFACTURERS = "/WEB-INF/pages/internal/admin_manufacturers.jsp";
    public static final String PAGE__CATEGORY = "/WEB-INF/pages/internal/category.jsp";
    public static final String PAGE__MANUFACTURER = "/WEB-INF/pages/internal/manufacturer.jsp";
    public static final String PAGE__ADMIN_PRODUCTS = "/WEB-INF/pages/internal/admin_products.jsp";
    public static final String PAGE__PRODUCT = "/WEB-INF/pages/internal/product.jsp";
    public static final String PAGE__CLIENT_PRODUCT = "/WEB-INF/pages/internal/clientProduct.jsp";

    // commands
    public static final String COMMAND__SIGN_UP = "registration";
    public static final String COMMAND__INDEX = "index";
    public static final String COMMAND__BASKET = "basket";
    public static final String COMMAND__MAKE_ORDER = "makeOrder";
    public static final String COMMAND__ADD_ORDER_INFO = "addOrderInfo";
    public static final String COMMAND__CONFIRM_ORDER = "confirmOrder";
    public static final String COMMAND__ORDER = "order";
    public static final String COMMAND__CATEGORY = "category";
    public static final String COMMAND__MANUFACTURER = "manufacturer";
    public static final String COMMAND_PRODUCT = "product";
}
