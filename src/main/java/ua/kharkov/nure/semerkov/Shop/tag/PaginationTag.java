package ua.kharkov.nure.semerkov.Shop.tag;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import ua.kharkov.nure.semerkov.Shop.parameter.ServiceParameters;

/**
 * Pagination tag.
 * 
 * @author Volodymyr_Semerkov
 * 
 */
public class PaginationTag extends SimpleTagSupport {
	private int value;

	public void setValue(int value) {
		this.value = value;
	}

	@Override
	public void doTag() throws IOException {
		PageContext pageContext = (PageContext) getJspContext();
		HttpServletRequest request = (HttpServletRequest) pageContext
				.getRequest();
		JspWriter writer = pageContext.getOut();
		String queryString = request.getQueryString();
		QueryStringEditor.addToQueryString(queryString, writer,
				ServiceParameters.PAGE_PARAMETER, String.valueOf(value));
	}
}
