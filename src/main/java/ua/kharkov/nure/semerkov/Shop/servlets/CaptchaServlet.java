package ua.kharkov.nure.semerkov.Shop.servlets;

import java.io.IOException;

import javax.imageio.ImageIO;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.kharkov.nure.semerkov.Shop.Path;
import ua.kharkov.nure.semerkov.Shop.parameter.ServiceParameters;
import ua.kharkov.nure.semerkov.Shop.security.captcha.CaptchaGenerator;
import ua.kharkov.nure.semerkov.Shop.security.captcha.entity.GeneratedCaptcha;
import ua.kharkov.nure.semerkov.Shop.security.captcha.manager.CaptchaManager;

/**
 * Servlet implementation class CaptchaServlet
 */
public class CaptchaServlet extends HttpServlet {
	private CaptchaManager captchaManager;
	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(CaptchaServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CaptchaServlet() {
		super();
	}

	@Override
	public void init() throws ServletException {
		ServletContext servletContext = getServletContext();
		captchaManager = (CaptchaManager) servletContext
				.getAttribute(ServiceParameters.CAPTCHA__MANAGER);
		if (captchaManager == null) {
			log.error("Captcha manager attribute is not exists.");
			throw new IllegalStateException(
					"Captcha manager attribute is not exists.");
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		if (log.isDebugEnabled()) {
			log.debug("GET request");
		}
		response.setContentType("image/png");
		GeneratedCaptcha generatedCaptcha = CaptchaGenerator.generateCaptcha();
		try {
			captchaManager.setCaptchaCode(request, response,
					generatedCaptcha.getCode());
			ImageIO.write(generatedCaptcha.getImage(), "png",
					response.getOutputStream());
			if (log.isDebugEnabled()) {
				log.debug("Response was sent");
			}
		} catch (IllegalStateException e) {
			log.error("IllegalStateException: " + e.getMessage());
			RequestDispatcher dispatcher = request
					.getRequestDispatcher(Path.COMMAND__SIGN_UP);
			dispatcher.forward(request, response);
			return;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
	}
}
