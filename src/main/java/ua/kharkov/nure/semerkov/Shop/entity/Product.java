package ua.kharkov.nure.semerkov.Shop.entity;

import java.io.Serializable;

/**
 * The <code>Product</code> class represents product data.
 *
 * @author Volodymyr_Semerkov
 */
public class Product implements Serializable, Cloneable {
    private static final long serialVersionUID = 7725130248489447322L;
    private long id;
    private String name;
    private double price;
    private int number;
    private String photoFileName;
    private Manufacturer manufacturer;
    private Category category;
    private String info;

    public Product() {
    }

    public Product(long id) {
        this.id = id;
    }

    public Product(long id, String name, double price, int number, String photoFileName,
                   Manufacturer manufacturer, Category category, String info) {
        this(id);
        this.name = name;
        this.price = price;
        this.number = number;
        this.photoFileName = photoFileName;
        this.manufacturer = manufacturer;
        this.category = category;
        this.info = info;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getPhotoFileName() {
        return photoFileName;
    }

    public void setPhotoFileName(String photoFileName) {
        this.photoFileName = photoFileName;
    }

    public Manufacturer getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(Manufacturer manufacturer) {
        this.manufacturer = manufacturer;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Product [id=");
        sb.append(id);
        sb.append(", name=");
        sb.append(name);
        sb.append(", price=");
        sb.append(price);
        sb.append(", number=");
        sb.append(number);
        sb.append(", photoFileName=");
        sb.append(photoFileName);
        sb.append(", manufacturerName=");
        sb.append(manufacturer.getName());
        sb.append(", categoryName=");
        sb.append(category.getName());
        sb.append(", info=");
        sb.append(info);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || (obj.getClass() != this.getClass()))
            return false;
        Product otherProduct = (Product) obj;
        return this.getId() == otherProduct.getId();
    }

    @Override
    public int hashCode() {
        return new Long(this.getId()).hashCode();
    }

    @Override
    public Product clone() throws CloneNotSupportedException {
        return (Product) super.clone();
    }
}
