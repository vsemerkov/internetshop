package ua.kharkov.nure.semerkov.Shop.parameter;

/**
 * Holder for mapper parameters names.
 * 
 * @author Volodymyr_Semerkov
 * 
 */
public final class MapperParameters {

	// Total mapper parameter.
	public static final String ID = "id";

	// User mapper parameters.
	public static final String USER__FIRST_NAME = "first_name";
	public static final String USER__LAST_NAME = "last_name";
	public static final String USER__EMAIL = "email";
	public static final String USER__PASSWORD = "password";
	public static final String USER__SUBSCRIBE = "subscribe";
	public static final String USER__ROLE = "role";
	public static final String USER__IMAGE_NAME = "image_name";
	public static final String USER__LAST_LOGIN = "last_login";
	public static final String USER__ATTEMPT_NUMBER = "attempt_number";
	public static final String USER__UNLOCKING_DATE = "unlocking_date";

	// Product mapper parameters.
	public static final String PRODUCT__NAME = "name";
	public static final String PRODUCT__PRICE = "price";
    public static final String PRODUCT__NUMBER = "number";
	public static final String PRODUCT__PHOTO_FILE_NAME = "photo_file_name";
	public static final String PRODUCT__MANUFACTURER_ID = "manufacturer_id";
	public static final String PRODUCT__MANUFACTURER_NAME = "manufacturer_name";
	public static final String PRODUCT__CATEGORY_ID = "category_id";
	public static final String PRODUCT__CATEGORY_NAME = "category_name";
	public static final String PRODUCT__INFO = "info";

	// Category mapper parameter.
	public static final String CATEGORY__NAME = "name";

	// Manufacturer mapper parameter.
	public static final String MANUFACTURER__NAME = "name";

	// Order mapper parameters.
	public static final String ORDER__STATUS = "order_status";
	public static final String ORDER__CANCELED_STATUS = "canceled_order_status";
	public static final String ORDER__DATE = "order_date";
	public static final String ORDER__USER_ID = "user_id";
	public static final String ORDER__DELIVERY_TYPE = "delivery_type";
	public static final String ORDER__PAYMENT_TYPE = "payment_type";
	public static final String ORDER__CARD_NUMBER = "card_number";
	public static final String ORDER__ADDRESS = "address";

	// Ordered product mapper parameters.
	public static final String ORDERED_PRODUCT__ORDER_ID = "order_id";
	public static final String ORDERED_PRODUCT__PRODUCT_ID = "product_id";
	public static final String ORDERED_PRODUCT__PRICE = "price";
	public static final String ORDERED_PRODUCT__NUMBER = "number";
}
