package ua.kharkov.nure.semerkov.Shop.dao.mapper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import ua.kharkov.nure.semerkov.Shop.CanceledOrderStatus;
import ua.kharkov.nure.semerkov.Shop.dao.db.DeliveryType;
import ua.kharkov.nure.semerkov.Shop.dao.db.PaymentType;
import ua.kharkov.nure.semerkov.Shop.parameter.MapperParameters;
import ua.kharkov.nure.semerkov.Shop.OrderStatus;
import ua.kharkov.nure.semerkov.Shop.entity.Order;

/**
 * Order mapper.
 *
 * @author Volodymyr_Semrkov
 */
public class OrderMapper {
    public static Order unMapOrder(ResultSet rs) throws SQLException {
        Order order = new Order(rs.getLong(MapperParameters.ID));
        order.setOrderStatus(OrderStatus.valueOf(rs
                .getString(MapperParameters.ORDER__STATUS)));
        String orderCanceledStatus = rs
                .getString(MapperParameters.ORDER__CANCELED_STATUS);
        order.setCanceledOrderStatus(orderCanceledStatus != null ? CanceledOrderStatus.valueOf(orderCanceledStatus) : null);
        order.setOrderDate(rs.getTimestamp(MapperParameters.ORDER__DATE));
        order.setUserId(rs.getLong(MapperParameters.ORDER__USER_ID));
        order.setDeliveryType(DeliveryType.valueOf(rs
                .getString(MapperParameters.ORDER__DELIVERY_TYPE)));
        order.setPaymentType(PaymentType.valueOf(rs
                .getString(MapperParameters.ORDER__PAYMENT_TYPE)));
        order.setAddress(rs.getString(MapperParameters.ORDER__ADDRESS));
        return order;
    }

    public static void mapOrder(Order order, PreparedStatement pstmt)
            throws SQLException {
        pstmt.setString(1, order.getOrderStatus().name());
        if (order.getCanceledOrderStatus() != null) {
            pstmt.setString(2, order.getCanceledOrderStatus().name());
        } else {
            pstmt.setString(2, null);
        }
        if (order.getOrderDate() != null) {
            pstmt.setTimestamp(3, new Timestamp(order.getOrderDate().getTime()));
        } else {
            pstmt.setTimestamp(3, null);
        }
        pstmt.setLong(4, order.getUserId());
        pstmt.setString(5, order.getDeliveryType().name());
        pstmt.setString(6, order.getPaymentType().name());
        pstmt.setString(7, order.getAddress());
    }
}
