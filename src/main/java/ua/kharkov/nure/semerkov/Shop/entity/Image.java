package ua.kharkov.nure.semerkov.Shop.entity;

import java.awt.image.BufferedImage;

public class Image {
	private String imageName;
	private BufferedImage bufferedImage;

	public Image() {
	}

	public Image(String imageName, BufferedImage bufferedImage) {
		this.imageName = imageName;
		this.bufferedImage = bufferedImage;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public BufferedImage getBufferedImage() {
		return bufferedImage;
	}

	public void setBufferedImage(BufferedImage bufferedImage) {
		this.bufferedImage = bufferedImage;
	}
}
