package ua.kharkov.nure.semerkov.Shop.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import ua.kharkov.nure.semerkov.Shop.dao.UserDAO;
import ua.kharkov.nure.semerkov.Shop.dao.db.jdbc.JdbcConnectionHolder;
import ua.kharkov.nure.semerkov.Shop.dao.mapper.UserMapper;
import ua.kharkov.nure.semerkov.Shop.dao.exception.DAOException;
import ua.kharkov.nure.semerkov.Shop.db.DatabaseManager;
import ua.kharkov.nure.semerkov.Shop.entity.User;

/**
 * Class for access to user data.
 *
 * @author Volodymyr_Semrkov
 */
public class MysqlUserDAO implements UserDAO {
    private static final String SQL__FIND_USER_BY_EMAIL = "SELECT * FROM users where email=?";
    private static final String SQL__FIND_USER_BY_ID = "SELECT * FROM users where id=?";
    private static final String SQL__INSERT_USER = "INSERT INTO users(email, password, first_name, last_name, "
            + "subscribe, role, image_name, last_login, attempt_number, unlocking_date) "
            + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String SQL__UPDATE_USER = "UPDATE users SET password=?, first_name=?, last_name=?, "
            + "subscribe=?, role=?, image_name=?, last_login=?, attempt_number=?, unlocking_date=? WHERE email=?";
    private static final String SQL__REMOVE_USER = "DELETE FROM users WHERE email=?";

    @Override
    public boolean containsUser(String email) throws DAOException {
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__FIND_USER_BY_EMAIL);
            pstmt.setString(1, email);
            return pstmt.executeQuery().first();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public boolean addUser(User user) throws DAOException {
        if (containsUser(user.getEmail()))
            throw new DAOException(
                    "User with such email address already exists in database!");
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__INSERT_USER);
            UserMapper.mapUserForAdd(user, pstmt);
            return pstmt.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public User getUser(String email) throws DAOException {
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__FIND_USER_BY_EMAIL);
            pstmt.setString(1, email);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                return UserMapper.unMapUser(rs);
            }
            return null;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public User getUser(long userId) throws DAOException {
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__FIND_USER_BY_ID);
            pstmt.setLong(1, userId);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                return UserMapper.unMapUser(rs);
            }
            return null;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public boolean removeUser(String email) throws DAOException {
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__REMOVE_USER);
            pstmt.setString(1, email);
            return pstmt.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public boolean updateUser(User user) throws DAOException {
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__UPDATE_USER);
            UserMapper.mapUserForUpdate(user, pstmt);
            return pstmt.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }
}
