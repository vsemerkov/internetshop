package ua.kharkov.nure.semerkov.Shop.dao.exception;

/**
 * This class represents DAOExcepton object
 * 
 * @author Volodymyr_Semerkov
 * 
 */
public class DAOException extends Exception {
	private static final long serialVersionUID = -4228928927428907518L;

	public DAOException() {
		super();
	}

	public DAOException(String message) {
		super(message);
	}

	public DAOException(String message, Throwable cause) {
		super(message, cause);
	}

	public DAOException(Throwable cause) {
		super(cause);
	}

	protected DAOException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
