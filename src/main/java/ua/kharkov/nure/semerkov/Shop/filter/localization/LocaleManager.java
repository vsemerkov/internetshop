package ua.kharkov.nure.semerkov.Shop.filter.localization;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Main interface for the LocaleManager pattern implementation.
 * 
 * @author Volodymyr_Semerkov
 * 
 */
public interface LocaleManager {
	public Locale getLocale(HttpServletRequest request);

	public void setLocale(Locale locale, HttpServletRequest request,
			HttpServletResponse response);
}
