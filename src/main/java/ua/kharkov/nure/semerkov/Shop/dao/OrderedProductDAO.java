package ua.kharkov.nure.semerkov.Shop.dao;

import java.util.List;

import ua.kharkov.nure.semerkov.Shop.dao.exception.DAOException;
import ua.kharkov.nure.semerkov.Shop.entity.OrderedProduct;

/**
 * Interface for access to ordered products data.
 * 
 * @author Volodymyr_Semrkov
 * 
 */
public interface OrderedProductDAO {
	public boolean addOrderedProduct(OrderedProduct orderedProduct)
			throws DAOException;
	
	public boolean addOrderedProducts(List<OrderedProduct> orderedProducts)
			throws DAOException;

	public boolean removeOrderedProduct(long id) throws DAOException;
	
	public boolean removeOrderedProducts(long orderId) throws DAOException;

	public List<OrderedProduct> getOrderedProducts(long orderId)
			throws DAOException;
}
