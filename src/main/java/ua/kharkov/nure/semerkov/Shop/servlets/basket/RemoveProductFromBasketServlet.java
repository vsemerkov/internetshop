package ua.kharkov.nure.semerkov.Shop.servlets.basket;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.kharkov.nure.semerkov.Shop.parameter.BasketParameters;
import ua.kharkov.nure.semerkov.Shop.Path;
import ua.kharkov.nure.semerkov.Shop.entity.Basket;

/**
 * Servlet implementation class RemoveProductFromBasketServlet
 */
public class RemoveProductFromBasketServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger
			.getLogger(RemoveProductFromBasketServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RemoveProductFromBasketServlet() {
		super();
		if (log.isDebugEnabled()) {
			log.debug("Servlet creates");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		if (log.isDebugEnabled()) {
			log.debug("POST method starts");
		}
		boolean result = true;
		long id = -1;
		if (log.isTraceEnabled()) {
			log.trace("product id --> "
					+ request.getParameter(BasketParameters.ID));
		}
		if (request.getParameter(BasketParameters.ID) != null) {
			try {
				id = Long.parseLong(request.getParameter(BasketParameters.ID));
			} catch (NumberFormatException e) {
				result = false;
				log.error(e.getMessage());
			}
		} else {
			result = false;
		}
		if (result) {
			HttpSession session = request.getSession();
			Basket basket = (Basket) session
					.getAttribute(BasketParameters.BASKET);
			if (log.isTraceEnabled()) {
				log.trace("basket --> " + basket);
			}
			if (basket != null) {
				basket.removeProduct(id);
				session.setAttribute(BasketParameters.BASKET, basket);
				if (log.isTraceEnabled()) {
					log.trace("basket --> " + basket);
				}
			}
			response.sendRedirect(Path.COMMAND__BASKET);
			if (log.isDebugEnabled()) {
				log.debug("Response was sent");
			}
		}
	}
}
