package ua.kharkov.nure.semerkov.Shop.dao.db.search.product;

/**
 * The <code>SortParameter</code> class contains the enumeration of sort
 * parameters.
 * 
 * @author Volodymyr_Semerkov
 * 
 */
public enum SortParameter {
	PRICE, NAME
}
