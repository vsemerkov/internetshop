package ua.kharkov.nure.semerkov.Shop.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.kharkov.nure.semerkov.Shop.Path;
import ua.kharkov.nure.semerkov.Shop.dao.db.search.product.ProductSearchParametersBean;
import ua.kharkov.nure.semerkov.Shop.dao.db.search.product.ProductSearchParametersParser;
import ua.kharkov.nure.semerkov.Shop.parameter.OrderParameters;
import ua.kharkov.nure.semerkov.Shop.parameter.SearchParameters;
import ua.kharkov.nure.semerkov.Shop.parameter.ServiceParameters;
import ua.kharkov.nure.semerkov.Shop.dao.db.search.product.SortParameter;
import ua.kharkov.nure.semerkov.Shop.dao.exception.DAOException;
import ua.kharkov.nure.semerkov.Shop.entity.Category;
import ua.kharkov.nure.semerkov.Shop.entity.Manufacturer;
import ua.kharkov.nure.semerkov.Shop.entity.Product;
import ua.kharkov.nure.semerkov.Shop.service.CategoryService;
import ua.kharkov.nure.semerkov.Shop.service.ManufacturerService;
import ua.kharkov.nure.semerkov.Shop.service.ProductService;

/**
 * Servlet implementation class <code>IndexServlet</code>
 */
public class IndexServlet extends HttpServlet {
    private static final long serialVersionUID = 7509527962723289491L;
    private CategoryService categoryService;
    private ManufacturerService manufacturerService;
    private ProductService productService;
    private static final Logger log = Logger.getLogger(IndexServlet.class);

    /**
     * @see HttpServlet#HttpServlet()
     */
    public IndexServlet() {
        super();
        if (log.isDebugEnabled()) {
            log.debug("Servlet creates");
        }
    }

    @Override
    public void init() throws ServletException {
        ServletContext servletContext = getServletContext();
        categoryService = (CategoryService) servletContext
                .getAttribute(ServiceParameters.CATEGORY__SERVICE);
        if (categoryService == null) {
            log.error("Category service attribute is not exists.");
            throw new IllegalStateException(
                    "Category service attribute is not exists.");
        }
        manufacturerService = (ManufacturerService) servletContext
                .getAttribute(ServiceParameters.MANUFACTURER__SERVICE);
        if (manufacturerService == null) {
            log.error("Manufacturer service attribute is not exists.");
            throw new IllegalStateException(
                    "Manufacturer service attribute is not exists.");
        }
        productService = (ProductService) servletContext
                .getAttribute(ServiceParameters.PRODUCT__SERVICE);
        if (productService == null) {
            log.error("Product service attribute is not exists.");
            throw new IllegalStateException(
                    "Product service attribute is not exists.");
        }
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        if (log.isDebugEnabled()) {
            log.debug("GET request");
        }
        ProductSearchParametersBean searchParametersBean = ProductSearchParametersParser
                .getSearchParametersBean(request.getParameterMap());
        try {
            setParameters(request);
            int numberOfProducts = productService
                    .getNumberOfRequiredProducts(searchParametersBean);
            validatePageNumber(numberOfProducts, searchParametersBean);
            setSearchParameters(request, searchParametersBean);
            request.setAttribute(SearchParameters.NUMBER_OF_ELEMENTS, numberOfProducts);
            if (numberOfProducts != 0) {
                List<Product> products = productService
                        .findProducts(searchParametersBean);
                request.setAttribute("products", products);
            }
            RequestDispatcher dispatcher = request
                    .getRequestDispatcher(Path.PAGE__INDEX);
            dispatcher.forward(request, response);
            if (log.isDebugEnabled()) {
                log.debug("Response was sent");
            }
        } catch (DAOException e) {
            log.error(e.getMessage());
            response.sendRedirect(Path.PAGE__ERROR_PAGE);
        }
    }

    private void setParameters(HttpServletRequest request) throws DAOException {
        List<Category> categories = categoryService.getAllCategories();
        request.setAttribute(OrderParameters.CATEGORIES, categories);
        List<Manufacturer> manufacturers = manufacturerService
                .getAllManufacturers();
        request.setAttribute(OrderParameters.MANUFACTURERS, manufacturers);
        List<String> sortParameters = new ArrayList<String>();
        sortParameters.add(SortParameter.PRICE.name().toLowerCase());
        sortParameters.add(SortParameter.NAME.name().toLowerCase());
        request.setAttribute(SearchParameters.SORT_PARAMETER, sortParameters);
        List<String> sortOrders = new ArrayList<String>();
        sortOrders.add(SearchParameters.SORT_ORDER_ASC);
        sortOrders.add(SearchParameters.SORT_ORDER_DESC);
        request.setAttribute(SearchParameters.SORT_ORDER, sortOrders);
    }

    private void setSearchParameters(HttpServletRequest request,
                                     ProductSearchParametersBean searchParametersBean) {
        if (searchParametersBean.isMinPriceSelected()) {
            request.setAttribute(SearchParameters.MIN_PRICE,
                    searchParametersBean.getMinPrice());
        }
        if (searchParametersBean.isMaxPriceSelected()) {
            request.setAttribute(SearchParameters.MAX_PRICE,
                    searchParametersBean.getMaxPrice());
        }
        request.setAttribute(SearchParameters.SELECTED_CATEGORY,
                searchParametersBean.getCategories());
        request.setAttribute(SearchParameters.SELECTED_MANUFACTURER,
                searchParametersBean.getManufacturers());
        request.setAttribute(SearchParameters.NAME,
                searchParametersBean.getProductName());
        if (searchParametersBean.getSortParameter() != null) {
            request.setAttribute(SearchParameters.SELECTED_SORT_PARAMETER,
                    searchParametersBean.getSortParameter().name()
                            .toLowerCase());
        }
        if (searchParametersBean.getSortOrder() != null) {
            request.setAttribute(SearchParameters.SELECTED_SORT_ORDER,
                    searchParametersBean.getSortOrder().name().toLowerCase());
        }
        request.setAttribute(SearchParameters.ELEMENT_NUMBER,
                searchParametersBean.getProductsOnPage());
        request.setAttribute(SearchParameters.PAGE_NUMBER,
                searchParametersBean.getPageNumber());
    }

    private void validatePageNumber(int numberOfProducts,
                                    ProductSearchParametersBean searchParametersBean) {
        int numberOfPages = numberOfProducts
                / searchParametersBean.getProductsOnPage()
                + ((numberOfProducts % searchParametersBean.getProductsOnPage()) != 0 ? 1
                : 0);
        if (searchParametersBean.getPageNumber() < 1
                || searchParametersBean.getPageNumber() > numberOfPages) {
            searchParametersBean.setPageNumber(1);
        }
    }
}
