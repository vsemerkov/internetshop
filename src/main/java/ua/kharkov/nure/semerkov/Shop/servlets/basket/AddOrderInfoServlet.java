package ua.kharkov.nure.semerkov.Shop.servlets.basket;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.kharkov.nure.semerkov.Shop.parameter.BasketParameters;
import ua.kharkov.nure.semerkov.Shop.parameter.ErrorParameters;
import ua.kharkov.nure.semerkov.Shop.parameter.OrderParameters;
import ua.kharkov.nure.semerkov.Shop.OrderStatus;
import ua.kharkov.nure.semerkov.Shop.Path;
import ua.kharkov.nure.semerkov.Shop.parameter.UserParameters;
import ua.kharkov.nure.semerkov.Shop.dao.db.DeliveryType;
import ua.kharkov.nure.semerkov.Shop.dao.db.PaymentType;
import ua.kharkov.nure.semerkov.Shop.entity.Basket;
import ua.kharkov.nure.semerkov.Shop.entity.Order;
import ua.kharkov.nure.semerkov.Shop.entity.OrderItem;
import ua.kharkov.nure.semerkov.Shop.entity.OrderedProduct;
import ua.kharkov.nure.semerkov.Shop.entity.User;
import ua.kharkov.nure.semerkov.Shop.security.PaymentValidator;

/**
 * Servlet implementation class AddOrderInfoServlet
 */
public class AddOrderInfoServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger
            .getLogger(AddOrderInfoServlet.class);

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddOrderInfoServlet() {
        super();
        if (log.isDebugEnabled()) {
            log.debug("Servlet creates");
        }
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        if (log.isDebugEnabled()) {
            log.debug("GET method starts");
        }
        HttpSession session = request.getSession();
        Order order = (Order) session.getAttribute(OrderParameters.ORDER);
        if (log.isTraceEnabled()) {
            log.trace("order --> " + order);
        }
        Basket basket = (Basket) session
                .getAttribute(BasketParameters.BASKET);
        if (log.isTraceEnabled()) {
            log.trace("basket --> " + basket);
        }
        if (order != null && basket != null) {
            List<OrderItem> items = basket.getListOfProducts();
            List<OrderItem> orderItems = new ArrayList<OrderItem>();
            for (OrderItem orderItem : items) {
                if (orderItem.getNumber() > 0) {
                    orderItems.add(orderItem);
                }
            }
            request.setAttribute("items", orderItems);
            RequestDispatcher dispatcher = request
                    .getRequestDispatcher(Path.PAGE__ADD_ORDER_INFO);
            dispatcher.forward(request, response);
        } else {
            session.setAttribute("error", "Incorrect info was entered!");
            response.sendRedirect(Path.COMMAND__MAKE_ORDER);
        }
        if (log.isDebugEnabled()) {
            log.debug("Response was sent");
        }
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {
        if (log.isDebugEnabled()) {
            log.debug("POST method starts");
        }
        boolean result = true;
        String address = null;
        DeliveryType deliveryType = null;
        PaymentType paymentType = null;
        String cardNumber = null;
        String cvv = null;
        HttpSession session = request.getSession();
        List<String> errorList = new ArrayList<String>();
        session.removeAttribute("errors");
        session.removeAttribute("error");
        if (request.getParameter(OrderParameters.ORDER__DELIVERY_TYPE) != null
                && request.getParameter(OrderParameters.ORDER__PAYMENT_TYPE) != null) {
            address = request.getParameter(OrderParameters.ORDER__ADDRESS);
            if (address == null || address.trim().length() < 5) {
                result = false;
                errorList.add(ErrorParameters.ADDRESS_ERROR);
            }
            deliveryType = DeliveryType.valueOf(request.getParameter(
                    OrderParameters.ORDER__DELIVERY_TYPE).toUpperCase());
            paymentType = PaymentType.valueOf(request.getParameter(
                    OrderParameters.ORDER__PAYMENT_TYPE).toUpperCase());
            if (paymentType == PaymentType.NON_CASH) {
                if (request.getParameter(OrderParameters.ORDER__CARD_NUMBER) != null) {
                    cardNumber = request
                            .getParameter(OrderParameters.ORDER__CARD_NUMBER);
                    if (!PaymentValidator.checkCardNumber(cardNumber)) {
                        result = false;
                        errorList.add(ErrorParameters.CARD_NUMBER_ERROR);
                    }
                } else {
                    result = false;
                    errorList.add(ErrorParameters.CARD_NUMBER_ERROR);
                }
                if (request.getParameter(OrderParameters.ORDER__CVV) != null) {
                    cvv = request
                            .getParameter(OrderParameters.ORDER__CVV);
                    if (!PaymentValidator.checkCVV(cvv)) {
                        result = false;
                        errorList.add(ErrorParameters.CVV_ERROR);
                    }
                } else {
                    result = false;
                    errorList.add(ErrorParameters.CVV_ERROR);
                }
            }
        } else {
            result = false;
        }
        if (result) {
            Basket basket = (Basket) session
                    .getAttribute(BasketParameters.BASKET);
            if (log.isTraceEnabled()) {
                log.trace("basket --> " + basket);
            }
            List<OrderedProduct> orderedProducts = null;
            if (basket != null) {
                List<OrderItem> items = basket.getListOfProducts();
                orderedProducts = new ArrayList<OrderedProduct>();
                int numberOfProducts = 0;
                List<OrderItem> orderItems = new ArrayList<OrderItem>();
                for (OrderItem orderItem : items) {
                    if (orderItem.getNumber() > 0) {
                        orderItems.add(orderItem);
                    }
                }
                for (OrderItem item : orderItems) {
                    OrderedProduct orderedProduct = new OrderedProduct(
                            item.getOrderId(), item.getProduct().getId(),
                            item.getPrice(), item.getNumber());
                    orderedProducts.add(orderedProduct);
                    numberOfProducts += item.getNumber();
                }
                if (numberOfProducts < 1) {
                    result = false;
                }
            }
            if (result) {
                Order order = new Order();
                order.setOrderStatus(OrderStatus.FORMED);
                order.setOrderDate(Calendar.getInstance().getTime());
                order.setUserId(((User) session
                        .getAttribute(UserParameters.USER)).getId());
                order.setOrderedProducts(orderedProducts);
                order.setDeliveryType(deliveryType);
                order.setPaymentType(paymentType);
                order.setCardNumber(cardNumber);
                order.setAddress(address);
                session.setAttribute(OrderParameters.ORDER, order);
                response.sendRedirect(Path.COMMAND__ADD_ORDER_INFO);
            } else {
                response.sendRedirect(Path.COMMAND__BASKET);
            }
        } else {
            session.setAttribute("errors", errorList);
            response.sendRedirect(Path.COMMAND__MAKE_ORDER);
        }
        if (log.isDebugEnabled()) {
            log.debug("Response was sent");
        }
    }
}
