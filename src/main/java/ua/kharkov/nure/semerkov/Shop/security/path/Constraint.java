package ua.kharkov.nure.semerkov.Shop.security.path;

import java.util.ArrayList;
import java.util.List;

import ua.kharkov.nure.semerkov.Shop.Role;

/**
 * The <code>Constraint</code> class contains forbidden url-patterns for roles.
 * 
 * @author Volodymyr_Semerkov
 * 
 */
public class Constraint {
	private String URLPattern;
	private List<Role> roles;

	public String getURLPattern() {
		return URLPattern;
	}

	public void setURLPattern(String urlPattern) {
		URLPattern = urlPattern;
	}

	public List<Role> getRoles() {
		if (roles == null) {
			roles = new ArrayList<Role>();
		}
		return roles;
	}
}
