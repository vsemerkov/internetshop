package ua.kharkov.nure.semerkov.Shop.servlets.manufacturer;

import org.apache.log4j.Logger;
import ua.kharkov.nure.semerkov.Shop.Path;
import ua.kharkov.nure.semerkov.Shop.dao.exception.DAOException;
import ua.kharkov.nure.semerkov.Shop.entity.Manufacturer;
import ua.kharkov.nure.semerkov.Shop.parameter.OrderParameters;
import ua.kharkov.nure.semerkov.Shop.parameter.ServiceParameters;
import ua.kharkov.nure.semerkov.Shop.service.ManufacturerService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Servlet implementation class AdminManufacturersServlet
 */
public class AdminManufacturersServlet extends HttpServlet {
    private ManufacturerService manufacturerService;
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AdminManufacturersServlet.class);

    /**
     * @see javax.servlet.http.HttpServlet#HttpServlet()
     */
    public AdminManufacturersServlet() {
        super();
        if (log.isDebugEnabled()) {
            log.debug("Servlet creates");
        }
    }

    @Override
    public void init() throws ServletException {
        ServletContext servletContext = getServletContext();
        manufacturerService = (ManufacturerService) servletContext
                .getAttribute(ServiceParameters.MANUFACTURER__SERVICE);
        if (manufacturerService == null) {
            log.error("Manufacturer service attribute is not exists.");
            throw new IllegalStateException(
                    "Manufacturer service attribute is not exists.");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        if (log.isDebugEnabled()) {
            log.debug("GET method starts");
        }
        try {
            List<Manufacturer> manufacturers = manufacturerService.getAllManufacturers();
            request.setAttribute(OrderParameters.MANUFACTURERS, manufacturers);
            RequestDispatcher dispatcher = request
                    .getRequestDispatcher(Path.PAGE__ADMIN_MANUFACTURERS);
            dispatcher.forward(request, response);
        } catch (DAOException e) {
            log.error(e.getMessage());
            response.sendRedirect(Path.PAGE__ERROR_PAGE);
        }
        if (log.isDebugEnabled()) {
            log.debug("Response was sent");
        }
    }
}
