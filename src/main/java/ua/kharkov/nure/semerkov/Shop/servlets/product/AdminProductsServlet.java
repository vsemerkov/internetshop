package ua.kharkov.nure.semerkov.Shop.servlets.product;

import org.apache.log4j.Logger;
import ua.kharkov.nure.semerkov.Shop.Path;
import ua.kharkov.nure.semerkov.Shop.dao.exception.DAOException;
import ua.kharkov.nure.semerkov.Shop.entity.Product;
import ua.kharkov.nure.semerkov.Shop.parameter.OrderParameters;
import ua.kharkov.nure.semerkov.Shop.parameter.ServiceParameters;
import ua.kharkov.nure.semerkov.Shop.service.ProductService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Servlet implementation class AdminProductsServlet
 */
public class AdminProductsServlet extends HttpServlet {
    private ProductService productService;
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AdminProductsServlet.class);

    /**
     * @see javax.servlet.http.HttpServlet#HttpServlet()
     */
    public AdminProductsServlet() {
        super();
        if (log.isDebugEnabled()) {
            log.debug("Servlet creates");
        }
    }

    @Override
    public void init() throws ServletException {
        ServletContext servletContext = getServletContext();
        productService = (ProductService) servletContext
                .getAttribute(ServiceParameters.PRODUCT__SERVICE);
        if (productService == null) {
            log.error("Product service attribute is not exists.");
            throw new IllegalStateException(
                    "Product service attribute is not exists.");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        if (log.isDebugEnabled()) {
            log.debug("GET method starts");
        }
        try {
            List<Product> products = productService.getAllProducts();
            request.setAttribute("products", products);
            RequestDispatcher dispatcher = request
                    .getRequestDispatcher(Path.PAGE__ADMIN_PRODUCTS);
            dispatcher.forward(request, response);
        } catch (DAOException e) {
            log.error(e.getMessage());
            response.sendRedirect(Path.PAGE__ERROR_PAGE);
        }
        if (log.isDebugEnabled()) {
            log.debug("Response was sent");
        }
    }
}
