package ua.kharkov.nure.semerkov.Shop.parameter;

/**
 * Holder for search parameters names.
 *
 * @author Volodymyr_Semerkov
 */
public final class SearchParameters {
    // Form parameters.
    public static final String MIN_PRICE = "minPrice";
    public static final String MAX_PRICE = "maxPrice";
    public static final String CATEGORY = "category";
    public static final String MANUFACTURER = "manufacturer";
    public static final String NAME = "name";
    public static final String SORT_PARAMETER = "sortParameter";
    public static final String SORT_ORDER = "sortOrder";
    public static final String PAGE = "page";
    public static final String PAGE_NUMBER = "pageNumber";
    // Selected parameters.
    public static final String SELECTED_CATEGORY = "selectedCategories";
    public static final String SELECTED_MANUFACTURER = "selectedManufacturers";
    public static final String SELECTED_SORT_PARAMETER = "selectedSortParameter";
    public static final String SELECTED_SORT_ORDER = "selectedSortOrder";
    //Sort orders
    public static final String SORT_ORDER_ASC = "asc";
    public static final String SORT_ORDER_DESC = "desc";
    //Order parameters
    public static final String ORDER_ID = "orderId";
    public static final String ORDER_NUMBER = "orderNumber";
    public static final String ORDER_STATUS = "orderStatus";
    public static final String START_DATE = "startDate";
    public static final String END_DATE = "endDate";
    //pagination elements
    public static final String ELEMENT_NUMBER = "elementNumber";
    public static final String NUMBER_OF_ELEMENTS = "numberOfElements";
}
