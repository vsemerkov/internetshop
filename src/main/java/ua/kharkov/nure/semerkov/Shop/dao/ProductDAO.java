package ua.kharkov.nure.semerkov.Shop.dao;

import java.util.List;

import ua.kharkov.nure.semerkov.Shop.dao.db.search.product.ProductSearchParametersBean;
import ua.kharkov.nure.semerkov.Shop.dao.exception.DAOException;
import ua.kharkov.nure.semerkov.Shop.entity.Product;

/**
 * Interface for access to product data.
 *
 * @author Volodymyr_Semrkov
 */
public interface ProductDAO {
    public boolean containsProduct(long id) throws DAOException;

    public boolean addProduct(Product product) throws DAOException;

    public Product getProduct(long id) throws DAOException;

    public boolean removeProduct(long id) throws DAOException;

    public boolean updateProduct(Product product) throws DAOException;

    public double getMaxPrice() throws DAOException;

    public String getImagePath(long id) throws DAOException;

    public int getNumberOfRequiredProducts(
            ProductSearchParametersBean searchParameterBean) throws DAOException;

    public List<Product> findProducts(ProductSearchParametersBean searchParameterBean)
            throws DAOException;

    public List<Product> getAllProducts() throws DAOException;
}
