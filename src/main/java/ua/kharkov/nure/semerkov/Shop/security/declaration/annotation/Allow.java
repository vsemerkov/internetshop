package ua.kharkov.nure.semerkov.Shop.security.declaration.annotation;

import ua.kharkov.nure.semerkov.Shop.Role;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface Allow {
    Role[] value() default {Role.VISITOR};
}