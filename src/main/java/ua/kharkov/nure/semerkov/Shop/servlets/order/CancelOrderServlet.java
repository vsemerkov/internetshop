package ua.kharkov.nure.semerkov.Shop.servlets.order;

import org.apache.log4j.Logger;
import ua.kharkov.nure.semerkov.Shop.Path;
import ua.kharkov.nure.semerkov.Shop.Role;
import ua.kharkov.nure.semerkov.Shop.dao.exception.DAOException;
import ua.kharkov.nure.semerkov.Shop.entity.*;
import ua.kharkov.nure.semerkov.Shop.parameter.OrderParameters;
import ua.kharkov.nure.semerkov.Shop.parameter.ServiceParameters;
import ua.kharkov.nure.semerkov.Shop.parameter.UserParameters;
import ua.kharkov.nure.semerkov.Shop.service.OrderService;
import ua.kharkov.nure.semerkov.Shop.service.ProductService;
import ua.kharkov.nure.semerkov.Shop.service.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Servlet implementation class CancelOrderServlet
 */
public class CancelOrderServlet extends HttpServlet {
    private OrderService orderService;
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(CancelOrderServlet.class);

    /**
     * @see javax.servlet.http.HttpServlet#HttpServlet()
     */
    public CancelOrderServlet() {
        super();
        if (log.isDebugEnabled()) {
            log.debug("Servlet creates");
        }
    }

    @Override
    public void init() throws ServletException {
        ServletContext servletContext = getServletContext();
        orderService = (OrderService) servletContext
                .getAttribute(ServiceParameters.ORDER__SERVICE);
        if (orderService == null) {
            log.error("Order service attribute is not exists.");
            throw new IllegalStateException(
                    "Order service attribute is not exists.");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {
        if (log.isDebugEnabled()) {
            log.debug("GET method starts");
        }
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(UserParameters.USER);
        try {
            Order order = null;
            List<OrderItem> orderItems = null;
            long orderId = Long.parseLong(request
                    .getParameter(OrderParameters.ORDER_ID));
            if (orderService.belong(orderId, user.getId()) || user.getRole() == Role.ADMIN) {
                orderService.cancelOrder(orderId, user.getRole());
            } else {
                response.sendError(423);
                if (log.isDebugEnabled()) {
                    log.debug("Response was sent");
                }
                return;
            }
            session.setAttribute(OrderParameters.ORDER_ID, orderId);
            response.sendRedirect(Path.COMMAND__ORDER);
        } catch (NullPointerException | NumberFormatException | DAOException e) {
            log.error(e.getMessage());
            response.sendRedirect(Path.PAGE__ERROR_PAGE);
        }
        if (log.isDebugEnabled()) {
            log.debug("Response was sent");
        }
    }
}
