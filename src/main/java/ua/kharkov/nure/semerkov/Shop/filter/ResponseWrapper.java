package ua.kharkov.nure.semerkov.Shop.filter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

/**
 * Response wrapper.
 * 
 * @author Volodymyr_Semerkov
 * 
 */
public class ResponseWrapper extends HttpServletResponseWrapper {
	private ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
	private StringWriter stringWriter = new StringWriter();
	private boolean streamUsed;
	private boolean writerUsed;

	public ResponseWrapper(HttpServletResponse response) {
		super(response);
	}

	@Override
	public ResponseOutputStream getOutputStream() throws IOException {
		if (writerUsed)
			throw new IllegalStateException();
		streamUsed = true;
		return new ResponseOutputStream(byteStream);
	}

	@Override
	public PrintWriter getWriter() throws IOException {
		if (streamUsed)
			throw new IllegalStateException();
		writerUsed = true;
		return new PrintWriter(stringWriter);
	}

	public boolean isStreamUsed() {
		return streamUsed;
	}

	public boolean isWriterUsed() {
		return writerUsed;
	}

	public String toString() {
		return stringWriter.toString();
	}

	public byte[] getBytes() {
		return byteStream.toByteArray();
	}
}
