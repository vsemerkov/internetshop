<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setBundle basename="resources" />

<div class="line">
	<fmt:message key="manufacturers" />
</div>
<c:forEach var="manufacturer" items="${manufacturers}">
	<c:set var="fl" value="false" />
	<c:forEach var="selectedManufacturer" items="${selectedManufacturers}">
		<c:if test="${manufacturer.id==selectedManufacturer}">
			<c:set var="fl" value="true" />
			<p>
				<input type="checkbox" name="manufacturer" value="${manufacturer.id}"
					checked /> <span class="parameter">${manufacturer.name}</span>
			</p>
		</c:if>
	</c:forEach>
	<c:if test="${fl eq 'false'}">
		<p>
			<input type="checkbox" name="manufacturer" value="${manufacturer.id}" />
			<span class="parameter">${manufacturer.name}</span>
		</p>
	</c:if>
</c:forEach>