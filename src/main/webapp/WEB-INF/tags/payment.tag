<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setBundle basename="resources" />


<div class="main_date_panel">
	<div class="date_panel"><fmt:message key="order.payment" /></div>
	<div class="date_panel_value">
		<select name="paymentType" class="large_input">
			<c:forEach var="type" items="${paymentTypes}">
				<c:choose>
					<c:when test="${paymentType eq type}">
						<option value="${type}" selected>
							<fmt:message key="payment.${type}" />
						</option>
					</c:when>
					<c:otherwise>
						<option value="${type}">
							<fmt:message key="payment.${type}" />
						</option>
					</c:otherwise>
				</c:choose>
			</c:forEach>
		</select>
	</div>
</div>
