<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setBundle basename="resources" />

<div class="line">
	<fmt:message key="categories" />
</div>
<c:forEach var="category" items="${categories}">
	<c:set var="fl" value="false" />
	<c:forEach var="selectedCategory" items="${selectedCategories}">
		<c:if test="${category.id==selectedCategory}">
			<c:set var="fl" value="true" />
			<p>
				<input type="checkbox" name="category" value="${category.id}"
					checked /> <span class="parameter">${category.name}</span>
			</p>
		</c:if>
	</c:forEach>
	<c:if test="${fl eq false}">
		<p>
			<input type="checkbox" name="category" value="${category.id}" /> <span
				class="parameter">${category.name}</span>
		</p>
	</c:if>
</c:forEach>