<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setBundle basename="resources"/>

<c:choose>
	<c:when test="${empty user}">
	<form name="loginForm" action="<c:url value="/login"/>" method="post" class="form-inline">
  		<input type="text" name="loginEmail" placeholder="<fmt:message key="email"/>" value="${email}">
  		<input type="password" name="loginPassword" placeholder="<fmt:message key="password"/>">
  		<button type="submit" name="sendLoginData" class="btn"><fmt:message key="login"/></button>
  		<a href="<c:url value="/registration"/>"><button type="button" class="btn"><fmt:message key="sign.in"/></button></a>
	</form>
	<span class="help-block error" id="loginError">${paramErrors['loginError'] }</span>
	</c:when>
	<c:otherwise>
		<form name="logoutForm" action="<c:url value="/logout"/>" method="post" class="form-inline">
			<img src="<c:url value="/image"/>" height="75" width="50">
			<span class="user_email"><c:out value="${user.email}"/></span>
			<button type="submit" class="btn"><fmt:message key="logout"/></button>
		</form>
	</c:otherwise>
</c:choose>