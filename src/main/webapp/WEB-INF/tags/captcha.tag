<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:choose>
	<c:when test="${initParam.captchaMode == 'hidden'}">
		<input type="hidden" name="captchaId" value="${captchaId}"/>
		<img src="<c:url value="/captcha?captchaId=${captchaId}"/>">
	</c:when>
	<c:otherwise>
		<img src="<c:url value="/captcha"/>">
	</c:otherwise>
</c:choose>