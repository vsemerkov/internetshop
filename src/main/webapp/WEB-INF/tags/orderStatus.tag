<%@ tag language="java" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<fmt:setBundle basename="resources"/>


<div class="main_date_panel">
    <div class="date_panel"><fmt:message key="order.status"/></div>
    <div class="date_panel_value">
        <c:set var="orderStatusName"
               value="status.${fn:toLowerCase(orderStatus)}"/>
        <select name="orderStatus" class="large_input">
            <option value="none"><fmt:message key="any"/></option>
            <c:forEach var="status" items="${orderStatuses}">
                <c:set var="statusName"
                       value="status.${fn:toLowerCase(status)}"/>
                <c:choose>
                    <c:when test="${orderStatus eq status}">
                        <option value="${status}" selected>
                            <fmt:message key="order.${statusName}"/>
                        </option>
                    </c:when>
                    <c:otherwise>
                        <option value="${status}">
                            <fmt:message key="order.${statusName}"/>
                        </option>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </select>
    </div>
</div>
