<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setBundle basename="resources" />

<select name="sortOrder" class="big_input">
	<c:forEach var="order" items="${sortOrder}">
		<c:choose>
			<c:when test="${selectedSortOrder eq order}">
				<option value="${order}" selected><fmt:message
						key="sort.${order}" /></option>
			</c:when>
			<c:otherwise>
				<option value="${order}"><fmt:message key="sort.${order}" /></option>
			</c:otherwise>
		</c:choose>
	</c:forEach>
</select>