<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/tags/paginationTag.tld" prefix="p"%>
<fmt:setBundle basename="resources" />

<c:set var="pageRemainder" value="${numberOfElements%elementNumber}" />
<c:set var="numberOfPages"
	value="${((numberOfElements-pageRemainder)/elementNumber)+(pageRemainder!=0?1:0)}" />
<c:if test="${numberOfPages > 0}">
	<p>
		<a href="<p:paginationTag value="1"/>">&lt;&lt;</a>

		<c:choose>
			<c:when test="${pageNumber!=1}">
				<a href="<p:paginationTag value="${pageNumber - 1}"/>">&lt;</a>
			</c:when>
			<c:otherwise>
				<a href="<p:paginationTag value="${pageNumber}"/>">&lt;</a>
			</c:otherwise>
		</c:choose>

		<c:forEach var="pageNo" begin="1" step="1" end="${numberOfPages}">
			<c:choose>
				<c:when test="${pageNo!=pageNumber}">
					<a href="<p:paginationTag value="${pageNo}"/>"><c:out
							value="${pageNo}" /></a>
				</c:when>
				<c:otherwise>
					<span class="item_title"><c:out value="${pageNo}" /></span>
				</c:otherwise>
			</c:choose>
		</c:forEach>

		<c:choose>
			<c:when test="${pageNumber!=numberOfPages}">
				<a href="<p:paginationTag value="${pageNumber + 1}"/>">&gt;</a>
			</c:when>
			<c:otherwise>
				<a href="<p:paginationTag value="${pageNumber}"/>">&gt;</a>
			</c:otherwise>
		</c:choose>

		<a href="<p:paginationTag value="${numberOfPages}"/>">&gt;&gt;</a>
	</p>
</c:if>