<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setBundle basename="resources" />

<select name="elementNumber" class="min_input">
	<c:forEach var="number" begin="5" step="5" end="20">
		<c:choose>
			<c:when test="${number == elementNumber}">
				<option value="${number}" selected>${number}</option>
			</c:when>
			<c:otherwise>
				<option value="${number}">${number}</option>
			</c:otherwise>
		</c:choose>
	</c:forEach>
</select>