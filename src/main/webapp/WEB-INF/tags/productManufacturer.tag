<%@ tag language="java" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<fmt:setBundle basename="resources"/>


<div class="main_date_panel">
    <div class="date_panel"><fmt:message key="title.product.manufacturer"/></div>
    <div class="date_panel_value">
        <select name="productManufacturer" class="large_input">
            <c:forEach var="manufacturer" items="${manufacturers}">
                <c:choose>
                    <c:when test="${manufacturer.name eq product.manufacturer.name}">
                        <option value="${manufacturer.id}" selected>
                            <c:out value="${manufacturer.name}"/>
                        </option>
                    </c:when>
                    <c:otherwise>
                        <option value="${manufacturer.id}">
                            <c:out value="${manufacturer.name}"/>
                        </option>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </select>
    </div>
</div>
