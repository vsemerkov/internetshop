<%@ tag language="java" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<fmt:setBundle basename="resources"/>


<div class="main_date_panel">
    <div class="date_panel"><fmt:message key="title.product.category"/></div>
    <div class="date_panel_value">
        <select name="productCategory" class="large_input">
            <c:forEach var="category" items="${categories}">
                <c:choose>
                    <c:when test="${category.name eq product.category.name}">
                        <option value="${category.id}" selected>
                            <c:out value="${category.name}"/>
                        </option>
                    </c:when>
                    <c:otherwise>
                        <option value="${category.id}">
                            <c:out value="${category.name}"/>
                        </option>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </select>
    </div>
</div>
