<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tags/localizationTag.tld" prefix="loc"%>

<c:if test="${not empty supportedLocales}">
	<div class="locales_panel">
		<c:forEach var="language" items="${supportedLocales}">
			<a href="<loc:localizationTag value="${language}"/>"><img
				src="<c:url value="/img/${language}.png"/>" class="locale_img"
				alt="${language}"></a>
		</c:forEach>
	</div>
</c:if>