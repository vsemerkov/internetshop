<%@ tag language="java" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tags/paginationTag.tld" prefix="p" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="t" %>
<fmt:setBundle basename="resources"/>

<div class="number_panel">
    <span class="item_title"><fmt:message key="total.number"/></span>
    <c:out value="${numberOfElements}"/>
    <t:pagination/>
</div>
<div class="items_panel">
    <c:if test="${numberOfElements == 0}">
        <fmt:message key="no.products"/>
    </c:if>
    <c:forEach var="product" items="${products}">
        <div class="item">
            <div class="item_picture_frame">
                <img src="<c:url value="/productImage?productId=${product.id}"/>"
                     class="item_picture">
            </div>
            <div class="item_info">
                <p>
					<span class="item_name">
					<a href="<c:url value="/clientProduct?productId=${product.id}"/>">${product.manufacturer.name}
                            ${product.name}</a>
                    </span>
                </p>

                <p>
                    <span class="item_title"><fmt:message key="category"/> </span>${product.category.name}</p>

                <p>
					<span class="item_title"><fmt:message key="manufacturer"/>
					</span>${product.manufacturer.name}</p>

                <p>
					<span class="item_title"><fmt:message key="product.name"/>
					</span>${product.name}</p>

                <p>
					<span class="item_price"><fmt:message key="price"/>
                        <fmt:formatNumber value="${product.price}" var="price" type="number"
                                          minFractionDigits="2" maxFractionDigits="2"/>
						${price}
                        <fmt:message key="currency"/></span>
                </p>
                <c:if test="${fn:toLowerCase(user.role) ne 'admin'}">
                    <p>
					<span class="item_name">
							<button type="button" class="btn btn-success add_to_basket" id="${product.id}">
                                <fmt:message key="add.to.basket"/>
                            </button>
					</span>
                    </p>
                </c:if>
            </div>
        </div>
        <hr>
    </c:forEach>
    <t:pagination/>
</div>