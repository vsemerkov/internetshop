<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setBundle basename="resources" />

<select name="sortParameter" class="middle_input">
	<option value="none"></option>
	<c:forEach var="parameter" items="${sortParameter}">
		<c:choose>
			<c:when test="${selectedSortParameter eq parameter}">
				<option value="${parameter}" selected><fmt:message
						key="sort.${parameter}" /></option>
			</c:when>
			<c:otherwise>
				<option value="${parameter}"><fmt:message key="sort.${parameter}" /></option>
			</c:otherwise>
		</c:choose>
	</c:forEach>
</select>