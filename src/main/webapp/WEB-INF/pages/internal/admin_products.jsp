<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>

<c:set var="title">
    Office Equipment - <fmt:message key="page.title.admin.orders"/>
</c:set>
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>
<div class="container">
    <%@ include file="/WEB-INF/jspf/sign_in.jspf" %>
    <div class="dashboard">
        <t:localeSelection/>
        <%@ include file="/WEB-INF/jspf/header.jspf" %>
        <div class="index_main_panel">
            <fieldset>
                <legend><fmt:message key="products"/></legend>
                <form name="addCategory" action="<c:url value="/adminProduct"/>" method="get" enctype="charset=UTF-8">
                    <div class="big_line">
                        <div class="main_date_panel">
                            <button type="submit" name="sendData" class="btn">
                                <fmt:message key="add"/>
                            </button>
                        </div>
                    </div>
                </form>
                <table class="table table-striped table-hover table-boarder">
                    <thead>
                    <tr class="primary-table">
                        <th class="item_title"><fmt:message key="title.product.id"/></th>
                        <th class="item_title"><fmt:message key="title.product.name"/></th>
                        <th class="item_title primary"><fmt:message key="title.product.price"/></th>
                        <th class="item_title"><fmt:message key="title.product.number"/></th>
                        <th class="item_title"><fmt:message key="title.product.manufacturer"/></th>
                        <th class="item_title"><fmt:message key="title.product.category"/></th>
                    </tr>
                    </thead>
                    <c:forEach var="product" items="${products}">
                        <tr class="clickableRow referenceRow" href="<c:url value="/adminProduct?productId=${product.id}"/>">
                            <td>
                                <div class="basket_field_value"><c:out value="${product.id}"/></div>
                            </td>
                            <td>
                                <div class="basket_field_value"><c:out value="${product.name}"/></div>
                            </td>
                            <td>
                                <fmt:formatNumber value="${product.price}" var="price" type="number"
                                                  minFractionDigits="2" maxFractionDigits="2"/>
                                <div class="basket_field_value"><c:out value="${price}"/> <fmt:message
                                        key="currency"/></div>
                            </td>
                            <td>
                                <div class="basket_field_value"><c:out value="${product.number}"/></div>
                            </td>
                            <td>
                                <div class="basket_field_value"><c:out value="${product.manufacturer.name}"/></div>
                            </td>
                            <td>
                                <div class="basket_field_value"><c:out value="${product.category.name}"/></div>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </fieldset>
        </div>
        <div class="btm"></div>
    </div>
    <%@ include file="/WEB-INF/jspf/footer.jspf" %>
</div>
</body>
</html>