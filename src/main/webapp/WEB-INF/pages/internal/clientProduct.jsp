<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>

<c:set var="title">
    Office Equipment - <fmt:message key="product"/>
</c:set>
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>
<div class="container">
    <%@ include file="/WEB-INF/jspf/sign_in.jspf" %>
    <div class="dashboard">
        <t:localeSelection/>
        <%@ include file="/WEB-INF/jspf/header.jspf" %>
        <div class="index_main_panel">
            <fieldset>
                <legend><fmt:message key="product"/></legend>
                <span class="help-block error" name="error">
                    <c:if test="${error != null}">
                        <fmt:message key="${error}"/>
                    </c:if>
                </span>

                <c:if test="${productId !=0  && productId ne null}">
                    <input type="hidden" name="productId" value="${productId}">

                    <div class="large_line">
                        <div class="item_picture_frame_line">
                            <img src="<c:url value="/productImage?productId=${product.id}"/>"
                                 class="item_picture">
                        </div>
                    </div>
                </c:if>

                <div class="big_line">
                    <div class="main_date_panel">
                        <div class="date_panel"><fmt:message key="title.product.name"/></div>
                        <div class="date_panel_value">
                            ${product.name}
                        </div>
                    </div>
                </div>

                <div class="big_line">
                    <div class="main_date_panel">
                        <div class="date_panel"><fmt:message key="title.product.price"/></div>
                        <div class="date_panel_value">
                            <fmt:formatNumber value="${product.price}" var="price" type="number"
                                              minFractionDigits="2" maxFractionDigits="2"/>
                            ${price} <fmt:message key="currency"/>
                        </div>
                    </div>
                </div>

                <div class="big_line">
                    <div class="main_date_panel">
                        <div class="date_panel"><fmt:message key="title.product.number"/></div>
                        <div class="date_panel_value">
                            ${product.number}
                        </div>
                    </div>
                </div>

                <div class="big_line">
                    <div class="main_date_panel">
                        <div class="date_panel"><fmt:message key="title.product.manufacturer"/></div>
                        <div class="date_panel_value">
                            ${product.manufacturer.name}
                        </div>
                    </div>
                </div>

                <div class="big_line">
                    <div class="main_date_panel">
                        <div class="date_panel"><fmt:message key="title.product.category"/></div>
                        <div class="date_panel_value">
                            ${product.category.name}
                        </div>
                    </div>
                </div>

                <div class="big_line">
                    <div class="main_date_panel">
                        <div class="date_panel"><fmt:message key="title.product.info"/></div>
                        <div class="date_panel_value_textarea">
                            <c:out value="${product.info}"/>
                        </div>
                    </div>
                </div>

                <div class="big_line"></div>
                <div class="big_line"></div>

                <c:if test="${fn:toLowerCase(user.role) ne 'admin'}">
                    <p>
					<span class="item_name">
							<button type="button" class="btn btn-success add_to_basket" id="${product.id}">
                                <fmt:message key="add.to.basket"/>
                            </button>
					</span>
                    </p>
                </c:if>
            </fieldset>
        </div>
        <div class="btm"></div>
    </div>
    <%@ include file="/WEB-INF/jspf/footer.jspf" %>
</div>
</body>
</html>