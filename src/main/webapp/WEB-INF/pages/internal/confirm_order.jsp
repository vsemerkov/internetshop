<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>

<html>

<c:set var="title">
    Office Equipment - <fmt:message key="page.title.confirm.order" />
</c:set>
<%@ include file="/WEB-INF/jspf/head.jspf"%>

<body>
	<div class="container">
		<%@ include file="/WEB-INF/jspf/sign_in.jspf"%>
		<div class="dashboard">
			<t:localeSelection />
			<%@ include file="/WEB-INF/jspf/header.jspf"%>
			<div class="index_main_panel">
					<fieldset>
						<legend><fmt:message key="order.status" /></legend>
						<fmt:message key="order" /> <fmt:message key="order.number" /> <c:out value="${orderId}" /> <fmt:message key="order.adopted" />
						<div class="line">
							<a href="<c:url value="/index"/>"><button type="button" class="btn"><fmt:message key="to.products" /></button></a>
                            <a href="<c:url value="/clientOrders"/>"><button type="button" class="btn"><fmt:message key="to.orders" /></button></a>
						</div>
					</fieldset>
			</div>
			<div class="btm"></div>
		</div>
		<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>