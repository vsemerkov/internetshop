<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>

<c:set var="title">
    Office Equipment - <fmt:message key="page.title.admin.orders"/>
</c:set>
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>
<div class="container">
    <%@ include file="/WEB-INF/jspf/sign_in.jspf" %>
    <div class="dashboard">
        <t:localeSelection/>
        <%@ include file="/WEB-INF/jspf/header.jspf" %>
        <div class="index_main_panel">
            <fieldset>
                <legend><fmt:message key="orders"/></legend>
                <form name="adminOrders" action="<c:url value="/adminOrders"/>" method="get" enctype="charset=UTF-8">
                    <div class="big_line">
                        <div class="main_date_panel">
                            <div class="date_panel"><fmt:message key="order.id"/></div>
                            <div class="date_panel_value"><input type="text" name="orderNumber" maxlength="10"
                                                                 class="large_input" value="${orderNumber}"></div>
                        </div>
                    </div>
                    <div class="big_line">
                        <t:orderStatus/>
                    </div>
                    <div class="big_line">
                        <div class="main_date_panel">
                            <div class="date_panel"><fmt:message key="start.date"/></div>
                            <div class="date_panel_value">
                                <div class="input-append date datepicker">
                                    <input name="startDate" class="datepicker" type="text" value="${startDate}" data-date-format="dd-mm-yyyy">
                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="big_line">
                        <div class="main_date_panel">
                            <div class="date_panel"><fmt:message key="end.date"/></div>
                            <div class="date_panel_value">
                                <div class="input-append date datepicker">
                                    <input name="endDate" class="datepicker" type="text" value="${endDate}" data-date-format="dd-mm-yyyy">
                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="big_line">
                        <div class="main_date_panel">
                            <div class="date_panel"><fmt:message key="orders.on.page"/></div>
                            <div class="date_panel_value">
                                <t:orderCount/>
                            </div>
                        </div>
                    </div>
                    <div class="button_line">
                        <button type="submit" name="sendData" class="btn">
                            <fmt:message key="apply"/>
                        </button>
                    </div>
                </form>
                <div class="number_panel">
                    <span class="item_title"><fmt:message key="total.number" /></span>
                    <c:out value="${numberOfElements}" />
                    <t:pagination />
                </div>
                <table class="table table-striped table-hover table-boarder">
                    <thead>
                    <tr class="primary-table">
                        <th class="item_title"><fmt:message key="order.id"/></th>
                        <th class="item_title"><fmt:message key="order.date"/></th>
                        <th class="item_title primary"><fmt:message key="order.status"/></th>
                        <th class="item_title"><fmt:message key="order.sum"/></th>
                    </tr>
                    </thead>
                    <c:forEach var="order" items="${orders}">
                        <tr class="clickableRow referenceRow" href="<c:url value="/order?orderId=${order.id}"/>">
                            <td>
                                <div class="basket_field_value"><c:out value="${order.id}"/></div>
                            </td>
                            <td>
                                <fmt:formatDate value="${order.orderDate}" var="orderDate"
                                                type="date" pattern="dd-MM-yyyy kk:mm:ss"/>
                                <div class="basket_field_value"><c:out value="${orderDate}"/></div>
                            </td>
                            <td>
                                <c:set var="orderStatus"
                                       value="order.status.${fn:toLowerCase(order.orderStatus)}${order.canceledOrderStatus == null ? '' : '.'}${fn:toLowerCase(order.canceledOrderStatus)}"/>
                                <div class="basket_field_value"><fmt:message key="${orderStatus}"/></div>
                            </td>
                            <td>
                                <fmt:formatNumber value="${order.totalSum}" var="totalSum" type="number"
                                                  minFractionDigits="2" maxFractionDigits="2"/>
                                <div class="basket_field_value"><c:out value="${totalSum}"/> <fmt:message
                                        key="currency"/></div>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
                <c:if test="${numberOfElements == 0}">
                    <fmt:message key="no.orders" />
                </c:if>
                <t:pagination />
            </fieldset>
        </div>
        <div class="btm"></div>
    </div>
    <%@ include file="/WEB-INF/jspf/footer.jspf" %>
</div>
</body>
</html>