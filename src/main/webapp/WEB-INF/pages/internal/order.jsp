<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>

<c:set var="title">
    Office Equipment - <fmt:message key="page.title.order"/>
</c:set>
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>
<div class="container">
    <%@ include file="/WEB-INF/jspf/sign_in.jspf" %>
    <div class="dashboard">
        <t:localeSelection/>
        <%@ include file="/WEB-INF/jspf/header.jspf" %>
        <div class="index_main_panel">
            <fieldset>
                <legend><fmt:message key="order"/></legend>

                <span class="help-block error" name="error">
                    <c:if test="${error != null}">
                        <fmt:message key="${error}"/>
                    </c:if>
                </span>

                <table class="table table-striped table-hover table-boarder">
                    <thead>
                    <tr class="primary-table">
                        <th class="item_title"><fmt:message key="basket.product.name"/></th>
                        <th class="item_title"><fmt:message key="basket.cost.of.item"/></th>
                        <th class="item_title primary"><fmt:message key="basket.product.number"/></th>
                        <th class="item_title"><fmt:message key="basket.product.sum"/></th>
                    </tr>
                    </thead>
                    <c:forEach var="orderItem" items="${orderItems}">
                        <tr>
                            <td>
                                <div class="basket_field_value">
                                    <c:out value="${orderItem.product.manufacturer.name}"/>
                                    <c:out value="${orderItem.product.name}"/>
                                </div>
                            </td>
                            <td>
                                <div class="basket_field_value">
                                    <fmt:formatNumber value="${orderItem.price}" var="price" type="number"
                                                      minFractionDigits="2" maxFractionDigits="2"/>
                                    <c:out value="${price}"/> <fmt:message key="currency"/>
                                </div>
                            </td>
                            <td>
                                <div class="basket_field_value">
                                    <c:out value="${orderItem.number}"/>
                                    <c:if test="${fn:toLowerCase(user.role) eq 'admin' && order.canceledOrderStatus == null}">
                                        (<c:out value="${orderItem.product.number}"/>)
                                    </c:if>
                                </div>
                            </td>
                            <td>
                                <div class="basket_field_value">
										<span id="sum${orderItem.product.id}" class="sum">
                                            <fmt:formatNumber value="${orderItem.price * orderItem.number}" var="sum"
                                                              type="number"
                                                              minFractionDigits="2" maxFractionDigits="2"/>
                                            <c:out value="${sum}"/>
                                        </span>
                                    <fmt:message key="currency"/>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
                <div class="line">
                    <span class="item_title"><fmt:message key="total.sum"/> </span>
                        <span id="orderTotalSum">
                            <fmt:formatNumber value="${order.totalSum}" var="totalSum" type="number"
                                              minFractionDigits="2" maxFractionDigits="2"/>
                            <c:out value="${totalSum}"/>
                        </span>
                    <fmt:message key="currency"/>
                </div>
                <form name="cancelOrder" action="<c:url value="/cancelOrder"/>" method="post">
                    <div class="big_line">
                        <div class="main_date_panel">
                            <div class="date_panel">
                                <fmt:message key="order.status"/>
                            </div>
                            <div class="date_panel_value">
                                <c:set var="orderStatus"
                                       value="order.status.${fn:toLowerCase(order.orderStatus)}${order.canceledOrderStatus == null ? '' : '.'}${fn:toLowerCase(order.canceledOrderStatus)}"/>
                                <fmt:message key="${orderStatus}"/>
                            </div>
                        </div>
                    </div>
                    <div class="big_line">
                        <div class="main_date_panel">
                            <div class="date_panel">
                                <fmt:message key="order.address"/>
                            </div>
                            <div class="date_panel_value">
                                <c:out value="${order.address}"/>
                            </div>
                        </div>
                    </div>
                    <div class="big_line">
                        <div class="main_date_panel">
                            <div class="date_panel"><fmt:message key="order.delivery"/></div>
                            <div class="date_panel_value">
                                <fmt:message key="delivery.${fn:toLowerCase(order.deliveryType)}"/>
                            </div>
                        </div>
                    </div>
                    <div class="big_line">
                        <div class="main_date_panel">
                            <div class="date_panel"><fmt:message key="order.payment"/></div>
                            <div class="date_panel_value">
                                <fmt:message key="payment.${fn:toLowerCase(order.paymentType)}"/>
                            </div>
                        </div>
                    </div>

                    <c:if test="${fn:toLowerCase(user.role) eq 'admin'}">
                        <div class="big_line">
                            <div class="main_date_panel">
                                <div class="date_panel"><fmt:message key="client"/></div>
                                <div class="date_panel_value">
                                    <c:out value="${orderOwner}"/>
                                </div>
                            </div>
                        </div>
                        <div class="big_line">
                            <div class="main_date_panel">
                                <div class="date_panel"><fmt:message key="client.email"/></div>
                                <div class="date_panel_value">
                                    <c:out value="${orderOwnerEmail}"/>
                                </div>
                            </div>
                        </div>
                    </c:if>

                    <input type="hidden" name="orderId" value="${order.id}"/>

                    <div class="line">
                        <c:if test="${fn:toLowerCase(user.role) eq 'client'}">
                            <a href="<c:url value="/clientOrders"/>">
                                <button type="button" class="btn">
                                    <fmt:message key="to.orders"/>
                                </button>
                            </a>
                        </c:if>
                        <c:if test="${fn:toLowerCase(user.role) eq 'admin'}">
                            <a href="<c:url value="/adminOrders"/>">
                                <button type="button" class="btn">
                                    <fmt:message key="to.orders"/>
                                </button>
                            </a>
                        </c:if>
                        <c:if test="${(fn:length(orderItems) gt 0) && (fn:toLowerCase(order.orderStatus) eq 'execution')}">
                            <button type="submit" class="btn btn-danger">
                                <fmt:message key="cancel.order"/>
                            </button>
                        </c:if>
                        </div>
                    </form>
                <form name="executeOrder" action="<c:url value="/executeOrder"/>" method="post">
                    <input type="hidden" name="orderId" value="${order.id}"/>
                    <div class="line">
                        <c:if test="${(fn:length(orderItems) gt 0) && (fn:toLowerCase(order.orderStatus) eq 'execution') && (fn:toLowerCase(user.role) eq 'admin')}">
                            <button type="submit" class="btn btn-success">
                                <fmt:message key="complete.order"/>
                            </button>
                        </c:if>
                    </div>
                </form>
            </fieldset>
        </div>
        <div class="btm"></div>
    </div>
    <%@ include file="/WEB-INF/jspf/footer.jspf" %>
</div>
</body>
</html>