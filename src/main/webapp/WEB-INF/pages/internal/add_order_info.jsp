<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>

<html>

<c:set var="title">
    Office Equipment - <fmt:message key="page.title.confirm.order" />
</c:set>
<%@ include file="/WEB-INF/jspf/head.jspf"%>

<body>
	<div class="container">
		<%@ include file="/WEB-INF/jspf/sign_in.jspf"%>
		<div class="dashboard">
			<t:localeSelection />
			<%@ include file="/WEB-INF/jspf/header.jspf"%>
			<div class="index_main_panel">
				<fieldset>
					<legend><fmt:message key="order.confirmation" /></legend>
                    <table class="table table-striped table-hover table-boarder">
                        <thead>
                        <tr class="primary-table">
                            <th class="item_title"><fmt:message key="basket.product.name" /></th>
                            <th class="item_title"><fmt:message key="basket.cost.of.item" /></th>
                            <th class="item_title primary"><fmt:message key="basket.product.number" /></th>
                            <th class="item_title"><fmt:message key="basket.product.sum" /></th>
                        </tr>
                        </thead>
						<c:forEach var="orderedProduct" items="${items}">
							<tr>
								<td><div class="basket_field_value">
										<c:out value="${orderedProduct.product.manufacturer.name}" />
										<c:out value="${orderedProduct.product.name}" />
									</div></td>
								<td><div class="basket_field_value">
										<span id="price${orderedProduct.product.id}" class="currency_value">
                                            <fmt:formatNumber value="${orderedProduct.price}" var="price" type="number"
                                                              minFractionDigits="2" maxFractionDigits="2"/>
										${price}
										</span> <span class="currency_value"> <fmt:message
												key="currency" /></span>
									</div></td>
								<td><div class="basket_field_value">
										<c:out value="${orderedProduct.number}" />
									</div></td>
								<td><div class="basket_field_value">
										<span id="sum${orderedProduct.product.id}" class="sum">
                                            <fmt:formatNumber value="${orderedProduct.price * orderedProduct.number}" var="sum" type="number"
                                                              minFractionDigits="2" maxFractionDigits="2"/>
                                            <c:out value="${sum}" /></span>
										<fmt:message key="currency" />
									</div></td>
							</tr>
						</c:forEach>
					</table>
					<div class="line">
						<span class="item_title"><fmt:message key="total.sum" /> </span><span
							id="totalSum"></span>
						<fmt:message key="currency" />
					</div>
					<form name="confirmOrder" action="<c:url value="/confirmOrder"/>" method="post">
						<div class="big_line">
							<div class="main_date_panel">
								<div class="date_panel">
									<fmt:message key="order.address" />
								</div>
								<div class="date_panel_value">
									<c:out value="${order.address}" />
								</div>
							</div>
						</div>
						<div class="big_line">
							<div class="main_date_panel">
								<div class="date_panel"><fmt:message key="order.delivery" /></div>
								<div class="date_panel_value">
									<fmt:message key="delivery.${fn:toLowerCase(order.deliveryType)}" />
								</div>
							</div>
						</div>
						<div class="big_line">
							<div class="main_date_panel">
								<div class="date_panel"><fmt:message key="order.payment" /></div>
								<div class="date_panel_value">
									<fmt:message key="payment.${fn:toLowerCase(order.paymentType)}" />
								</div>
							</div>
						</div>
						<input type="hidden" name=paymentType value="${fn:toLowerCase(order.paymentType)}" />
						<div class="big_line invisible" id="card_number_text">
							<div class="main_date_panel">
								<div class="date_panel">
									<fmt:message key="order.card_number" />
								</div>
								<div class="date_panel_value">
									<c:out value="${order.cardNumber}" />
								</div>
							</div>
						</div>
						<div class="line">
							<a href="<c:url value="/makeOrder"/>"><button type="button"
									class="btn">
									<fmt:message key="back" />
								</button></a>
							<c:if test="${fn:length(items) gt 0}">
								<button type="submit" class="btn btn-success">
									<fmt:message key="confirm" />
								</button>
							</c:if>
						</div>
					</form>
				</fieldset>
			</div>
			<div class="btm"></div>
		</div>
		<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>