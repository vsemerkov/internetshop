<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>

<c:set var="title">
    Office Equipment - <fmt:message key="page.title.client.orders" />
</c:set>
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>
<div class="container">
    <%@ include file="/WEB-INF/jspf/sign_in.jspf" %>
    <div class="dashboard">
        <t:localeSelection/>
        <%@ include file="/WEB-INF/jspf/header.jspf" %>
        <div class="index_main_panel">
            <fieldset>
                <legend><fmt:message key="orders"/></legend>
                <table class="table table-striped table-hover table-boarder">
                    <thead>
                    <tr class="primary-table">
                        <th class="item_title"><fmt:message key="order.id"/></th>
                        <th class="item_title"><fmt:message key="order.date"/></th>
                        <th class="item_title primary"><fmt:message key="order.status"/></th>
                        <th class="item_title"><fmt:message key="order.sum"/></th>
                    </tr>
                    </thead>
                    <c:forEach var="order" items="${orders}">
                        <tr class="clickableRow referenceRow" href="<c:url value="/order?orderId=${order.id}"/>">
                            <td>
                                <div class="basket_field_value"><c:out value="${order.id}"/></div>
                            </td>
                            <td>
                                <fmt:formatDate value="${order.orderDate}" var="orderDate"
                                                type="date" pattern="dd-MM-yyyy kk:mm:ss"/>
                                <div class="basket_field_value"><c:out value="${orderDate}"/></div>
                            </td>
                            <td>
                                <c:set var="orderStatus"
                                       value="order.status.${fn:toLowerCase(order.orderStatus)}${order.canceledOrderStatus == null ? '' : '.'}${fn:toLowerCase(order.canceledOrderStatus)}"/>
                                <div class="basket_field_value"><fmt:message key="${orderStatus}"/></div>
                            </td>
                            <td>
                                <fmt:formatNumber value="${order.totalSum}" var="totalSum" type="number"
                                                  minFractionDigits="2" maxFractionDigits="2"/>
                                <div class="basket_field_value"><c:out value="${totalSum}"/> <fmt:message
                                        key="currency"/></div>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </fieldset>
        </div>
        <div class="btm"></div>
    </div>
    <%@ include file="/WEB-INF/jspf/footer.jspf" %>
</div>
</body>
</html>