<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>

<c:set var="title">
    Office Equipment - <fmt:message key="page.title.admin.manufacturers"/>
</c:set>
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>
<div class="container">
    <%@ include file="/WEB-INF/jspf/sign_in.jspf" %>
    <div class="dashboard">
        <t:localeSelection/>
        <%@ include file="/WEB-INF/jspf/header.jspf" %>
        <div class="index_main_panel">
            <fieldset>
                <legend><fmt:message key="page.title.admin.manufacturers"/></legend>
                <form name="addManufacturer" action="<c:url value="/manufacturer"/>" method="get" enctype="charset=UTF-8">
                    <div class="big_line">
                        <div class="main_date_panel">
                            <button type="submit" name="sendData" class="btn">
                                <fmt:message key="add"/>
                            </button>
                        </div>
                    </div>
                </form>
                <table class="table table-striped table-hover table-boarder">
                    <thead>
                    <tr class="primary-table">
                        <th class="item_title"><fmt:message key="manufacturer.id"/></th>
                        <th class="item_title"><fmt:message key="manufacturer.name"/></th>
                    </tr>
                    </thead>
                    <c:forEach var="manufacturer" items="${manufacturers}">
                        <tr class="clickableRow referenceRow" href="<c:url value="/manufacturer?manufacturerId=${manufacturer.id}"/>">
                            <td>
                                <div class="basket_field_value"><c:out value="${manufacturer.id}"/></div>
                            </td>
                            <td>
                                <div class="basket_field_value"><c:out value="${manufacturer.name}"/></div>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </fieldset>
        </div>
        <div class="btm"></div>
    </div>
    <%@ include file="/WEB-INF/jspf/footer.jspf" %>
</div>
</body>
</html>