<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>

<c:set var="title">
    Office Equipment - <fmt:message key="page.title.admin.categories"/>
</c:set>
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>
<div class="container">
    <%@ include file="/WEB-INF/jspf/sign_in.jspf" %>
    <div class="dashboard">
        <t:localeSelection/>
        <%@ include file="/WEB-INF/jspf/header.jspf" %>
        <div class="index_main_panel">
            <fieldset>
                <legend><fmt:message key="page.title.admin.categories"/></legend>
                <form name="addCategory" action="<c:url value="/category"/>" method="get" enctype="charset=UTF-8">
                    <div class="big_line">
                        <div class="main_date_panel">
                            <button type="submit" name="sendData" class="btn">
                                <fmt:message key="add"/>
                            </button>
                        </div>
                    </div>
                </form>
                <table class="table table-striped table-hover table-boarder">
                    <thead>
                    <tr class="primary-table">
                        <th class="item_title"><fmt:message key="category.id"/></th>
                        <th class="item_title"><fmt:message key="category.name"/></th>
                    </tr>
                    </thead>
                    <c:forEach var="category" items="${categories}">
                        <tr class="clickableRow referenceRow" href="<c:url value="/category?categoryId=${category.id}"/>">
                            <td>
                                <div class="basket_field_value"><c:out value="${category.id}"/></div>
                            </td>
                            <td>
                                <div class="basket_field_value"><c:out value="${category.name}"/></div>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </fieldset>
        </div>
        <div class="btm"></div>
    </div>
    <%@ include file="/WEB-INF/jspf/footer.jspf" %>
</div>
</body>
</html>