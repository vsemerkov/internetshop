<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>

<c:set var="title">
    Office Equipment - <fmt:message key="page.title.admin.products"/>
</c:set>
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>
<div class="container">
    <%@ include file="/WEB-INF/jspf/sign_in.jspf" %>
    <div class="dashboard">
        <t:localeSelection/>
        <%@ include file="/WEB-INF/jspf/header.jspf" %>
        <div class="index_main_panel">
            <fieldset>
                <legend><fmt:message key="page.title.admin.products"/></legend>
                <span class="help-block error" name="error">
                    <c:if test="${error != null}">
                        <fmt:message key="${error}"/>
                    </c:if>
                </span>

                <form name="addProduct" action="<c:url value="/product"/>" method="post" enctype="charset=UTF-8">
                    <c:if test="${productId !=0  && productId ne null}">
                        <input type="hidden" name="productId" value="${productId}">

                        <div class="large_line">
                            <div class="item_picture_frame_line">
                                <img src="<c:url value="/productImage?productId=${product.id}"/>"
                                     class="item_picture">
                            </div>
                        </div>
                    </c:if>

                    <div class="big_line">
                        <div class="main_date_panel">
                            <div class="date_panel"><fmt:message key="title.product.name"/></div>
                            <div class="date_panel_value">
                                <input type="text" name="productName" maxlength="30"
                                       class="large_input" value="${product.name}">
                            </div>
                        </div>
                    </div>

                    <div class="big_line">
                        <div class="main_date_panel">
                            <div class="date_panel"><fmt:message key="title.product.price"/></div>
                            <div class="date_panel_value">
                                <input type="text" name="productName" maxlength="30"
                                       class="large_input" value="${product.price}">
                                <fmt:message key="currency"/>
                            </div>
                        </div>
                    </div>

                    <div class="big_line">
                        <div class="main_date_panel">
                            <div class="date_panel"><fmt:message key="title.product.number"/></div>
                            <div class="date_panel_value">
                                <input type="text" name="productName" maxlength="30"
                                       class="large_input" value="${product.number}">
                            </div>
                        </div>
                    </div>

                    <div class="big_line">
                        <t:productManufacturer/>
                    </div>

                    <div class="big_line">
                        <t:productCategory/>
                    </div>

                    <div class="big_line">
                        <div class="main_date_panel">
                            <div class="date_panel"><fmt:message key="title.product.photo"/></div>
                            <div class="date_panel_value">
                                <input type="file" name="photo" accept="image/jpeg,image/png" />
                            </div>
                        </div>
                    </div>

                    <div class="big_line">
                        <div class="main_date_panel">
                            <div class="date_panel"><fmt:message key="title.product.info"/></div>
                            <div class="date_panel_value_textarea">
                                <textarea><c:out value="${product.info}"/></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="big_line"></div>

                    <div class="big_line">
                        <button type="submit" name="sendData" class="btn">
                            <c:choose>
                                <c:when test="${productId!=0 && productId ne null}">
                                    <fmt:message key="edit"/>
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="add"/>
                                </c:otherwise>
                            </c:choose>
                        </button>
                    </div>
                </form>
            </fieldset>
        </div>
        <div class="btm"></div>
    </div>
    <%@ include file="/WEB-INF/jspf/footer.jspf" %>
</div>
</body>
</html>