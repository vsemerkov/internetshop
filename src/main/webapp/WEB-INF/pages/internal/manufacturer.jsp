<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>

<c:set var="title">
    Office Equipment - <fmt:message key="page.title.admin.manufacturers"/>
</c:set>
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>
<div class="container">
    <%@ include file="/WEB-INF/jspf/sign_in.jspf" %>
    <div class="dashboard">
        <t:localeSelection/>
        <%@ include file="/WEB-INF/jspf/header.jspf" %>
        <div class="index_main_panel">
            <fieldset>
                <legend><fmt:message key="page.title.admin.manufacturers"/></legend>
                <span class="help-block error" name="error">
                    <c:if test="${error != null}">
                        <fmt:message key="${error}"/>
                    </c:if>
                </span>

                <form name="addManufacturer" action="<c:url value="/manufacturer"/>" method="post"
                      enctype="charset=UTF-8">
                    <c:if test="${manufacturerId !=0  && manufacturerId ne null}">
                        <input type="hidden" name="manufacturerId" value="${manufacturerId}">
                    </c:if>
                    <div class="big_line">
                        <div class="main_date_panel">
                            <div class="date_panel"><fmt:message key="manufacturer.name"/></div>
                            <div class="date_panel_value">
                                <c:choose>
                                    <c:when test="${manufacturerId !=0 && manufacturerId ne null}">
                                        <input type="text" name="manufacturerName" maxlength="30"
                                               class="large_input" value="${manufacturerName}">
                                    </c:when>
                                    <c:otherwise>
                                        <input type="text" name="manufacturerName" maxlength="30"
                                               class="large_input">
                                    </c:otherwise>
                                </c:choose>
                            </div>
                            <button type="submit" name="sendData" class="btn">
                                <c:choose>
                                    <c:when test="${manufacturerId!=0 && manufacturerId ne null}">
                                        <fmt:message key="edit"/>
                                    </c:when>
                                    <c:otherwise>
                                        <fmt:message key="add"/>
                                    </c:otherwise>
                                </c:choose>
                            </button>
                        </div>
                    </div>
                </form>
            </fieldset>
        </div>
        <div class="btm"></div>
    </div>
    <%@ include file="/WEB-INF/jspf/footer.jspf" %>
</div>
</body>
</html>