﻿<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>

<html>

<c:set var="title">
    Office Equipment - <fmt:message key="page.title.main.page" />
</c:set>
<%@ include file="/WEB-INF/jspf/head.jspf"%>

<body>
	<div class="container">
		<%@ include file="/WEB-INF/jspf/sign_in.jspf"%>
		<div class="dashboard">
			<t:localeSelection />
			<%@ include file="/WEB-INF/jspf/header.jspf"%>
			<div class="index_main_panel">
				<div class="parameters_panel">
					<form name="search" action="<c:url value="/index"/>" method="get" enctype="charset=UTF-8">
						<div class="top_line">
							<fmt:message key="price" />
						</div>
						<span class="search_text_panel"><fmt:message
								key="price.from" /> </span><input type="text" name="minPrice"
							class="min_input" value="${minPrice}" /> <span
							class="search_text_panel"><fmt:message key="price.to" /></span>
						<input type="text" name="maxPrice" class="min_input"
							value="${maxPrice}" />
						<t:categories />
						<t:manufacturers />
						<div class="line">
							<fmt:message key="name" />
						</div>
						<input type="text" name="name" value="${name}" class="input-name" />
						<div class="sort">
							<div class="line">
								<fmt:message key="sort" />
							</div>
							<t:sortParameter />
							<t:sortOrder />
						</div>
						<div class="line">
							<fmt:message key="products.on.page" />
						</div>
						<t:productCount />
						<div class="button_line">
							<button type="submit" name="sendData" class="btn">
								<fmt:message key="apply" />
							</button>
						</div>
					</form>
				</div>
				<div class="main_panel">
					<div class="switch_panel">
						<t:products />
					</div>
				</div>
			</div>
			<div class="btm"></div>
		</div>
		<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>