<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>

<html>

<c:set var="title">
    Office Equipment - <fmt:message key="page.title.basket" />
</c:set>
<%@ include file="/WEB-INF/jspf/head.jspf"%>

<body>
	<div class="container">
		<%@ include file="/WEB-INF/jspf/sign_in.jspf"%>
		<div class="dashboard">
			<t:localeSelection />
			<%@ include file="/WEB-INF/jspf/header.jspf"%>
			<div class="index_main_panel">
					<fieldset>
						<legend><fmt:message key="basket" /></legend>
                        <table class="table table-striped table-hover table-boarder">
                            <thead>
                            <tr class="primary-table">
                                <th class="item_title"><fmt:message key="basket.product.name" /></th>
                                <th class="item_title"><fmt:message key="basket.cost.of.item" /></th>
                                <th class="item_title primary"><fmt:message key="basket.product.number" /></th>
                                <th class="item_title"><fmt:message key="basket.product.sum" /></th>
                                <th class="item_title"></th>
                            </tr>
                            </thead>
							<c:forEach var="orderedProduct" items="${items}">
							<tr>
								<form name="removeFromBasket" action="<c:url value="/removeFromBasket"/>" method="post">
									<td><div class="basket_field_value">${orderedProduct.product.manufacturer.name} ${orderedProduct.product.name}</div></td>
									<td><div class="basket_field_value"><span id="price${orderedProduct.product.id}" class="currency_value">
                                       <fmt:formatNumber value="${orderedProduct.price}" var="price" type="number"
                                                         minFractionDigits="2" maxFractionDigits="2"/>
									${price}
                                    </span> <span class="currency_value">&nbsp;<fmt:message key="currency" /></span></div></td>
									<td>
										<div class="basket_field_value">
											<input type="text" name="number" id="${orderedProduct.product.id}" class="min_input"
												maxlength="6" value="${orderedProduct.number}">
										</div>
										<div class="plus_minus_block">
											<div class="plus">
												<a id="plus${orderedProduct.product.id}" class="image_template_button"><img
													src="<c:url value="/img/plus.png"/>" class="locale_img"
													alt="add"></a>
											</div>
											<div class="minus">
												<a id="minus${orderedProduct.product.id}" class="image_template_button"><img
													src="<c:url value="/img/minus.png"/>" class="locale_img"
													alt="subtrac"></a>
											</div>
										</div>
									</td>
									<td><div class="basket_field_value"><span id="sum${orderedProduct.product.id}" class="sum">
                                        <fmt:formatNumber value="${orderedProduct.price * orderedProduct.number}" var="sum" type="number"
                                                          minFractionDigits="2" maxFractionDigits="2"/>
                                        <c:out value="${sum}" />
                                    </span> <fmt:message key="currency" /></div></td>
									<td><div class="basket_field_value">
									<input type="hidden" name="id" value="${orderedProduct.product.id}">
											<button type="submit" class="btn btn-danger"><fmt:message key="basket.product.remove" /></button>
										</div></td>
								</form>
							</tr>
							</c:forEach>
						</table>
						<div class="line"><span class="item_title"><fmt:message key="total.sum" /> </span><span id="totalSum"></span> <fmt:message key="currency" /></div>
						<div class="line">
							<a href="<c:url value="/index"/>"><button type="button" class="btn"><fmt:message key="to.products" /></button></a>
							<c:if test="${fn:length(items) gt 0}" >
								<a href="<c:url value="/makeOrder"/>"><button type="button" class="btn btn-success"><fmt:message key="make.order" /></button></a>
							</c:if>
						</div>
					</fieldset>
			</div>
			<div class="btm"></div>
		</div>
		<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>