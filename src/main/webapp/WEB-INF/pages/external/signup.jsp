<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>

<html>

<c:set var="title">
    Office Equipment - <fmt:message key="page.title.sign.in" />
</c:set>
<%@ include file="/WEB-INF/jspf/head.jspf"%>

<body>
	<div class="container">
		<%@ include file="/WEB-INF/jspf/sign_in.jspf"%>
		<div class="dashboard">
			<t:localeSelection />
			<%@ include file="/WEB-INF/jspf/header.jspf"%>
			<div class="index_main_panel">
				<form name="signupForm" action="<c:url value="/registration"/>"
					enctype="multipart/form-data" method="post">
					<fieldset>
						<legend>
							<fmt:message key="sign.in" />
						</legend>
						<label><fmt:message key="first.name" /></label>
						<input type="text" id="inputError" name="firstName" maxlength="25" value="${formBean.firstName}">
						<span class="help-block error" id="firstName">${paramErrors['firstName']}</span> 
						<label><fmt:message key="second.name" /></label>
						<input type="text" name="lastName" maxlength="25" value="${formBean.lastName}">
						<span class="help-block error" id="lastName">${paramErrors['lastName']}</span>
						<label><fmt:message key="email" /></label>
						<input type="text" name="email" maxlength="25" value="${formBean.email}">
						<span class="help-block error" id="email">${paramErrors['email']}</span>
						<label><fmt:message key="password" /></label>
						<input type="password" name="password" maxlength="25">
						<span class="help-block error" id="password">${paramErrors['password']}</span>
						<label><fmt:message key="repeat.password" /></label>
						<input type="password" name="passwordRepeat" maxlength="25">
						<span class="help-block error" id="passwordRepeat">${paramErrors['passwordRepeat']}</span>
						<label><fmt:message key="image" /></label>
						<input type="file"	name="image" accept="image/jpeg,image/png" />
						<span class="help-block error" id="image">${paramErrors['image']}</span>
						<label><fmt:message key="captcha" /></label>
						<input type="text" id="inputError" name="captcha" maxlength="5">
						<span class="help-block error" id="captcha">${paramErrors['captcha']}</span>
						<t:captcha />
						<p>
							<label class="checkbox"> <input type="checkbox"
								name="subscribe"
								<c:if test="${formBean.subscribe}">
						checked
					</c:if>>
								<fmt:message key="subscribe" />
							</label>
						</p>
						<span class="help-block error" id="user">${paramErrors['user']
							}</span>
						<p>
							<button type="submit" name="sendData" class="btn">
								<fmt:message key="sign.in" />
							</button>
						</p>
					</fieldset>
				</form>
			</div>
			<div class="btm"></div>
		</div>
		<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>