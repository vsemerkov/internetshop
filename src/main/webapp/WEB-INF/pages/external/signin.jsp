<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>

<html>

<c:set var="title">
    Office Equipment - <fmt:message key="page.title.login" />
</c:set>
<%@ include file="/WEB-INF/jspf/head.jspf"%>

<body>
	<div class="container">
		<%@ include file="/WEB-INF/jspf/sign_in.jspf"%>
		<div class="dashboard">
			<t:localeSelection />
			<%@ include file="/WEB-INF/jspf/header.jspf"%>
			<div class="index_main_panel">
					<fieldset>
						<legend>
							<fmt:message key="login" />
						</legend>
						<form name="mainLoginForm" action="<c:url value="/signin"/>"
							method="post" class="form-inline">
							<p>
								<input type="text" name="formLoginEmail"
									placeholder="<fmt:message key="email"/>" value="${email}">
							</p>
							<p>
								<input type="password" name="formLoginPassword"
									placeholder="<fmt:message key="password"/>">
							</p>
							<p>
								<button type="submit" name="formSendLoginData" class="btn">
									<fmt:message key="login" />
								</button>
							</p>
						</form>
						<span class="help-block error" id="formLoginError">${paramErrors['loginError'] }</span>
					</fieldset>
			</div>
			<div class="btm"></div>
		</div>
		<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>